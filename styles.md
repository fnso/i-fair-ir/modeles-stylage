## Styles



### Syntaxe

- pour le séparater dans les noms de styles :
  - `:` > `_3a_`
  - `_` > `_5f_`
  - `-` > `-`



### Mode de saisie simple application du style

- Mode de saisie à préciser

- Langue `language` : liste déroulante ?
- titres traduits `trl-title_fr, trl-title_en, ...` : application de la langue ?
- Index mots clés `keywords_fr, keywords_en, ...` : application de la langue ?
- résumés `abstract_fr, abstract_en, ...` : application de la langue ?
- `citation` : application de la langue ?
- Début Section biblio : fonction pour débuter la section biblio -> ajout d'un paragraphe contenant la string "########" stylé en `bibl-section`. Pour Métopes, l'éditeur peut remplacer la string par le titre de la section "Bibliographie", "Référence",etc.
- Début section Annexe : fonction pour débuter la section annexe -> ajout d'un paragraphe contenant la string "########" stylé en `appendix-section`. Pour Métopes, l'éditeur peut remplacer la string par le titre de la section "Annexes".
- Titres et intertitres des annexes ??

- contributeurs : index de personnes pour OE / rang d'autorité pour Métopes
- métadonnées des contributeurs : affiliation (forme libre hors référentiel), email, biographie



### Liste des styles

- Titre `title`
- Sous-titre `subtitle`
- Surtitre `subhead`
- Numéro du document `documentnumber` 
- Note de la rédaction / de l'éditeur `note_pbl`
- Note de l'auteur `note_aut`
- Note du traducteur `note_trl`
- Dedicace `dedication`
- Remerciements `ack`
- Chapo `leadparagraph`
- Epigraphe `epigraphe`
- Référence bibliographique `bibliography`
- Titre1 -> Titre 6 `heading1` -> `heading6`
- Normal `normal`
- Séparateur `break`
- Paragraphe de suite `consecutiveparagraph`
- citation
- suite de citation
- citation imbriquée
- (car) citation inline
- didascalie
- ligne versifiée
- réplique
- (car) locuteur
- (car) didascalie
- vers
- (car) num vers
- source (de l'épigraphe ou de la citation)
- date de réception 
- date d'acceptation



| Style validé                        | Label FR                              | Label EN                    |
| ----------------------------------- | ------------------------------------- | --------------------------- |
| TEI_didascaly                       | didascalie                            | Didascaly                   |
| TEI_didascaly-inline                | (car.) didascalie                     | Inline Didascaly            |
| TEI_replica                         | réplique                              | Replica                     |
| TEI_speaker                         | locuteur                              | Speaker                     |
| TEI_speaker-inline                  | (car.) locuteur                       | Speaker                     |
| TEI_verse                           | vers                                  | Verse                       |
| TEI_versifiedreplica                | Réplique versifiée                    | Versified replica           |
| TEI_versenumber-inline              | (car.) num vers                       | Verse number                |
| TEI_quote_continuation              | suite de citation                     | continuation of quotation   |
| TEI_quote-inline                    | (car) citation en ligne               | inline quote                |
| TEI_quote_nested                    | citation imbriquée                    | Nested quotation (ou quote) |
| TEI_quote                           | citation                              | quote                       |
| TEI_quote2                          | citation 2                            | quote 2                     |
| TEI_quote:trl:xx                    | citation dans une autre langue        |                             |
| TEI_reviewed_author-inline          | (car) auteur de l'oeuvre              | Reviewed author             |
| TEI_reviewed_date-inline            | (car) date de l'oeuvre                | Reviewed date               |
| TEI_reviewed_reference              | référence bibliographique oeuvre      | Reviewed reference          |
| TEI_reviewed_title-inline           | (car) titre de l'oeuvre               | Reviewed title              |
| TEI_figure_caption                  | légende figure                        | Figure caption              |
| TEI_figure_credits                  | crédits figure                        | Figure credits              |
| TEI_figure_title                    | titre figure                          | Figure title                |
| TEI_formula                         | Formules                              | Formula                     |
| TEI_formula-inline                  | (car.) Formules inline                | inline Formula              |
| TEI_bibl_reference-inline           | (car.) Référence bibliographique      | Bibliographic citation      |
| TEI_bibl_citation                   | Référence bibliographique (source)    | Bibliographic citation      |
| TEI_linguistic_example              | Exemple                               | Example                     |
| TEI_linguistic_gloss                | Glose                                 | Gloss                       |
| TEI_linguistic_number-inline        | (car) Numéro                          | Number                      |
| TEI_linguistic_translation          | Traduction                            | Translation                 |
| TEI_linguistic_seg                  | (car.)                                | (car.)                      |
| TEI_keywords_subjects:year          | Année de l'opération                  | Operation's year            |
| TEI_patriarcheid                    | patriarcheid                          | patriarcheid                |
| TEI_pagination                      | Pagination                            | Pagination                  |
| TEI_document_number                 | Numéro du document                    | Document number             |
| TEI_title:sup                       | Surtitre                              | Subhead                     |
| TEI_keywords_subjects:works         | Oeuvre citée                          | Works                       |
| TEI_keywords_subjects:propernoun    | Noms mots-clés                        | Proper Nouns                |
| TEI_keywords_subjects:quotedpersons | Personnes citées                      | Quoted persons              |
| TEI_abstract:fr                     | Résumé                                | Abstract                    |
| TEI_editor:ctb                      | Collaborateur                         | Collaborator                |
| TEI_editor:fld                      | Directeur de fouilles                 | Excavations director        |
| TEI_author:aut                      | Auteur                                | Author                      |
| TEI_editor:trl                      | Traducteur                            | Translator                  |
| TEI_editor:edt                      | Editeur scientifique                  | Academic editor             |
| TEI_authority_biography             | Biographie auteur                     | Author's biography          |
| TEI_authority_mail                  | Mail                                  | Mail                        |
| TEI_authority_affiliation           | Affiliation                           | Affiliation                 |
| TEI_keywords_subjects:chronology    | Chronologique                         | Chronology                  |
| TEI_keywords_subjects:geography     | Géographique                          | Geography                   |
| TEI_keywords:xx                     | Mots Clés                             | Keywords                    |
| TEI_language                        | Langue                                | Language                    |
| TEI_date_reception                  | Date de réception                     | Reception date              |
| TEI_keywords_subjects:subject       | Thèmes                                | Subject                     |
| TEI_title:sub                       | Sous-titre                            | Subtitle                    |
| style natif Word : wdStyleTitle     | Titre                                 | Title                       |
| TEI_date_acceptance                 | Date d'acceptation                    | Acceptance date             |
| TEI_title:trl:xx                    | Titres traduits                       | Translated titles           |
| TEI_paragraph_break                 | Séparateur                            | Separator                   |
| style natif Word                    | Titre1 -> Titre 6                     | Heading1 -> Heading6        |
| TEI_erratum                         | Erratum                               | Erratum                     |
| TEI_acknowledgment                  | Remerciements                         | Acknowledgment              |
| TEI_appendix_start                  | Début Section annexe                  | Appendix section            |
| TEI_bibl_start                      | Début Section biblio                  | Bibliography section        |
| TEI_bibl_reference                  | Référence bibliographique             | Bibliography                |
| TEI_dedication                      | Dédicace                              | Dedication                  |
| TEI_epigraph                        | Epigraphe                             | Epigraph                    |
| TEI_paragraph_lead                  | Chapo                                 | Lead paragraph              |
| TEI_note:aut                        | Note de l'auteur                      | Author's note               |
| TEI_note:pbl                        | Note de la rédaction / de l'éditeur ? | Editor's note               |
| TEI_note:trl                        | Note du traducteur                    | Translator's note           |
| TEI_answer                          | Réponse / Interlocuteur 2             | Answer                      |
| TEI_question                        | Question / Interlocuteur 1            | Question                    |
| TEI_floatingText_start              | début encadré                         | Box                         |
| TEI_floatingText_end                | fin encadré                           |                             |
| TEI_code                            | code                                  | Code                        |
| TEI_code-inline                     | (car) code                            | Inline Code                 |
| TEI_paragraph_consecutive           | Paragraphe de suite                   | Consecutive paragraph       |
| style natif Word                    | Normal                                | Normal                      |
| style natif Word                    | note de bas de page                   | Footnote                    |
| style natif Word                    | note de fin                           | Endnote                     |
| style natif Word                    | listes                                | List                        |
| TEI_cell                            | cellule de tableaux données           | Cell                        |
| TEI_image_alternative               |                                       |                             |
