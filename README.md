Ce dépôt git contient les modèles de stylage pour Microsoft Word (version LibreOffice à venir) pour préparer les documents à la conversion au format TEI Commons-Publishing par Circé.

Les outils associés (Schéma TEI, Circé) sont versionnés à cette adresse : https://git.unicaen.fr/fnso/i-fair-ir


Les dernières versions des modèles de documents au format `.dotm` pour Métopes et OpenEdition sont disponibles aux urls suivantes :
- https://git.unicaen.fr/fnso/i-fair-ir/modeles-stylage/-/tree/master/MS_Word/Metopes
- https://git.unicaen.fr/fnso/i-fair-ir/modeles-stylage/-/tree/master/MS_Word/OpenEdition

## Installation

N.B. : autoriser les macros.



## Compatibilités

- Word 2013 (Windows)
- Word 2016 (Windows et Mac OS)
- Word 2019 (Windows et Mac OS)



## Traductions

- Modèle Métopes : Traductions embarquées en français, espagnol et anglais.
- Modèle OpenEdition : Traductions embarquées en français et anglais.


## Licence et contributeurs

Licence : CeCILL-C voir fichier [LICENSE.txt](https://git.unicaen.fr/fnso/i-fair-ir/modeles-stylage/-/blob/master/LICENSE.txt?ref_type=heads).

Contributeurs : OpenEdition, Métopes.



