VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ndl_selector_es 
   Caption         =   "Nota"
   ClientHeight    =   2072
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   4067
   OleObjectBlob   =   "ndl_selector_es.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "ndl_selector_es"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub validate_Click()
    ndl_selector_es.Hide
End Sub

Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With ndl_selector_es.dropdown_ndl
        .AddItem ""
        .AddItem "autor [aut]"
        .AddItem "editor [pbl]"
        .AddItem "traductor [trl]"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub

Private Sub cancel_Click()
    Unload Me
End Sub
