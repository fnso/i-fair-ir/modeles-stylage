VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} code_selector 
   Caption         =   "Code language"
   ClientHeight    =   1988
   ClientLeft      =   105
   ClientTop       =   455
   ClientWidth     =   4319
   OleObjectBlob   =   "code_selector.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "code_selector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub dropdown_code_Change()

End Sub

Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With code_selector.dropdown_code
        .AddItem "undefined"
        'source: https://github.com/syntaxhighlighter/syntaxhighlighter/wiki/Package-Status
        .AddItem "bash"
        .AddItem "cpp"
        .AddItem "csharp"
        .AddItem "css"
        .AddItem "diff"
        .AddItem "java"
        .AddItem "javascript"
        .AddItem "perl"
        .AddItem "php"
        .AddItem "plain"
        .AddItem "python"
        .AddItem "ruby"
        .AddItem "sass"
        .AddItem "scala"
        .AddItem "sql"
        .AddItem "vb"
        .AddItem "xml"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub

Private Sub validate_Click()
    code_selector.Hide
End Sub

