VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} typez39_en 
   Caption         =   "Choose document type"
   ClientHeight    =   2541
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   4900
   OleObjectBlob   =   "typez39_en.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "typez39_en"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub dropdown_typez39_Change()

End Sub


Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With typez39_en.dropdown_typez39
        '.AddItem ""
        .AddItem "(j) article [article]"
        .AddItem "(b) chapter [chapter]"
        .AddItem "----------------------"
        .AddItem "(j) acknowledgments [acknowledgments]"
        .AddItem "(j) dedication [dedication]"
        .AddItem "(j) foreword [foreword]"
        .AddItem "(j) preamble [preamble]"
        .AddItem "(j) introduction [introduction]"
        .AddItem "(j) editorial [editorial]"
        .AddItem "(j) article [article]"
        .AddItem "(j) review(s) [review]"
        .AddItem "(j) feature [feature]"
        .AddItem "(j) commentary [commentary]"
        .AddItem "(j) conclusion [conclusion]"
        .AddItem "(j) appendix [appendix]"
        .AddItem "(j) bibliography [bibliography]"
        .AddItem "(j) list of illustrations [loi]"
        .AddItem "(j) list of tables [lot]"
        .AddItem "(j) contributors [contributeurs]"
        .AddItem "(j) other-credits [other-credits]"
        .AddItem "(j) timeline [timeline]"
        .AddItem "(j) collection [collection]"
        .AddItem "(j) interrelated items [Interrelated items]"
        .AddItem "(j) gallery [gallery]"
        .AddItem "(j) abbreviations [abbreviations]"
        .AddItem "(j) catalogue [catalogue]"
        .AddItem "----------------------"
        .AddItem "(b) acknowledgments [acknowledgments]"
        .AddItem "(b) dedication [dedication]"
        .AddItem "(b) epigraph [epigraph]"
        .AddItem "(b) preamble [preamble]"
        .AddItem "(b) biographical-note [biographical-note]"
        .AddItem "(b) translator-note [translator-note]"
        .AddItem "(b) editorial-note [editorial-note]"
        .AddItem "(b) foreword [foreword]"
        .AddItem "(b) preface [preface]"
        .AddItem "(b) introduction [introduction]"
        .AddItem "(b) chapter [chapter]"
        .AddItem "(b) conclusion [conclusion]"
        .AddItem "(b) afterword [afterword]"
        .AddItem "(b) bibliography [bibliography]"
        .AddItem "(b) annexes [appendix]"
        .AddItem "(b) glossary [glossary]"
        .AddItem "(b) publisher-works [publisher-works]"
        .AddItem "(b) rear notes [rearnotes]"
        .AddItem "(b) list of illustrations [loi]"
        .AddItem "(b) list of tables [lot]"
        .AddItem "(b) contributors [contributeurs]"
        .AddItem "(b) other-credits [other-credits]"
        .AddItem "(b) timeline [timeline]"
        .AddItem "(b) collection [collection]"
        .AddItem "(b) interrelated items [interrelated items]"
        .AddItem "(b) gallery [gallery]"
        .AddItem "(b) abbreviations [abbreviations]"
        .AddItem "(b) catalogue [catalogue]"
    End With
End Sub

Private Sub validate_Click()
    typez39_en.Hide
End Sub
