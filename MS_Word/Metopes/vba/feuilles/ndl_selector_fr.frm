VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ndl_selector_fr 
   Caption         =   "Note de ..."
   ClientHeight    =   2128
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   3969
   OleObjectBlob   =   "ndl_selector_fr.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "ndl_selector_fr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub validate_Click()
    ndl_selector_fr.Hide
End Sub

Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With ndl_selector_fr.dropdown_ndl
        .AddItem ""
        .AddItem "auteur [aut]"
        .AddItem "�diteur [pbl]"
        .AddItem "traducteur [trl]"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub
Private Sub cancel_Click()
    Unload Me
End Sub
