VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} type_selector_fr 
   Caption         =   "Type de divisions de texte"
   ClientHeight    =   1988
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   3710
   OleObjectBlob   =   "type_selector_fr.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "type_selector_fr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With type_selector_fr.dropdown_type
        .AddItem "chronique"
        .AddItem "notice"
        .AddItem "rubrique"
        .AddItem "<alt�>"

        .style = fmStyleDropDownCombo
        .MatchRequired = False
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub

Private Sub validate_Click()
    type_selector_fr.Hide
End Sub

Private Sub cancel_Click()
    Me.Hide
End Sub




