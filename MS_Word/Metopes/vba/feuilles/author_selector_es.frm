VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} author_selector_es 
   Caption         =   "Papel del autor"
   ClientHeight    =   1988
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   3710
   OleObjectBlob   =   "author_selector_es.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "author_selector_es"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With author_selector_es.dropdown_aut
        .AddItem ""
        .AddItem "autor [aut]"
        .AddItem "autor corespondiente [aut rcp]"
        .AddItem "ed. cient. [edt]"
        .AddItem "traductor [trl]"
        .AddItem "fotógrafo [pht]"
        .AddItem "illustrador [ill]"
        .AddItem "dibujante [drm]"
        .AddItem "dir. excav. [fld]"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub

Private Sub validate_Click()
    author_selector_es.Hide
End Sub

Private Sub cancel_Click()
    Unload Me
End Sub
