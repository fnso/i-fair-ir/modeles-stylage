VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} author_selector_fr 
   Caption         =   "R�le de l�auteur"
   ClientHeight    =   1946
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   4921
   OleObjectBlob   =   "author_selector_fr.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "author_selector_fr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With author_selector_fr.dropdown_aut
        .AddItem ""
        .AddItem "auteur [aut]"
        .AddItem "auteur correspondant [aut rcp]"
        .AddItem "�diteur scientifique [edt]"
        .AddItem "traducteur [trl]"
        .AddItem "photographe [pht]"
        .AddItem "illustrateur [ill]"
        .AddItem "dessinateur [drm]"
        .AddItem "directeur d�op�rations [fld]"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub

Private Sub validate_Click()
    author_selector_fr.Hide
End Sub

Private Sub cancel_Click()
    Unload Me
End Sub
