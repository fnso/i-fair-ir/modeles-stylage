VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} editor_selector_en 
   Caption         =   "Collaboratorís role"
   ClientHeight    =   2177
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   5292
   OleObjectBlob   =   "editor_selector_en.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "editor_selector_en"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    editor_selector_en.Caption = "Collaborator" & ChrW(39) & "s role"
    With editor_selector_en.dropdown_aut
        .AddItem ""
        .AddItem "translator [trl]"
        .AddItem "editor [edt]"
        .AddItem "annotator [ann]"
        .AddItem "contributor [ctb]"
        .AddItem "compiler [com]"
        .AddItem "cartographer [ctg]"
        .AddItem "illlustrator [ill]"
        .AddItem "draftsman [drm]"
        .AddItem "photographer [pht]"
        .AddItem "field director [fld]"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub
Private Sub validate_Click()
    editor_selector_en.Hide
End Sub
Private Sub cancel_Click()
    Unload Me
End Sub
