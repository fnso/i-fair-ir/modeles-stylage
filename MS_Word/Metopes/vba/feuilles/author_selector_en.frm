VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} author_selector_en 
   Caption         =   "Author�s role"
   ClientHeight    =   1988
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   3710
   OleObjectBlob   =   "author_selector_en.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "author_selector_en"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    author_selector_en.Caption = "Author" & ChrW(39) & "s role"
    With author_selector_en.dropdown_aut
        .AddItem ""
        .AddItem "author [aut]"
        .AddItem "corresponding author [aut rcp]"
        .AddItem "editor [edt]"
        .AddItem "translator [trl]"
        .AddItem "photograph [pht]"
        .AddItem "illlustrator [ill]"
        .AddItem "draftsman [drm]"
        .AddItem "field director [fld]"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub

Private Sub validate_Click()
    author_selector_en.Hide
End Sub

Private Sub cancel_Click()
    Unload Me
End Sub
