VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} typez39_fr 
   Caption         =   "Choisir le type de document"
   ClientHeight    =   2548
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   4907
   OleObjectBlob   =   "typez39_fr.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "typez39_fr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub dropdown_typez39_Change()

End Sub


Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With typez39_fr.dropdown_typez39
        '.AddItem ""
        .AddItem "(j) article [article]"
        .AddItem "(b) chapitre [chapter]"
        .AddItem "----------------------"
        .AddItem "(j) remerciements [acknowledgments]"
        .AddItem "(j) d�dicace [dedication]"
        .AddItem "(j) avant-propos [foreword]"
        .AddItem "(j) pr�ambule [preamble]"
        .AddItem "(j) introduction [introduction]"
        .AddItem "(j) �ditorial [editorial]"
        .AddItem "(j) article [article]"
        .AddItem "(j) recension(s) [review]"
        .AddItem "(j) chronique [feature]"
        .AddItem "(j) compte-rendu [commentary]"
        .AddItem "(j) conclusion [conclusion]"
        .AddItem "(j) annexes [appendix]"
        .AddItem "(j) bibliographie [bibliography]"
        .AddItem "(j) liste des illustrations [loi]"
        .AddItem "(j) liste des tables [lot]"
        .AddItem "(j) contributeurs [contributeurs]"
        .AddItem "(j) autres cr�dits [other-credits]"
        .AddItem "(j) chronologie [timeline]"
        .AddItem "(j) collection [collection]"
        .AddItem "(j) hors texte [Interrelated items]"
        .AddItem "(j) gallerie [gallery]"
        .AddItem "(j) abr�viations [abbreviations]"
        .AddItem "(j) catalogue [catalogue]"
        .AddItem "----------------------"
        .AddItem "(b) remerciements [acknowledgments]"
        .AddItem "(b) d�dicace [dedication]"
        .AddItem "(b) �pigraphe [epigraph]"
        .AddItem "(b) pr�ambule [preamble]"
        .AddItem "(b) notice biographique [biographical-note]"
        .AddItem "(b) note de traduction [translator-note]"
        .AddItem "(b) note �ditoriale [editorial-note]"
        .AddItem "(b) avant-propos [foreword]"
        .AddItem "(b) pr�face [preface]"
        .AddItem "(b) introduction [introduction]"
        .AddItem "(b) chapitre [chapter]"
        .AddItem "(b) conclusion [conclusion]"
        .AddItem "(b) postface [afterword]"
        .AddItem "(b) bibliographie [bibliography]"
        .AddItem "(b) annexes [appendix]"
        .AddItem "(b) glossaire [glossary]"
        .AddItem "(b) publications [publisher-works]"
        .AddItem "(b) notes de fin [rearnotes]"
        .AddItem "(b) liste des illustrations [loi]"
        .AddItem "(b) liste des tables [lot]"
        .AddItem "(b) contributeurs [contributeurs]"
        .AddItem "(b) autres cr�dits [other-credits]"
        .AddItem "(b) chronologie [timeline]"
        .AddItem "(b) collection [collection]"
        .AddItem "(b) hors texte [interrelated items]"
        .AddItem "(b) gallerie [gallery]"
        .AddItem "(b) abr�viations [abbreviations]"
        .AddItem "(b) catalogue [catalogue]"
    End With
End Sub

Private Sub validate_Click()
    typez39_fr.Hide
End Sub



