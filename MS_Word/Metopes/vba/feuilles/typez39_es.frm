VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} typez39_es 
   Caption         =   "Elegir el tipo de documento"
   ClientHeight    =   2583
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   4858
   OleObjectBlob   =   "typez39_es.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "typez39_es"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub dropdown_typez39_Change()

End Sub


Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With typez39_es.dropdown_typez39
        '.AddItem ""
        .AddItem "(j) art�culo [article]"
        .AddItem "(b) cap�tulo [chapter]"
        .AddItem "----------------------"
        .AddItem "(j) agradecimientos [acknowledgments]"
        .AddItem "(j) dedicatoria [dedication]"
        .AddItem "(j) pr�logo [foreword]"
        .AddItem "(j) pre�mbulo [preamble]"
        .AddItem "(j) introducci�n [introduction]"
        .AddItem "(j) editorial [editorial]"
        .AddItem "(j) art�culo [article]"
        .AddItem "(j) rese�a(s) [review]"
        .AddItem "(j) cr�nica [feature]"
        .AddItem "(j) informe [commentary]"
        .AddItem "(j) conclusi�n [conclusion]"
        .AddItem "(j) anexo [appendix]"
        .AddItem "(j) bibliograf�a [bibliography]"
        .AddItem "(j) lista de ilustraciones [loi]"
        .AddItem "(j) lista de tablas [lot]"
        .AddItem "(j) contribuyentes [contributeurs]"
        .AddItem "(j) cr�ditos [other-credits]"
        .AddItem "(j) cronolog�a [timeline]"
        .AddItem "(j) colecci�n [collection]"
        .AddItem "(j) fuera del texto [Interrelated items]"
        .AddItem "(j) galer� [gallery]"
        .AddItem "(j) abreviaturas [abbreviations]"
        .AddItem "(j) publicaciones [catalogue]"
        .AddItem "----------------------"
        .AddItem "(b) agradecimientos [acknowledgments]"
        .AddItem "(b) dedicatoria [dedication]"
        .AddItem "(b) ep�grafe [epigraph]"
        .AddItem "(b) pre�mbulo [preamble]"
        .AddItem "(b) datos biogr�ficos [biographical-note]"
        .AddItem "(b) nota de traducci�n [translator-note]"
        .AddItem "(b) nota del editor [editorial-note]"
        .AddItem "(b) pr�logo [foreword]"
        .AddItem "(b) prefacio [preface]"
        .AddItem "(b) introducci�n [introduction]"
        .AddItem "(b) cap�tulo [chapter]"
        .AddItem "(b) conclusi�n [conclusion]"
        .AddItem "(b) ep�logo [afterword]"
        .AddItem "(b) bibliograf�a [bibliography]"
        .AddItem "(b) anexo [appendix]"
        .AddItem "(b) glosario [glossary]"
        .AddItem "(b) publicaciones [publisher-works]"
        .AddItem "(b) notas de fin [rearnotes]"
        .AddItem "(b) lista de ilustraciones [loi]"
        .AddItem "(b) lista de tablas [lot]"
        .AddItem "(b) contribuyentes [contributeurs]"
        .AddItem "(b) cr�ditos [other-credits]"
        .AddItem "(b) cronolog�a [timeline]"
        .AddItem "(b) colecci�n [collection]"
        .AddItem "(b) fuera del texto [interrelated items]"
        .AddItem "(b) galer� [gallery]"
        .AddItem "(b) abreviaturas [abbreviations]"
        .AddItem "(b) publicaciones [catalogue]"
    End With
End Sub

Private Sub validate_Click()
    typez39_es.Hide
End Sub
