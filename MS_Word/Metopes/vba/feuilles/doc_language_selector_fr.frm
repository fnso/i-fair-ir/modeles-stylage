VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} doc_language_selector_fr 
   Caption         =   "Langue du document"
   ClientHeight    =   2002
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   4319
   OleObjectBlob   =   "doc_language_selector_fr.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "doc_language_selector_fr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub dropdown_language_Change()

End Sub

Private Sub dropdown_doc_language_Change()

End Sub

Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
' source : https://fr.wiktionary.org/wiki/Wiktionnaire:BCP_47/language-2
' source : https://fr.wiktionary.org/wiki/Wiktionnaire:BCP_47/region-2
    With doc_language_selector_fr.dropdown_doc_language
        '.AddItem ""
        .AddItem "de-DE"
        .AddItem "en-GB"
        .AddItem "en-US"
        .AddItem "es-ES"
        .AddItem "es-AR"
        .AddItem "es-PE"
        .AddItem "fr-FR"
        .AddItem "fr-CA"
        .AddItem "it-IT"
        .AddItem "pt-PT"
        .AddItem "pt-BR"
        .AddItem "Autre langue"
    End With
End Sub

Private Sub validate_Click()
    doc_language_selector_fr.Hide
End Sub


