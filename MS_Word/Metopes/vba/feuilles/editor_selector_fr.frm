VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} editor_selector_fr 
   Caption         =   "R�le du collaborateur"
   ClientHeight    =   1974
   ClientLeft      =   91
   ClientTop       =   406
   ClientWidth     =   5292
   OleObjectBlob   =   "editor_selector_fr.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "editor_selector_fr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub UserForm_Initialize()
  #If Mac Then
    ResizeUserForm Me
  #End If
    With editor_selector_fr.dropdown_aut
        .AddItem ""
        .AddItem "traducteur [trl]"
        .AddItem "�d. scient. [edt]"
        .AddItem "annotateur [ann]"
        .AddItem "contributeur [ctb]"
        .AddItem "compilateur [com]"
        .AddItem "cartographe [ctg]"
        .AddItem "illustrateur [ill]"
        .AddItem "dessinateur [drm]"
        .AddItem "photographe [pht]"
        .AddItem "directeur d�op�rations [fld]"
        
        .style = fmStyleDropDownList
        .BoundColumn = 1
        .ListIndex = 0
    End With
End Sub

Private Sub validate_Click()
    editor_selector_fr.Hide
End Sub
Private Sub cancel_Click()
    Unload Me
End Sub
