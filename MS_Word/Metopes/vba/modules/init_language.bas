Attribute VB_Name = "init_language"
'Attribute VB_Name = "init_and_commons"
Sub initialize()
Dim languageCode As String
    With ActiveDocument.ActiveWindow
        .View.Type = wdNormalView
        .View.ShowAll = True
        .StyleAreaWidth = CentimetersToPoints(4)
    End With
    
    If ActiveDocument.Footnotes.Count >= 1 Then
        With ActiveDocument.ActiveWindow.View
            .SplitSpecial = wdPaneFootnotes
        End With
    End If
    
    Call SetMetopesDocumentProperty
    'La commande pour lier le mod�le et mettre � jour les styles de document est appel�e deux fois (condition de fonctionnement)
    Call AccessGlobalTemplate
    Call AccessGlobalTemplate
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload doc_language_selector_fr
        Load doc_language_selector_fr
        doc_language_selector_fr.Show
        result = doc_language_selector_fr.dropdown_doc_language.Value
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload doc_language_selector_es
        Load doc_language_selector_es
        doc_language_selector_es.Show
        result = doc_language_selector_es.dropdown_doc_language.Value
    Case Else 'en
        Unload doc_language_selector_en
        Load doc_language_selector_en
        doc_language_selector_en.Show
        result = doc_language_selector_en.dropdown_doc_language.Value
    End Select
    
    'doc_language_selector.Show
    'result = doc_language_selector.dropdown_doc_language.Value
    
    'MsgBox "Result =" + result
    
    'r�cup' OE : � conserver ?
    If result <> "" Then
        On Error Resume Next
        If result = "Other language" Or result = "Autre langue" Or result = "Otro idioma" Then
            Dim Message, Title, Default, MyValue
            
            Select Case Application.International(wdProductLanguageID)
                Case 1036, 2060, 3084 'fr
                    Message = "Remplacer la cha�ne xx-XX par un code langue BCP47."
                    Title = "Choix de la langue"
                Case 1034, 3082, 11274, 2058, 9226, 1050 'es
                    Message = "Sustituya la cadena xx-XX insertada por un c�digo de idioma BCP47."
                    Title = "Establecer idioma"
                Case Else 'en
                    Message = "Please replace the string xx-XX inserted with a BCP47 language code."
                    Title = "Set language"
            End Select
    
            'Message = "Please replace the string xx-XX inserted with a BCP47 language code"
            'Title = "Set language"
            Default = "xx-XX"
            MyValue = InputBox(Message, Title, Default)
            ActiveDocument.CustomDocumentProperties("language").Delete
            ActiveDocument.CustomDocumentProperties.Add _
                Name:="language", _
                LinkToContent:=False, _
                Type:=msoPropertyTypeString, _
                Value:=MyValue
            'MsgBox "Result =" + MyValue
        Else
            languageCode = result
            ActiveDocument.CustomDocumentProperties("language").Delete
            ActiveDocument.CustomDocumentProperties.Add _
        Name:="language", LinkToContent:=False, Type:=msoPropertyTypeString, Value:=languageCode
        End If
        On Error Resume Next
    Else
        Select Case Application.International(wdProductLanguageID)
            Case 1036, 2060, 3084 'fr
                 MsgBox "Une langue est requise."
                 'Call initialize
            Case 1034, 3082, 11274, 2058, 9226, 1050 'es
                 MsgBox "Se necesita un idioma."
            Case Else 'en
                 MsgBox "A language is required."
        End Select
    End If
    
    'Typage z39-98 du document
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload typez39_fr
        Load typez39_fr
        typez39_fr.Show
        selectedItem = typez39_fr.dropdown_typez39.Value
        Start = InStr(typez39_fr.dropdown_typez39, "[")
        EndS = InStr(typez39_fr.dropdown_typez39, "]")
        resulttype = Mid(selectedItem, Start + 1, EndS - Start - 1)
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload typez39_es
        Load typez39_es
        typez39_es.Show
        selectedItem = typez39_es.dropdown_typez39.Value
        Start = InStr(typez39_es.dropdown_typez39, "[")
        EndS = InStr(typez39_es.dropdown_typez39, "]")
        resulttype = Mid(selectedItem, Start + 1, EndS - Start - 1)
    Case Else 'en
        Unload typez39_en
        Load typez39_en
        typez39_en.Show
        selectedItem = typez39_en.dropdown_typez39.Value
        Start = InStr(typez39_en.dropdown_typez39, "[")
        EndS = InStr(typez39_en.dropdown_typez39, "]")
        resulttype = Mid(selectedItem, Start + 1, EndS - Start - 1)
    End Select
    
    'MsgBox "Resulttype =" + resulttype
    
    ActiveDocument.CustomDocumentProperties("typez39").Delete
    ActiveDocument.CustomDocumentProperties.Add _
        Name:="typez39", LinkToContent:=False, Type:=msoPropertyTypeString, Value:=resulttype
    
    myRibbon.Invalidate
    
    'r�cup' OE : � conserver ?
    'ActiveDocument.UpdateStyles

End Sub
Sub SetMetopesDocumentProperty()
    Call SetCustomDocumentProperty(Name_:="tplVersion", Value:="1.0")
    Call SetCustomDocumentProperty(Name_:="source", Value:="Metopes")
End Sub
Sub SetCustomDocumentProperty(Name_ As String, Value)
    For Each Prop In ActiveDocument.CustomDocumentProperties
        If LCase(Prop.Name) = LCase(Name_) Then
            'suppression de la propri�t� ind�pendamment de la casse
            ActiveDocument.CustomDocumentProperties(Prop.Name).Delete
        End If
    Next
    'D�finition de la propri�t� (nouvelle ou qui vient d'�tre supprim�e)
    ActiveDocument.CustomDocumentProperties.Add _
        Name:=Name_, LinkToContent:=False, Type:=msoPropertyTypeString, Value:=Value
End Sub
Sub ApplyStyleLang(styleRootName As String, Optional delimiter As String = "|")
    Dim result As String
    Dim styleName As String
    Dim langiso6391 As String
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload language_selector_fr
        Load language_selector_fr
        language_selector_fr.Show
        result = language_selector_fr.dropdown_language.Value
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload language_selector_es
        Load language_selector_es
        language_selector_es.Show
        result = language_selector_es.dropdown_language.Value
    Case Else 'en
        Unload language_selector_en
        Load language_selector_en
        language_selector_en.Show
        result = language_selector_en.dropdown_language.Value
    End Select
    
    'language_selector.Show
    'result = language_selector.dropdown_language.Value

    If result <> "" And result <> "-------------" Then
        langiso6391 = Split(result, " | ")(0)
        ' If styleRootName ends with '|xx', remove suffix
        If Left(Right(styleRootName, 3), 1) = delimiter Then
          styleRootName = Left(styleRootName, Len(styleRootName) - 3)
        End If
        styleName = styleRootName + delimiter + langiso6391
        On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
        the_style.BaseStyle = ActiveDocument.Styles(styleRootName)
        Selection.paragraphs.style = ActiveDocument.Styles(styleName)
    Else
        Select Case Application.International(wdProductLanguageID)
            Case 1036, 2060, 3084 'fr
                 MsgBox "Une langue est requise."
                 'Call init
            Case 1034, 3082, 11274, 2058, 9226, 1050 'es
                 MsgBox "Se necesita un idioma."
            Case Else 'en
                 MsgBox "A language is required."
        End Select
    End If
End Sub
Sub ApplyStyleLangSuggest(styleRootName As String, Optional delimiter As String = "|")
    Dim result As String
    Dim styleName As String
    Dim langiso6391 As String
    Dim doc As DocumentProperty
    
    On Error Resume Next
    Set doc = ActiveDocument.CustomDocumentProperties("language")
    On Error GoTo 0
    
    If Not doc Is Nothing Then
    MainLanguage = Left(ActiveDocument.CustomDocumentProperties("language").Value, 2)
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        With language_selector_fr.dropdown_language
            If MainLanguage = "fr" Then
                result = language_selector_fr.dropdown_language.Value
                .Value = "fr | fran�ais"
            End If
            If MainLanguage = "en" Then
                result = language_selector_fr.dropdown_language.Value
                .Value = "en | anglais"
            End If
            If MainLanguage = "es" Then
                result = language_selector_fr.dropdown_language.Value
                .Value = "es | espagnol"
            End If
        End With
        Unload language_selector_fr
        Load language_selector_fr
        language_selector_fr.Show
        result = language_selector_fr.dropdown_language.Value
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        With language_selector_es.dropdown_language
            If MainLanguage = "fr" Then
                result = language_selector_es.dropdown_language.Value
                .Value = "fr | French"
            End If
            If MainLanguage = "en" Then
                result = language_selector_es.dropdown_language.Value
                .Value = "en | English"
            End If
            If MainLanguage = "es" Then
                result = language_selector_es.dropdown_language.Value
                .Value = "es | Spanish"
            End If
        End With
        Unload language_selector_es
        Load language_selector_es
        language_selector_es.Show
        result = language_selector_es.dropdown_language.Value
    Case Else 'en
        With language_selector_en.dropdown_language
            If MainLanguage = "fr" Then
                result = language_selector_en.dropdown_language.Value
                .Value = "fr | French"
            End If
            If MainLanguage = "en" Then
                result = language_selector_en.dropdown_language.Value
                .Value = "en | English"
            End If
            If MainLanguage = "es" Then
                result = language_selector_en.dropdown_language.Value
                .Value = "es | Spanish"
            End If
        End With
        Unload language_selector_en
        Load language_selector_en
        language_selector_en.Show
        result = language_selector_en.dropdown_language.Value
    End Select
    

    If result <> "" And result <> "-------------" Then
        langiso6391 = Split(result, " | ")(0)
        ' If styleRootName ends with '|xx', remove suffix
        If Left(Right(styleRootName, 3), 1) = delimiter Then
          styleRootName = Left(styleRootName, Len(styleRootName) - 3)
        End If
        styleName = styleRootName + delimiter + langiso6391
        On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
        the_style.BaseStyle = ActiveDocument.Styles(styleRootName)
        Selection.paragraphs.style = ActiveDocument.Styles(styleName)
    Else
        Select Case Application.International(wdProductLanguageID)
            Case 1036, 2060, 3084 'fr
                 MsgBox "Une langue est requise."
                 'Call initialize
            Case 1034, 3082, 11274, 2058, 9226, 1050 'es
                 MsgBox "Se necesita un idioma."
            Case Else 'en
                 MsgBox "A language is required."
        End Select
    End If
    Else
        Select Case Application.International(wdProductLanguageID)
            Case 1036, 2060, 3084 'fr
                 MsgBox "Veuillez d�abord renseigner la langue du document (Config. env.)"
                 'Call initialize
            Case 1034, 3082, 11274, 2058, 9226, 1050 'es
                 MsgBox "Rellene primero la lengua del documento (Config. env.)."
            Case Else 'en
                 MsgBox "Please fill in the language of the document first (Config. env.)."
        End Select
    End If
End Sub
' Apply language suffix, except for Headings
Sub style_language(control As IRibbonControl)
    Dim SelParaFirst As Paragraph
    Set SelParaFirst = Selection.paragraphs.First
    Dim current_style_name As String
    If SelParaFirst.OutlineLevel > 9 Or SelParaFirst.OutlineLevel < 1 Then
        current_style_name = ActiveDocument.ActiveWindow.Selection.style
        Call ApplyStyleLang(current_style_name, "|")
    Else
        Select Case Application.International(wdProductLanguageID)
            Case 1036, 2060, 3084 'fr
                 MsgBox "Le suffixe de langue n'est pas disponible pour les niveaux de titres."
                 'Call initialize
            Case 1034, 3082, 11274, 2058, 9226, 1050 'es
                 MsgBox "El sufijo de idioma no est� disponible para los t�tulos."
            Case Else 'en
                 MsgBox "Language suffix is not available for Headings."
        End Select
        MsgBox ""
    End If
End Sub

Sub init(control As IRibbonControl)
   initialize
End Sub

'Sub AutoNew()
    'Applies to new documents you create
  '  initialize
'End Sub

'Sub AutoOpen()
    'Applies to existing documents you open
'    initialize
'End Sub

Function paragraphs()
     If apply_style_to_selection = False Then
        Selection.Expand wdParagraph
    End If
    
    Set paragraphs = Selection
End Function
Sub format_date(style_name)
' If the string match a date, format and apply style_name

 Dim k As String
 Dim DateValue As Date
 k = paragraphs.Text

 If Selection.paragraphs(1).Range.Characters.Count = 1 Then
   Select Case Application.International(wdProductLanguageID)
       Case 1036, 2060, 3084 'fr
           MsgBox "Veuillez d�abord saisir une date."
       Case 1034, 3082, 11274, 2058, 9226, 1050 'es
           MsgBox "Por favor, introduzca primero una fecha."
       Case Else 'en
           MsgBox "Please first enter a date."
   End Select
   
   ElseIf InStr(k, ":") <> 0 Then
   dateAfterDot = Split(k, ":")(1)
   DateValue = CDate(dateAfterDot)
   Selection.EndKey Unit:=wdLine
   'Selection.MoveEnd Unit:=wdCharacter, Count:=-1
   'Selection.MoveRight Unit:=wdCharacter, Count:=1
   l = Format(DateValue, "DD/MM/YYYY") '& Chr(13)
   Selection.InsertAfter Text:="[" & l & "]"
   With Selection
    .style = "TEI_date_normalised"
   End With
   paragraphs.style = ActiveDocument.Styles(style_name)
  
 ElseIf IsDate(k) Then
   DateValue = CDate(k)
  
    Selection.EndKey Unit:=wdLine
    'Selection.MoveRight Unit:=wdCharacter, Count:=1
    l = Format(DateValue, "DD/MM/YYYY") '& Chr(13)
    Selection.InsertAfter Text:="[" & l & "]"
    With Selection
    .style = "TEI_date_normalised"
    End With
    paragraphs.style = ActiveDocument.Styles(style_name)
    'MsgBox "The date will be formatted as: " & Format(DateValue, "DD/MM/YYYY") & " (dd/mm/yyyy)"
    
 Else
   Select Case Application.International(wdProductLanguageID)
       Case 1036, 2060, 3084 'fr
           MsgBox "Veuillez v�rifier que le paragraphe contient une date."
       Case 1034, 3082, 11274, 2058, 9226, 1050 'es
           MsgBox "Compruebe que el p�rrafo contiene una fecha."
       Case Else 'en
           MsgBox "This is not a date. Please check."
   End Select
 End If
End Sub

'GetImage, callback pour changer de fa�on dynamique l'ic�ne du bouton (requiert actualisation du ruban via myRibbon.Invalidate dans macro init)
Sub GetImage(control As IRibbonControl, ByRef image)
    Dim source As DocumentProperty, present As Boolean

    For Each source In ActiveDocument.CustomDocumentProperties
    If source.Name = "source" Then
    present = True
        Select Case control.ID
        Case "init"
        image = "AcceptTask"
        End Select
    Exit For
    End If
    Next source
    If Not present Then
        Select Case control.ID
        Case "init"
        image = "DeclineTask"
        End Select
    End If
End Sub

Sub section(start_style As String, start_text As String, end_style As String, end_text As String)
    
    Dim SelRangeFirst As Range
    Dim SelRangeLast As Range
    Dim PrevRange As Range
    Dim NextRange As Range
    Dim oTable As Table
    Dim oRng As Range
    
    Selection.Expand wdParagraph
    Set SelRangeFirst = Selection.paragraphs.First.Range
    Set SelRangeLast = Selection.paragraphs.Last.Range

    ' Le 1er paragraphe de la s�lection est dans un tableau
    If SelRangeFirst.Information(wdWithInTable) = True Then
        Set oTable = SelRangeFirst.Tables(1)
        Set oRng = oTable.Range
        oRng.Select
        Selection.SplitTable
        Set PrevRange = oRng.paragraphs.First.Range
    ' La 1er paragraphe de la s�lection n'est pas dans un tableau
    Else
        SelRangeFirst.InsertParagraphBefore
        Set PrevRange = SelRangeFirst.paragraphs.First.Range
    End If
     ' Le dernier paragraphe de la s�lection est dans un tableau
    If SelRangeLast.Information(wdWithInTable) = True Then
        Set oTable = SelRangeLast.Tables(1)
        Set oRng = oTable.Range
        oRng.Collapse wdCollapseEnd
        oRng.InsertParagraphAfter
        Set NextRange = oRng.paragraphs.Last.Range
    ' Le dernier paragraphe de la s�lection n'est pas dans un tableau
    Else
        If Len(SelRangeLast) > 2 Then
            SelRangeLast.End = SelRangeLast.End - 1
            SelRangeLast.InsertParagraphAfter
            Set NextRange = SelRangeLast.paragraphs.Last.Next.Range
        Else ' le dernier paragraphe contient uniquement une image : Len(SelRangeLast)==2
            SelRangeLast.InsertParagraphAfter
            Set NextRange = SelRangeLast.paragraphs.Last.Range
        End If
    End If

    PrevRange.style = ActiveDocument.Styles(start_style)
    ActiveDocument.Range(PrevRange.Start, PrevRange.End - 1).Text = start_text
    
    NextRange.style = ActiveDocument.Styles(end_style)
    ActiveDocument.Range(NextRange.Start, NextRange.End - 1).Text = end_text
    
    Set oTable = Nothing
    Set oRng = Nothing
    Set SelRangeFirst = Nothing
    Set SelRangeLast = Nothing
    Set PrevRange = Nothing
    Set NextRange = Nothing
End Sub

Sub sectionFT(start_style As String, start_text As String, end_style As String, end_text As String)
    
    Dim SelRangeFirst As Range
    Dim SelRangeLast As Range
    Dim PrevRange As Range
    Dim NextRange As Range
    Dim oTable As Table
    Dim oRng As Range
    Dim answer As Integer
    
    Selection.Expand wdParagraph
    Set SelRangeFirst = Selection.paragraphs.First.Range
    Set SelRangeLast = Selection.paragraphs.Last.Range

    ' Le 1er paragraphe de la s�lection est dans un tableau
    If SelRangeFirst.Information(wdWithInTable) = True Then
        Set oTable = SelRangeFirst.Tables(1)
        Set oRng = oTable.Range
        oRng.Select
        Selection.SplitTable
        Set PrevRange = oRng.paragraphs.First.Range
    ' La 1er paragraphe de la s�lection n'est pas dans un tableau
    Else
        SelRangeFirst.InsertParagraphBefore
        Set PrevRange = SelRangeFirst.paragraphs.First.Range
    End If
     ' Le dernier paragraphe de la s�lection est dans un tableau
    If SelRangeLast.Information(wdWithInTable) = True Then
        Set oTable = SelRangeLast.Tables(1)
        Set oRng = oTable.Range
        oRng.Collapse wdCollapseEnd
        oRng.InsertParagraphAfter
        Set NextRange = oRng.paragraphs.Last.Range
    ' Le dernier paragraphe de la s�lection n'est pas dans un tableau
    Else
        If Len(SelRangeLast) > 2 Then
            SelRangeLast.End = SelRangeLast.End - 1
            SelRangeLast.InsertParagraphAfter
            Set NextRange = SelRangeLast.paragraphs.Last.Next.Range
        Else ' le dernier paragraphe contient uniquement une image : Len(SelRangeLast)==2
            SelRangeLast.InsertParagraphAfter
            Set NextRange = SelRangeLast.paragraphs.Last.Range
        End If
    End If

    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        FTMsg = MsgBox("Voulez-vous affecter un type � cet encadr� ?", vbYesNo + vbQuestion, "Confirmation")
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        FTMsg = MsgBox("Teclar un type por el cuaddro?", vbYesNo + vbQuestion, "Confirmation")
    Case Else 'anglais par d�faut
        FTMsg = MsgBox("Do you want to set a type to this boxed text?", vbYesNo + vbQuestion, "Confirmation")
    End Select
    
    answer = FTMsg
    
    If answer = vbYes Then
        Select Case Application.International(wdProductLanguageID)
            Case 1036, 2060, 3084 'fr
                Unload typetext_fr
                Load typetext_fr
                typetext_fr.Show
                resultCustom = "@" + typetext_fr.TextBox.Value
            Case 1034, 3082, 11274, 2058, 9226, 1050 'es
                Unload typetext_es
                Load typetext_es
                typetext_es.Show
                resultCustom = "@" + typetext_es.TextBox.Value
            Case Else 'anglais par d�faut
                Unload typetext_en
                Load typetext_en
                typetext_en.Show
                resultCustom = "@" + typetext_en.TextBox.Value
         End Select
    Else
        resultCustom = ""
    End If

    PrevRange.style = ActiveDocument.Styles(start_style)
    start_textType = start_text + resultCustom
    ActiveDocument.Range(PrevRange.Start, PrevRange.End - 1).Text = start_textType
    
    NextRange.style = ActiveDocument.Styles(end_style)
    ActiveDocument.Range(NextRange.Start, NextRange.End - 1).Text = end_text
    
    Set oTable = Nothing
    Set oRng = Nothing
    Set SelRangeFirst = Nothing
    Set SelRangeLast = Nothing
    Set PrevRange = Nothing
    Set NextRange = Nothing
End Sub

Sub ShowSections(control As IRibbonControl)
    If ActiveDocument.Styles("TEI_figure_start").Font.Size = 1 Then
    With ActiveDocument
        'With .Styles("TEI_appendix_start")
        '    .Font.Size = 8
        '    .Font.TextColor = RGB(113, 142, 172)
        'End With
        'With .Styles("TEI_bibl_start")
        '    .Font.Size = 8
        '    .Font.TextColor = RGB(113, 142, 172)
        'End With
        With .Styles("TEI_code_start")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
        With .Styles("TEI_code_end")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
        With .Styles("TEI_figure_start")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
        With .Styles("TEI_figure_end")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
       ' With .Styles("TEI_floatingText_start")
       '     .Font.Size = 11
       '     .Font.TextColor = RGB(113, 142, 172)
       ' End With
       ' With .Styles("TEI_floatingText_end")
       '     .Font.Size = 11
       '     .Font.TextColor = RGB(113, 142, 172)
       ' End With
        With .Styles("TEI_linguistic_start")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
        With .Styles("TEI_linguistic_end")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
        With .Styles("TEI_quote_start")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
         With .Styles("TEI_quote_end")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
        With .Styles("TEI_review_start")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
         With .Styles("TEI_review_end")
            .Font.Size = 8
            .Font.TextColor = RGB(113, 142, 172)
        End With
    End With
    Else
    With ActiveDocument
        'With .Styles("TEI_appendix_start")
        '    .Font.Size = 1
        '    .Font.ColorIndex = wdWhite
        'End With
        'With .Styles("TEI_bibl_start")
        '    .Font.Size = 1
        '    .Font.ColorIndex = wdWhite
        'End With
        With .Styles("TEI_code_start")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
        With .Styles("TEI_code_end")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
        With .Styles("TEI_figure_start")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
        With .Styles("TEI_figure_end")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
       ' With .Styles("TEI_floatingText_start")
       '     .Font.Size = 1
       '     .Font.ColorIndex = wdWhite
       ' End With
       ' With .Styles("TEI_floatingText_end")
       '     .Font.Size = 1
       '     .Font.ColorIndex = wdWhite
       ' End With
        With .Styles("TEI_linguistic_start")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
        With .Styles("TEI_linguistic_end")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
        With .Styles("TEI_quote_start")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
        With .Styles("TEI_quote_end")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
        With .Styles("TEI_review_start")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
         With .Styles("TEI_review_end")
            .Font.Size = 1
            .Font.ColorIndex = wdWhite
        End With
     End With
    End If
End Sub

Sub AccessGlobalTemplate()
    Dim template As template
    Dim attachedTemplateName As String
    Dim selectedGlobalTemplates As String
    Dim templateFolder As String
    Dim customTemplatePath As String
    Dim preAttachedTemplate As String
    
    templateFolder = Left(Application.Options.DefaultFilePath(wdUserTemplatesPath), _
                Len(Application.Options.DefaultFilePath(wdUserTemplatesPath)))
    preAttachedTemplate = ActiveDocument.AttachedTemplate
    
    selectedGlobalTemplates = ""
    
     If InStr(preAttachedTemplate, "Commons") > 0 Then
           'MsgBox "Un mod�le Commons est d�j� attach� au document"
        Else
            For Each template In Application.Templates
            attachedTemplateName = template.Name
            If attachedTemplateName Like "Commons*" Then
                selectedGlobalTemplates = attachedTemplateName
                'MsgBox "attachedTemplateName: " & attachedTemplateName
            End If
        Next template
    
        If selectedGlobalTemplates <> "" Then
        
            #If Mac Then
                customTemplatePath = templateFolder & selectedGlobalTemplates
            #Else
                customTemplatePath = templateFolder & "\" & selectedGlobalTemplates
            #End If
            
            'MsgBox "customTemplatePath = " & customTemplatePath
            
            'Nettoyage du champ du mod�le
            ActiveDocument.AttachedTemplate = ""
            'Copie du modle global vers le champ du mod�le attach� au document actif
            ActiveDocument.AttachedTemplate = customTemplatePath
            'Coche l'option mise � jour automatique des styles de document
            ActiveDocument.UpdateStylesOnOpen = True
            'Et copier-coller des styles du mod�le li� vers le document actif
            ActiveDocument.CopyStylesFromTemplate _
 template:=ActiveDocument.AttachedTemplate.FullName
        Else
    MsgBox "Please select a Commons template."
        End If
    End If
End Sub

