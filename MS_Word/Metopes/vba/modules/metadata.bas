Attribute VB_Name = "metadata"
Sub keywords(control As IRibbonControl)
    ApplyStyleLangSuggest "TEI_keywords"
End Sub
Sub date_reception(control As IRibbonControl)
    Call format_date(style_name:="TEI_date_reception")
End Sub
Sub date_acceptance(control As IRibbonControl)
    Call format_date(style_name:="TEI_date_acceptance")
End Sub
Sub pagination(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_pagination")
End Sub
Sub documentnumber(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_document_number")
End Sub
Sub funder(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_funder")
End Sub
Sub funderName(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_fundername_inline")
End Sub
Sub funderRef(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_funderref_inline")
End Sub
Sub keywords_subjects_chronology(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:chronology")
End Sub
Sub keywords_subjects_geography(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:geography")
End Sub
Sub keywords_subjects_subject(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:subject")
End Sub
Sub keywords_subjects_work(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:work")
End Sub
Sub keywords_subjects_propernoun(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:propernoun")
End Sub
Sub keywords_subjects_personcited(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:personcited")
End Sub
