Attribute VB_Name = "front"
Sub abstract(control As IRibbonControl)
    ApplyStyleLangSuggest "TEI_abstract"
End Sub
Sub paragraph_lead(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_paragraph_lead")
End Sub
Sub epigraph(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_epigraph")
End Sub
Sub acknowledgment(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_acknowledgment")
End Sub
Sub dedication(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_dedication")
End Sub
Sub partenaires(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_partner")
End Sub
Sub lienData(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_linked_data")
End Sub
Sub lienPubli(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_linked_publi")
End Sub
Sub note_front(control As IRibbonControl)
    Dim result As String
    Dim styleName As String
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload ndl_selector_fr
        Load ndl_selector_fr
        ndl_selector_fr.Show
        result1 = Right(ndl_selector_fr.dropdown_ndl.Value, 4)
        result = Left(result1, 3)
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload ndl_selector_es
        Load ndl_selector_es
        ndl_selector_es.Show
        result1 = Right(ndl_selector_es.dropdown_ndl.Value, 4)
        result = Left(result1, 3)
    Case Else 'anglais par d�faut
        Unload ndl_selector_en
        Load ndl_selector_en
        ndl_selector_en.Show
        result1 = Right(ndl_selector_en.dropdown_ndl.Value, 4)
        result = Left(result1, 3)
    End Select
    
    'MsgBox "Result =" + result
    
    If result = "" Then
    Else
    styleName = "TEI_note:" + result
    On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
        With the_style
                .BaseStyle = ActiveDocument.Styles("TEI_note:")
        End With
    Selection.paragraphs.style = ActiveDocument.Styles(styleName)
    End If
    
End Sub

