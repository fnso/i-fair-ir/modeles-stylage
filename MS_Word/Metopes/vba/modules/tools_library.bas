Attribute VB_Name = "tools_library"
Sub messages_ok(texte_message As String)
    'If messages_actives = True Then
    Call MsgBox(texte_message, vbInformation)
    'End If
End Sub

Sub messages_erreur(texte_message As String)
    Call MsgBox(texte_message, vbExclamation, "Erreur")
End Sub

Sub refresh()
    nom_macro = "refresh"
    Application.ScreenRefresh
    Repeat 10
End Sub

Sub nettoyage_recherche_remplace()
    nom_macro = "nettoyage_recherche_remplace"
    'Remise a zero des paramtres de recherche et de remplacementt
    Selection.Find.ClearFormatting 'Nettoyage de la zone "rechercher"
    Selection.Find.Replacement.ClearFormatting 'Nettoyage de la zone "remplacer"
    With Selection.Find
        .Text = ""
        .Replacement.Text = "^&"
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
End Sub

Sub desactive_revision()
    nom_macro = "desactive_revision"
    'Macro de d�sactivation du mode r�vision
    If ActiveDocument.TrackRevisions = True Then
       ActiveDocument.TrackRevisions = False
    End If
End Sub

Sub rechercheremplace(textecherche As String, texteremplace As String)
    nom_macro = "rechercheremplace"
    'Macro rechercher/remplacer
    'Utilisation dans d'autres macros, pour r_duire la taille du code :
    With Selection.Find
        .Text = textecherche
        .Replacement.Text = texteremplace
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .Execute Replace:=wdReplaceAll
    End With
End Sub

Sub rechercheremplaceER(textecherche As String, texteremplace As String)
    nom_macro = "rechercheremplaceER"
    'Macro rechercher/remplacer utilisant des expressions r_guliress
    With Selection.Find
        .Text = textecherche
        .Replacement.Text = texteremplace
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .MatchSoundsLike = False
        .MatchWildcards = True
        .Execute Replace:=wdReplaceAll
    End With
End Sub

Sub rechercheremplaceERboucle(textecherche As String, texteremplace As String)
    nom_macro = "rechercheremplaceERboucle"
    'Macro rechercher/remplacer utilisant une boucle et des expressions r�guli�res
    With Selection.Find
        .Text = textecherche
        .Replacement.Text = texteremplace
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .MatchSoundsLike = False
        .MatchWildcards = True
        .Execute Replace:=wdReplaceAll
        Do While Selection.Find.Execute
            'recherche jusqu'� ce que plus rien ne soit trouv�
            .Execute Replace:=wdReplaceAll
        Loop
    End With
End Sub

Sub activer_affichage_normal()
    nom_macro = "activer_affichage_normal"
    'Sous-macro permettant d'afficher une page en mode normal
    If ActiveWindow.View.Type = wdNormalView Then           'Si le mode normal est active, alors aucune action n'est necessaire
    Else
        ActiveWindow.ActivePane.View.Type = wdNormalView    'S'il n'est pas activ_, activation.
    End If
End Sub




''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT NETTOYAGE SURLIGNEMENT ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub nettoyage_surlignement()
    nom_macro = "nettoyage_surlignement"
    'Macro supprimant tout surlignage dans un document
    On Error Resume Next
    Call desactive_revision
    Call nettoyage_recherche_remplace
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
        ActiveWindow.Panes(1).Activate
    End If
    Selection.HomeKey Unit:=wdStory
    Selection.Find.Highlight = True
    Selection.Find.Replacement.Highlight = False
    Call rechercheremplace("", "")
    Call nettoyage_recherche_remplace
    
    'nettoyage dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
            Selection.HomeKey Unit:=wdStory
            Selection.Find.Highlight = True
            Selection.Find.Replacement.Highlight = False
            Call rechercheremplace("", "")
            Call nettoyage_recherche_remplace
        End If
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN NETTOYAGE SURLIGNEMENT ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT ESPACES INSECABLES ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub Espace_insecables_simple()
    nom_macro = "Espace_insecables_simple"
    Call Espaces_insecables(var_batch:=False)
End Sub

Sub Espaces_insecables(var_batch As Boolean)
    nom_macro = "Espaces_insecables"
    ' R�tablissement des ins�cables devant les signes de ponctuation double
    Call desactive_revision
    Call nettoyage_recherche_remplace
    Call activer_affichage_normal
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
        ActiveWindow.Panes(1).Activate
    End If
    Selection.HomeKey Unit:=wdStory
    
    Call retablissement_esp_ins
    Call nettoyage_insec_appel

    'R�tablir les espaces ins�cables dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call retablissement_esp_ins
        End If
    End If
    If var_batch = False Then
        Call messages_ok(texte_message:="Les actions suivantes ont �t� effectu�es :" & vbCrLf & vbCrLf & "   * r�tablissement des espaces ins�cables devant les signes de ponctuation double ;" & vbCrLf & "   * suppression des espaces ins�cables dans les passages en anglais ;" & vbCrLf & "   * suppression des espaces doubles.")
    End If
End Sub

Sub retablissement_esp_ins()
    nom_macro = "retablissement_esp_ins"
    'Faire un nettoyage : supprimer tous les ins�cables devant les signes de ponctuation doubles
    Call nettoyage_recherche_remplace
    Call rechercheremplaceER("^s([\?\;\:\!\%\=])", "\1")
    'Macro de r�tablissement des espaces ins�cables
    Selection.HomeKey Unit:=wdStory
    Call desactive_revision
    'Ajout d'espaces ins�cables
    Call nettoyage_recherche_remplace
    Call rechercheremplaceER("([\?\;\:\!\�\%\=])", "^s\1") 'Ajoute une espace ins�cable devant ? ; : ! � % et =
    Call rechercheremplace("�", "�^s")
    Call rechercheremplace("�^s�", "��^s") 'R�pare l'inversion des guillemets qui suivent une apostrophe
    Call rechercheremplace("��", "��^s")   'R�pare l'inversion des guillemets qui suivent une apostrophe
    Call rechercheremplace("n�", "n�^s")
    Call rechercheremplace("op. cit.", "op.^scit.")
    Call rechercheremplace("art. cit.", "art.^scit.")
    Call rechercheremplace("Art. cit.", "Art.^scit.")
    Call rechercheremplace("pp.", "pp.^s")
    Call rechercheremplace("(p.", "(p.^s")
    Call rechercheremplace(" vol.", " vol.^s")
    Call rechercheremplace("vol. ", "vol.^s")
    Call rechercheremplace("fig. ", "fig.^s")
    Call rechercheremplace("ill. ", "ill.^s")
    'Remplacement des espaces normales par des espaces ins�cables dans les nombres � plus de 4 chiffres
    Call rechercheremplaceER("([0-9]) ([0-9])([0-9])([0-9])", "\1^s\2\3\4")
    'Remplacement des "p." et "t." (abbr�viations de "pages" et "tome" : une espace ins�cable doit s�parer le chiffre de l'abbr�viation)
    'lignes sp�cifiques de code en utilisant des expressions r�guli�res :
    '* application de la macro uniquement aux abbr�viations suivies ou pr�c�d�es d'un chiffre, sinon cela pose un probl�me dans le cas o? "p." ou "t." sont des abbr�viations de noms
    '* le mode "utiliser les caract�res g�n�riques" (expressions r�guli�res) permet de conserver la casse des abbr�viations (majuscules ou minuscules)
    Call rechercheremplaceER("([0-9]) p.", "\1^sp.")
    Call rechercheremplaceER("([0-9]) t.", "\1^st.")
    Call rechercheremplaceER(" p. ([0-9])", " p.^s\1")
    Call rechercheremplaceER(" t. ([0-9])", " t.^s\1")
    Call rechercheremplaceER(" t.([0-9])", " t.^s\1")
    Call rechercheremplaceER(" p.([0-9])", " p.^s\1")
    Call rechercheremplaceER(" t.([0-9])", " t.^s\1")
    Call rechercheremplaceER(" p.  ([0-9])", " p.^s\1")
    Call rechercheremplaceER(" t.  ([0-9])", " t.^s\1")
    'Espaces ins�cables pour les signes mon�taires $, �, �, francs (= ChrW(8355) ), pesetas (= ChrW(8359) ), shekel (= ChrW(8362) ), dong (= ChrW(8363) ), livres (= ChrW(8356) )
    'recherche symbole mon�taire apr�s une s�rie de chiffres
    Call rechercheremplaceER("([0-9]) $", "\1^s$")
    Call rechercheremplaceER("([0-9]) �", "\1^s�")
    Call rechercheremplaceER("([0-9]) �", "\1^s�")
    Call rechercheremplaceER("([0-9]) " & ChrW(8355), "\1^s" & ChrW(8355)) 'fr
    Call rechercheremplaceER("([0-9]) " & ChrW(8359), "\1^s" & ChrW(8359)) 'pts
    Call rechercheremplaceER("([0-9]) " & ChrW(8362), "\1^s" & ChrW(8362)) 'shekel
    Call rechercheremplaceER("([0-9]) " & ChrW(8363), "\1^s" & ChrW(8363)) 'dong
    Call rechercheremplaceER("([0-9]) " & ChrW(8356), "\1^s" & ChrW(8356)) 'livres
    Call rechercheremplaceER("([0-9])$", "\1^s$")
    Call rechercheremplaceER("([0-9])�", "\1^s�")
    Call rechercheremplaceER("([0-9])�", "\1^s�")
    Call rechercheremplaceER("([0-9])" & ChrW(8355), "\1^s" & ChrW(8355)) 'fr
    Call rechercheremplaceER("([0-9])" & ChrW(8359), "\1^s" & ChrW(8359)) 'pts
    Call rechercheremplaceER("([0-9])" & ChrW(8362), "\1^s" & ChrW(8362)) 'shekel
    Call rechercheremplaceER("([0-9])" & ChrW(8363), "\1^s" & ChrW(8363)) 'dong
    Call rechercheremplaceER("([0-9])" & ChrW(8356), "\1^s" & ChrW(8356)) 'livres
    'recherche symbole mon�taire avant une s�rie de chiffres
    Call rechercheremplaceER("$ ([0-9])", "$^s\1")
    Call rechercheremplaceER("� ([0-9])", "�^s\1")
    Call rechercheremplaceER("� ([0-9])", "�^s\1")
    Call rechercheremplaceER(ChrW(8355) & " ([0-9])", ChrW(8355) & "^s\1") 'fr
    Call rechercheremplaceER(ChrW(8359) & " ([0-9])", ChrW(8359) & "^s\1") 'pts
    Call rechercheremplaceER(ChrW(8362) & " ([0-9])", ChrW(8362) & "^s\1") 'shekel
    Call rechercheremplaceER(ChrW(8363) & " ([0-9])", ChrW(8363) & "^s\1") 'dong
    Call rechercheremplaceER(ChrW(8356) & " ([0-9])", ChrW(8356) & "^s\1") 'livres
    Call rechercheremplaceER("$([0-9])", "$^s\1")
    Call rechercheremplaceER("�([0-9])", "�^s\1")
    Call rechercheremplaceER("�([0-9])", "�^s\1")
    Call rechercheremplaceER(ChrW(8355) & "([0-9])", ChrW(8355) & "^s\1") 'fr
    Call rechercheremplaceER(ChrW(8359) & "([0-9])", ChrW(8359) & "^s\1") 'pts
    Call rechercheremplaceER(ChrW(8362) & "([0-9])", ChrW(8362) & "^s\1") 'shekel
    Call rechercheremplaceER(ChrW(8363) & "([0-9])", ChrW(8363) & "^s\1") 'dong
    Call rechercheremplaceER(ChrW(8356) & "([0-9])", ChrW(8356) & "^s\1") 'livres
    'Remplacement des espaces doubles
    Call rechercheremplaceER("  ([\?\;\:\!\�\%\=])", "^s\1")
    Call rechercheremplace("^s ", "^s")
    Call rechercheremplace(" ^s", "^s")
    Call rechercheremplace("  ", " ")
    Call rechercheremplace("n�  ", "n�^s")
    Call rechercheremplace("pp.  ", "pp.^s")
    Call rechercheremplace(" pp.  ", " pp.^s")
    Call rechercheremplace("^s^s", "^s")
    Call nettoyage_recherche_remplace
    
    Call rechercheremplace("^s^s", "^s")
    Call rechercheremplace("^s://", "://") 'http :// https ://, etc.
    Call espaces_insecables_english
     
End Sub


Sub espaces_insecables_english() '
    nom_macro = "espaces_insecables_english"
    'R�tablir les espaces ins�cables en anglais
    Call desactive_revision
    Call nettoyage_recherche_remplace
    Selection.Find.LanguageID = wdEnglishUK
    Call rechercheremplaceERboucle("^s([\?\;\:\!\%\=])", "\1")
    Call nettoyage_recherche_remplace
    Selection.Find.LanguageID = wdEnglishUS
    Call rechercheremplaceERboucle("^s([\?\;\:\!\%\=])", "\1")
    Call nettoyage_recherche_remplace
End Sub

Sub nettoyage_insec_appel()
    nom_macro = "nettoyage_insec_appel"
    Selection.HomeKey Unit:=wdStory 'Placement du curseur au d_but du texte
    Call rechercheremplaceER(textecherche:=" (^2)", texteremplace:="\1")
    Call nettoyage_recherche_remplace
    Call rechercheremplaceER(textecherche:="^s(^2)", texteremplace:="\1")
    Call nettoyage_recherche_remplace
    Selection.HomeKey Unit:=wdStory
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN ESPACES INSECABLES ''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT APOSTROPHES '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Sub apostrophe()
    nom_macro = "apostrophe"
    'Macro de remplacement des apostrophes verticales par des apostrophes typographiques
    'Utilis�e comme sous-macro des macros apostrophe_cdt (correction dans le corps de texte) et apostrophe_notes (dans les notes)
     Call rechercheremplaceER("'", "�")
     Call rechercheremplace("�^s�", "��^s") 'R�pare l'inversion des guillemets qui suivent une apostrophe
     Call rechercheremplace("��", "��^s")   'R�pare l'inversion des guillemets qui suivent une apostrophe
     Call nettoyage_recherche_remplace
End Sub

Sub apostrophe_cdt(var_batch As Boolean)
    nom_macro = "apostrophe_cdt"
    Call desactive_revision
    Call nettoyage_recherche_remplace
    Call activer_affichage_normal
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
        ActiveWindow.Panes(1).Activate
    End If
    Selection.HomeKey Unit:=wdStory
    Call apostrophe
    
    'Corriger les apostrophes dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call apostrophe
        End If
    End If
    
    If var_batch = False Then
        Call messages_ok("Les apostrophes verticales ont �t� remplac�es par des apostrophes typographiques dans le corps de texte et dans les notes.")
    End If
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN APOSTROPHES '''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT DOUBLES SAUTS PARAGRAPHES '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub recherche_remplace_double_paragraphe()
    nom_macro = "recherche_remplace_double_paragraphe"
    Call desactive_revision
    
    Selection.HomeKey Unit:=wdStory
    Call rechercheremplaceER("[\^013]{2;}", "^p")
    Selection.EndKey Unit:=wdStory
    Selection.Delete Unit:=wdCharacter, Count:=1
    Selection.HomeKey Unit:=wdStory
    Call rechercheremplaceER("[\^013]{2;}", "^p")
    Call nettoyage_recherche_remplace

End Sub


Sub double_saut_paragraphe()
    nom_macro = "double_saut_paragraphe"
    ' Supprimer les doubles sauts de paragraphe
    Call desactive_revision
    Call nettoyage_recherche_remplace
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then    'V�rifie si les notes de bas de page sont affich�es
        ActiveWindow.Panes(1).Activate                          'Si c'est le cas, placement du curseur dans le corps de texte, pour empcher l'application de la macro aux notes (cela provoque sinon un bug))
    End If
    Selection.HomeKey Unit:=wdStory                             'Placement du curseur au debut du texte
    Call recherche_remplace_double_paragraphe                   'Appel de la sous-macro de remplacement des doubles paragraphes
    Call nettoyage_recherche_remplace
    
     'R�tablir les espaces ins�cables dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Selection.Find.ClearFormatting
            With Selection.Find
                .Text = "^p^p"
                .Replacement.Text = "^&"
                .Forward = True
                .Wrap = wdFindContinue
                .Format = False
                .MatchCase = False
                .MatchWholeWord = False
                .MatchWildcards = False
                .MatchSoundsLike = False
                .MatchAllWordForms = False
                'boucle de recherche et de suppression du saut de paragraphe en trop
                Do While Selection.Find.Execute
                    Selection.MoveLeft Unit:=wdCharacter, Count:=1
                    Selection.Delete Unit:=wdCharacter, Count:=1
                Loop
            End With
        End If
    End If
    
End Sub

Sub double_paragraphe_ndbp_simple()
    nom_macro = "double_paragraphe_ndbp_simple"
    Call double_paragraphe_ndbp(var_batch:=False)
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN DOUBLES SAUTS PARAGRAPHES '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''




''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT GUILLEMETS ''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub Guillemets_anglais_simple()
    nom_macro = "Guillemets_anglais_simple"
    Call Guillemets_anglais(var_batch:=False)
End Sub

Sub Guillemets_anglais(var_batch As Boolean)
    nom_macro = "Gu" 'llemets_anglais"
    'Remplacement des guillemets verticaux en guillemets anglais
    'If Application.Version > "12" Then
    With Options
        .AutoFormatAsYouTypeReplaceQuotes = False
    End With
    'End If
    Selection.HomeKey Unit:=wdStory 'Curseur en d�but de document
    Call nettoyage_recherche_remplace 'Remise � z�ro des param�tres de recherche
    Options.DefaultHighlightColorIndex = wdYellow 'Couleur de surlignage en jaune
    Selection.Find.Replacement.Highlight = True
    Call rechercheremplaceER("""(*)""", "�\1�")
    Selection.Range.HighlightColorIndex = wdYellow
    
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call nettoyage_recherche_remplace 'Remise � z�ro des param�tres de recherche
           Options.DefaultHighlightColorIndex = wdYellow 'Couleur de surlignage en jaune
           Selection.Find.Replacement.Highlight = True
           Call rechercheremplaceER("""(*)""", "�\1�")
           Selection.Range.HighlightColorIndex = wdYellow
        End If
    End If
    
    If var_batch = False Then
        Call messages_ok("Les guillemets verticaux ont �t� remplac�s par des guillemets anglais. Les passages modifi�s ont �t� surlign�s en jaune.")
    End If
    'Call nettoyage_recherche_remplace
End Sub

Sub Guillemets_francais_simple()
    nom_macro = "Guillemets_francais_simple"
    Call Guillemets_francais(var_batch:=False)
End Sub

Sub Guillemets_francais(var_batch As Boolean)
    nom_macro = "Guillemets_francais"
    Selection.HomeKey Unit:=wdStory
    'If Application.Version > "12" Then
    With Options
        .AutoFormatAsYouTypeReplaceQuotes = False
    End With
    'End If
    Call nettoyage_recherche_remplace
    Options.DefaultHighlightColorIndex = wdYellow
    Selection.Find.Replacement.Highlight = True
    Call rechercheremplaceER("""(*)""", "�^s\1^s�")
    Selection.Range.HighlightColorIndex = wdYellow
    
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call nettoyage_recherche_remplace
           Options.DefaultHighlightColorIndex = wdYellow
           Selection.Find.Replacement.Highlight = True
           Call rechercheremplaceER("""(*)""", "�^s\1^s�")
           Selection.Range.HighlightColorIndex = wdYellow
        End If
    End If
    
    If var_batch = False Then
        Call messages_ok("Les guillemets verticaux ont �t� remplac�s par des guillemets francais. Les passages modifi�s ont �t� surlign�s en jaune.")
    End If
    Call nettoyage_recherche_remplace
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN GUILLEMETS ''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT VERIFICATION PARAGRAPHES ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub verification_paragraphe() '(var_batch As Boolean)'
    nom_macro = "verification_paragraphe"
    'Macro de verification des paragraphes
    'Surligne et met en rouge les paragraphes atypiques (ne commenant pas par une majuscule ou ne se terminant pas par un signe de ponctuation))
    'Reglage de la couleur de surlignage sur jaune
    Options.DefaultHighlightColorIndex = wdYellow
    'remise � z_ro des paramtres de recherche et de remplacementt
    Call nettoyage_recherche_remplace
    'Acc_l_ration de l'ex_cution de la macro, en n'affichant pas les modifications au fur et � mesure qu'elles sont effectu_es, mais seulement une fois la macro termin_e
    Word.Application.ScreenUpdating = False
    'Appel de la macro de d_sactivation du mode r_vision (s'il est activ_, la macro ne peut pas fonctionner correctement)
    Call desactive_revision
    'Mise en avant des passages ne se terminant pas par un point
        'surlignage
            Selection.Find.Replacement.Highlight = True
        'couleur de police rouge
            Selection.Find.Replacement.Font.Color = wdColorRed
        'recherche des passages ne se terminant pas par un point
            Call rechercheremplaceER("[ a-zA-Z0-9,;][\^013]", "^&")
    'Supprimer la mise en avant des passages pour lesquels l'absence de point final est normale
        Call verif_exclusion_styles_point
    'Mise en avant des passages commenant par une minusculee
    Call nettoyage_recherche_remplace
    Selection.Find.Replacement.Highlight = True
    Selection.Find.Replacement.Font.Color = wdColorRed
    Call rechercheremplaceER("[\^013][a-z]", "^&")
    Call nettoyage_recherche_remplace
    'Supprimer la mise en avant des passages pour lesquels le commencement par une minuscule est normal
    Call verif_exclusion_styles_minuscule
    'Supprimer le rouge des marques de paragraphe
    Selection.Find.Font.Color = wdColorRed
    Selection.Find.Replacement.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont)
    With Selection.Find
        .Text = "^p"
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    'Boite de dialogue indiquant que la macro s'est appliqu_e correctement
    If var_batch = False Then
        Call messages_ok("Les d�buts de paragraphes commenant par des minuscules et les fins de paragraphes ne se terminant pas par de la ponctuation ont �t� surlign�s en jaune.")
    End If
End Sub

Sub verif_exclusion_styles_point()
    nom_macro = "verif_exclusion_styles_point"
    'Supprimer la mise en avant des passages pour lesquels l'absence de point final est normale
    'Intertitres
    Call verif_exclusion_style(style:="Titre 1", couleurpolice:=wdColorAutomatic) 'wdStyleHeading1
    Call verif_exclusion_style(style:="Titre 2", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 3", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 4", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 5", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 6", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 7", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 8", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 9", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Separateur", couleurpolice:=wdColorAutomatic)
    'Certaines m_tadonn_es
    Call verif_exclusion_style(style:="Titre", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Auteur", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Mots Cles", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Keywords", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Palabrasclaves", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Schlagworter", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Themes", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Periode", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Geographie", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Notice Biblio", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Langue", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Droits Auteur", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Datepublipapier", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Datepubli", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Surtitre", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Sous-titre", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Title (en)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titulo (es)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titolo (it)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titel (de)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titre (fr)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Traducteur", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Editeur scientifique", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Pagination", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Num_ro du document", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titre oeuvre", couleurpolice:=wdColorViolet)
    Call verif_exclusion_style(style:="Auteur oeuvre", couleurpolice:=wdColorViolet)
    Call verif_exclusion_style(style:="Notice Biblio oeuvre", couleurpolice:=wdColorViolet)
    Call verif_exclusion_style(style:="Date publi oeuvre", couleurpolice:=wdColorViolet)
    'certains contenus dans le corps de texte
    Call verif_exclusion_style(style:="Titre Illustration", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Legende Illustration", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Cr_dits Illustration", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Bibliographie", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Code", couleurpolice:=wdColorAutomatic)
End Sub

Sub verif_exclusion_styles_minuscule()
    nom_macro = "verif_exclusion_styles_minuscule"
    'Supprimer la mise en avant des passages pour lesquels le commencement par une minuscule est normal
    Call verif_exclusion_style(style:="Langue", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Mots Cles", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Keywords", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Palabrasclaves", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Schlagworter", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Themes", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Periode", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Geographie", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Code", couleurpolice:=wdColorAutomatic)
End Sub

Sub verif_exclusion_style(style As String, Optional couleurpolice As WdColor)
    nom_macro = "verif_exclusion_style"
    'Sous-macro permettant d'exclure le style d�fini en variable de l'application de la macro verification_paragraphe
    Call nettoyage_recherche_remplace
    On Error GoTo fin
    Selection.Find.style = ActiveDocument.Styles(style)
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Replacement.Font.Color = couleurpolice
    With Selection.Find
        .Highlight = True
        .Font.Color = wdColorRed
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Replacement.Highlight = True
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    Selection.Find.style = ActiveDocument.Styles(style)
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Replacement.Font.Color = couleurpolice
    With Selection.Find
        .Highlight = True
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    Selection.Find.style = ActiveDocument.Styles(style)
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Replacement.Font.Color = couleurpolice
    With Selection.Find
        .Font.Color = wdColorRed
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
fin:
End Sub

Sub fin_verif_paragraphes()
    nom_macro = "fin_verif_paragraphes"
    'Macro permettant de desurligner et de supprimer le rouge ajoute lors de l'application de la macro de verification des paragraphes
    Call desactive_revision
    Call nettoyage_recherche_remplace
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="Description Auteur")
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="R_sum_")
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="Abstract")
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="Resumen")
    'To do : autre type de m_ta
    Selection.HomeKey Unit:=wdStory
    Selection.Find.Highlight = True
    Selection.Find.Font.Color = wdColorRed
    Selection.Find.Replacement.Font.Color = wdColorAutomatic
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
End Sub

Sub fin_verif_para_style_meta(style As String)
    nom_macro = "fin_verif_para_style_meta"
    Selection.HomeKey Unit:=wdStory
    Selection.Find.Highlight = True
    Selection.Find.style = style
    Selection.Find.Font.Color = wdColorRed
    Selection.Find.Replacement.Font.Color = wdColorDarkBlue
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
End Sub

Sub activation_surlignement()
    nom_macro = "activation_surlignement"
    If Options.DefaultHighlightColorIndex = 0 Then
        Options.DefaultHighlightColorIndex = wdYellow
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN VERIFICATION PARAGRAPHES ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''



''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT NOTES '''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub renumerotation_notes()
    nom_macro = "renumerotation_notes"
    'Macro destin�e a renum�roter correctement les appels de notes dans le cas ou ceux-ci sont tous a zero
    '(problme frequent lors du passage d'un document de Mac vers PC.))
    Call desactive_revision
    Call activer_cdt
    Call nettoyage_recherche_remplace
    
    Selection.HomeKey Unit:=wdStory
    With Selection.Find
        .Text = "^f" 'rechercher le premier appel de note dans le document
        .MatchWildcards = False
    End With
    Selection.Find.Execute
    
    'Si un appel de note est trouv� :
    If Selection.Find.Found Then
        With Selection.FootnoteOptions  'Puis r_gler les options de notes de bas de page :
            .Location = wdBottomOfPage
            .NumberingRule = wdRestartContinuous    '1) d_finir une num_rotation continue ;
            .StartingNumber = 1                     '2) commencant a 1 ;;
            .NumberStyle = wdNoteNumberStyleArabic  '3) en chiffres arabes.
        End With
    End If
End Sub


Sub nettoyage_notes_points()
    nom_macro = "nettoyage_notes_points"
    'Macro supprimant dans les notes les points aprs les num_ros de notee
    If ActiveDocument.Footnotes.Count > 0 Then
        Call activer_notes
        Selection.HomeKey Unit:=wdStory
        Call nettoyage_recherche_remplace
        Call rechercheremplaceER(textecherche:="(^2)\.", texteremplace:="\1")
        Call nettoyage_recherche_remplace
        Selection.HomeKey Unit:=wdStory
        Call messages_ok(texte_message:="Les points ont �t� supprim�s apres les num�ros de notes.")
        
    ElseIf ActiveDocument.Footnotes.Count = 0 Then
        Call messages_erreur("Erreur : aucune note de bas de page n'est pr_sente dans ce document.")
    End If
End Sub

Sub activer_notes()
    nom_macro = "activer_notes"
     'Macro plaant le curseur dans les notess
    If ActiveDocument.Footnotes.Count = 0 Then Exit Sub 'Si pas de notes dans le document, arrter cette macro....
        Call activer_affichage_normal
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then    'v_rifier que les notes sont activ_es
            ActiveWindow.Panes(2).Activate      'Placement du curseur dans les notes : utile si les notes sont affich_es mais que le curseur est plac_ dans le corps du document
            Selection.HomeKey Unit:=wdStory     'Placement du curseur au d_but des notes
        Else
            'On error resume next
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes            'Si ce n'est pas le cas, les activer
        End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN NOTES '''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''



''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT MISES EN FORME LOCALES ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Sub MEFL_simple()
    nom_macro = "MEFL_simple"
    'Nettoyage du corps de texte dans le document actif uniquement
    Call MEFL_cdt(var_batch:=False)
    Call refresh
End Sub

Sub MEFL_cdt(var_batch As Boolean)
    nom_macro = "MEFL_cdt"
    'Nettoyage des styles locaux dans le corps de texte
    'La variable var_batch sert � d_finir si le contexte est un traitement simple ou par lot, et d'afficher ou non des boites de dialogue en cons_quence
        's'il s'agit d'un traitement par lot, d_finir var_batch = true
        'sinon, var_batch = false
    System.Cursor = wdCursorWait
    Word.Application.ScreenUpdating = False
    
    Call creation_styles                    'Creation des styles de caractre servant au nettoyagee
    Call nettoyage_styles(notes:=False)     'Nettoyage � partir des styles ainsi cr__s, dans le corps de texte
    Call suppression_styles                 'Une fois le traitement effectu_, suppression des styles
    Call protection_MEFL_listes
    Call mise_en_page                       'Nettoyage de la mise en page
    
    'Nettoyage de tout surlignement
    Call nettoyage_surlignement
    
    Call nettoyage_recherche_remplace       'Remise � z�ro des options de recherche et de remplacement
    
    'Corriger les apostrophes dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           ''''''
           Call MEFL_notes(var_batch:=False)
           ''''''
        End If
    End If
        

    
    'Red�finir l'�tat du curseur � normal
    System.Cursor = wdCursorNormal
    Call refresh
    If var_batch = False Then               'Si on se trouve dans le cadre du traitement simple d'un document :
        Call messages_ok("Les mises en forme et les styles locaux ont �t� supprim�s.")                               'Afficher ce message
    End If
   
End Sub


Sub MEFL_notes_simple()
    nom_macro = "MEFL_notes_simple"
    'Nettoyage des notes de bas de page dans le document actif uniquement
    Call MEFL_notes(var_batch:=False)
    Call refresh
End Sub

Sub MEFL_notes(var_batch As Boolean)
    nom_macro = "MEFL_notes"
    
    'Nettoyage des styles locaux dans les notes de bas de page
    'La variable var_batch sert � d�finir si le contexte est un traitement simple ou par lot
        's'il s'agit d'un traitement par lot, d�finir var_batch = true
        'sinon, var_batch = false
    'Pr_paration du document
    If ActiveDocument.Footnotes.Count = 0 Then Exit Sub
    If ActiveDocument.Footnotes.Count >= 1 Then
        Call activer_notes
    Else
        'If var_batch = True Then
        If var_batch = False Then
        Call messages_erreur("Erreur : ce document ne contient pas de notes de bas de page")
        End If
        Exit Sub
    End If
    Word.Application.ScreenUpdating = False
    'Activation du surlignage (la macro ne fonctionne sinon pas)
    Options.DefaultHighlightColorIndex = wdYellow
    Call desactive_revision
    Call nettoyage_recherche_remplace
    'Si on se trouve dans un traitement par lot :
    If var_batch = True Then
        Call activer_notes          'Activer les notes
    End If
    'Curseur "attendre"
    System.Cursor = wdCursorWait
    Selection.HomeKey Unit:=wdStory
    
    'Inutile dans les notes ?
    'Nettoyage des styles locaux dus au passage dans Orphan notes
    On Error Resume Next
    Call remplacement_style(stylecherche:="WW-Citation", styleremplace:="Citation")
    On Error Resume Next
    Call remplacement_style(stylecherche:="WW-Bibliographie", styleremplace:="Bibliographie")
    'Protection des styles de caractres locaux pertinents, pour ne pas qu'ils soient affect�s par la macro
    On Error Resume Next
    Call surligne_style(style:="TEI_bibl_reference-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_code-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_linguistic_number-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_quote-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_versenumber-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_didascaly-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_speaker-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_formula-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_reviewed_title-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_reviewed_author-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_reviewed_date-inline")
    Selection.HomeKey Unit:=wdStory
    
    'Nettoyage des appels de notes : suppression des styles locaux sur les appels de note
    Selection.Find.Replacement.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont)
    Call rechercheremplaceER(textecherche:="(^2)", texteremplace:="\1")
    Call nettoyage_recherche_remplace
    'Nettoyage des marques de paragraphe
    Call nettoyage_marque_paragraphes
    Call nettoyage_recherche_remplace
    
    'Nettoyage des styles
    Call creation_styles                'Creation des styles de caractre destin�s au nettoyagee
    Call nettoyage_styles(notes:=True)  'Nettoyage � partir des styles ainsi cr��s, dans les notes
    Call suppression_styles             'Une fois le traitement effectu_, suppression des styles
    
    Call nettoyage_surlignement 'Nettoyage du surlignement dans les notes
    'Application du style "notes de bas de page"
    'Selection.HomeKey unit:=wdStory
    'Selection.WholeStory
    'Selection.style = ActiveDocument.Styles("Note de bas de page")
    'Fin du traitement
    Call refresh
    'If var_batch = False Then           'Si on se trouve dans le cadre du traitement simple d'un document :
        'If ActiveWindow.View.SplitSpecial = wdPaneNone Then                                               'S'il n'y a pas de s_paration corps de texte / notes :
        'MsgBox "Les mises en forme et les styles locaux ont _t_ supprim_s."                               'Afficher ce message
        'ElseIf ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then                                      's'il y a une s_paration corps de texte / notes :
        'Call messages_ok("Les mises en forme et les styles locaux ont �t� supprim�s dans les notes de bas de page.") 'Afficher ce message
        'End If
    'End If
fin:
End Sub


Sub mise_en_page()
    nom_macro = "mise_en_page"
    'Cette macro permet de remplacer un style de paragraphe par lui-mme, ce qui nettoie toutes les mises en page locales (retraits droit et gauche, etc.))
    'syntaxe de la macro :
    '   "Call nettoyage_mep(style:="nom du style � nettoyer")" : appel de la macro "nettoyage_mep", et remplissage de la variable "style", qui sert � indiquer quel style de paragraphe est � nettoyer
    '   "On error Resume Next" : permet de continuer l'ex�cution de la macro si le style indiqu� n'est pas pr�sent dans le document
    'Appel pr�alable d'une sous-macro de surlignement des paragraphes �crits de droite � gauche. Cela les diff�rencie et permet de leur appliquer un traitement sp�cifique � la fin de la macro
    'Call right_to_left ': est-ce n�cessaire ? Si utilis�, penser � appeler Sub right_to_left_fin() � la fin de la macro
    'On Error Resume Next
    'Nettoyage de la mise en page des m�tadonn�es

    Call nettoyage_mep(style:=wdStyleTitle)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleNormal)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleHeading1)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleHeading2)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleHeading3)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleHeading4)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleHeading5)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleHeading6)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleFootnoteText)
    On Error Resume Next
    Call nettoyage_mep(style:=wdStyleEndnoteText)
    
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_title:sup")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_title:sub")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_author:aut")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_editor:trl")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_editor:ctb")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_authority_biography")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_authority_mail")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_authority_affiliation")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_keywords_subjects:chronology")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_keywords_subjects:geography")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_keywords_subjects:subject")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_keywords_subjects:work")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_keywords_subjects:propernoun")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_keywords_subjects:personcited")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_date_reception")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_date_acceptance")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_language")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_document_number")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_pagination")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_editor:edt")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_editor:fld")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_keywords_subjects:excavationyear")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_patriarcheid")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_paragraph_break")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_erratum")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_acknowledgment")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_dedication")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_epigraph")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_paragraph_lead")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_note:aut")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_note:pbl")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_note:trl")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_appendix_start")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_bibl_start")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_floatingtext_start")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_floatingtext_end")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_paragraph_consecutive")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_bibl_reference")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_question")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_answer")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_code")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_quote")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_quote2")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_quote_continuation")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_quote_nested")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_bibl_citation")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_replica")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_speaker")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_didascaly")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_verse")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_versifiedreplica")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_linguistic_example")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_linguistic_gloss")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_linguistic_translation")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_figure_title")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_figure_caption")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_figure_credits")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_cell")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_figure_alternative")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_formula")
    On Error Resume Next
    Call nettoyage_mep(style:="TEI_reviewed_reference")
    On Error Resume Next
    'r�tablissement de la mise en page "sens de lecture de droite � gauche" pour les paragraphes surlign�s au d�but de la macro
    'Call right_to_left_fin
    'On Error Resume Next
End Sub

Sub nettoyage_mep(style As String)
    nom_macro = "nettoyage_mep"
    'Sous-macro de nettoyage de la mise en page
    'Fonctionne en remplaant un style par lui-mme : :
    'wdReadingOrderLtl As String'
    Call nettoyage_recherche_remplace
    Selection.Find.style = ActiveDocument.Styles(style)
    Selection.Find.Replacement.style = ActiveDocument.Styles(style)
    With Selection.Find
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .Highlight = False
        If System.OperatingSystem Like "Windows*" Then
            .ParagraphFormat.ReadingOrder = wdReadingOrderLtr 'Ne rechercher que les paragraphes orient_s de gauche � droite (les paragraphes orient_s de droite � gauche seront trait_s par la suite)
        End If
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    If System.OperatingSystem Like "Windows*" Then
        Call nettoyage_mep_rtl(style:=style) 'Nettoyage des paragraphes orient_s de droite � gauche, qui ont besoin d'un traitement s_par_ pour ne pas perdre leur orientation
    End If
    Call nettoyage_recherche_remplace
End Sub

Sub nettoyage_mep_rtl(style As String)
    nom_macro = "nettoyage_mep_rtl"
    If System.OperatingSystem Like "Macintosh*" Then Exit Sub
    'Si on applique � un paragraphe son propre style de paragraphe en passant par un rechercher/remplacer, il est impossible de restaurer ensuite son orientation, s'il s'afit d'un paragraphe de droite � gauche
    'Il faut donc :
    'I/ rechercher chaque paragraphe orient_ de droite � gauche
    Call nettoyage_recherche_remplace   'Remise � z_ro des options de recherche
    Selection.HomeKey Unit:=wdStory     'Placer le curseur au d_but du document
    Selection.Find.style = ActiveDocument.Styles(style) 'Rechercher un style de paragraphe donn_ en variable
    With Selection.Find
            .Text = ""                                          'Rechercher tout texte
            .Replacement.Text = "^&"                            'le remplacer par lui mmee
            .Forward = True
            .Wrap = wdFindContinue                              'recherche en continu (ne pas demander de revenir au d_but du document)
            .Format = True
            .Highlight = False                                  'rechercher ce qui n'est pas surlign_
            .MatchCase = False
            .MatchWholeWord = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
            .ParagraphFormat.ReadingOrder = wdReadingOrderRtl   'rechercher ce qui est _crit de droite � gauche
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
        End With
        Do While Selection.Find.Execute     'Lancer la recherche, et lorsqu'un _l_ment est trouv_ :
            With Selection.ParagraphFormat
            .style = ActiveDocument.Styles(style) 'lui appliquer le style d_fini en variable
            .ReadingOrder = wdReadingOrderRtl     'et lui appliquer l'orientation de droite � gauche
            End With
        Loop                                'Puis rechercher le paragraphe suivant et lui appliquer les mmes optionss
    Call nettoyage_recherche_remplace   'Remise � z_ro des options de recherche
End Sub

Sub remplacement_style(Optional stylecherche As String, Optional styleremplace As String)
   nom_macro = "remplacement_style"
   Dim styStyle As style
   Dim sStyleName  As String
   sStyleName = stylecherche
   For Each styStyle In ActiveDocument.Styles
      If styStyle.NameLocal = sStyleName Then 'V_rifie si le style existe dans le document
    Selection.Find.style = ActiveDocument.Styles(stylecherche)
    With Selection.Find
        .Text = ""
        .Replacement.Text = "^&"
        .Replacement.style = ActiveDocument.Styles(styleremplace)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
      Exit For
    End If
    Next
fin:
End Sub

Sub surligne_style(style As String)
    nom_macro = "surligne_style"
    'Macro de surlignement d'un style d_fini en variable
    'Utilis_e pour la macro de suppression des styles locaux
    'Permet d'_viter que le style d_fini en variable soit affect_ par la macro de suppression des styles locaux
    Selection.Find.style = ActiveDocument.Styles(style)
    With Selection.Find
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Replacement.Highlight = True
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .Execute Replace:=wdReplaceAll
    End With
    Call nettoyage_recherche_remplace
End Sub

Sub nettoyage_marque_paragraphes()
    nom_macro = "nettoyage_marque_paragraphes"
    'Nettoyage des marques de paragraphe
    'Une marque de paragraphe avec une mise en forme locale (italique, couleur, etc.) bloque le nettoyage des styles et mises en forme sur les paragraphes qui la suivent.
    'Dans word < 2007, il n'est pas possible d'effectuer un remplacement de saut de paragraphe par lui-mme avec un style "police par d_faut" pour effectuer ce nettoyage des marques de paragraphe..
    'Il faut donc rechercher les marques de paragraphe une par une, et chaque fois leur appliquer le style "police par d_faut".
    If Word.Application.Version < "12" Then 'Bug n'apparaissant que dans word 2003 (application version < 12)
        With Selection.Find
                    .Text = "^p" 'Recherche d'une marque de paragraphe
                    .Replacement.Text = ""
                    .Replacement.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont)
                    .Forward = True
                    .Wrap = wdFindContinue
                    .Format = True
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                Do While Selection.Find.Execute              'D_but de la boucle permettant de rechercher les marques une par une.
                    .Execute Replace:=wdReplaceOne
                    Selection.Find.Execute
                    Selection.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont)          'Quand une marque de paragraphe est trouv_e, application du style "Police par d_faut", qui nettoie tout type de mise en forme locale sur la marque trouv_e.
                Loop
        End With                          'Fin du nettoyage des marques de paragraphe
    ElseIf Word.Application.Version > "12" Then
        With Selection.Find
                    .Text = "^p" 'Recherche d'une marque de paragraphe
                    .Replacement.Text = ""
                    .Replacement.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont)
                    .Forward = True
                    .Wrap = wdFindContinue
                    .Format = True
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
    End If
End Sub

Sub creation_styles()
nom_macro = "creation_styles"
'Creation d'un style de caractre pour chaque combinaison possible de mises en formee
'Chaque style poss�de la m�me mise en forme, celle du style de caractre par defaut de word, qui permet d'effacer toutes les mises en forme localesles
'On appliquera ensuite a chaque style de caractre la mise en forme lui correspondant
'Mises en forme prises en charge : normal, italique, gras, soulign_, exposant, indice, petites majuscules, tout en majuscules
'70 combinaisons possibles, en _liminant les combinaisons impossibles (mises en forme s'excluant : indice + exposant ; petites majuscules + tout en majuscules)
    'Ne pas afficher les modifications en cours
    Word.Application.ScreenUpdating = False
    'Pour eviter que les styles cres ne prennent les proprietes du paragraphe ou le curseur est place :
    Selection.HomeKey Unit:=wdStory                                      'Venir au debut du document
    Selection.TypeParagraph                                              'Creer un paragraphe vide
    Selection.MoveLeft Unit:=wdCharacter, Count:=1, Extend:=wdExtend     'Selectionner la marque du paragraphe vide (faire revenir le curseur � gauche  + Extend:=wdExtend pour lui dire d'effectuer une s_lection)
    Selection.ClearFormatting                                            'Nettoyer la marque de paragraphe des mises en forme locales
    Selection.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont) 'Appliquer le style de caractre par defaut (police par defaut = wdStyleDefaultParagraphFont))
    Selection.ClearFormatting                                            'Nettoyer a nouveau la marque de paragraphe des mises en forme locales

    'Creation d'un style normal
    Call creer_style(style:="#StyleMEFL#normal")
    'Creation d'un style italiques
    Call creer_style(style:="#StyleMEFL#italiques")
    'Creation d'un style italique+gras
    Call creer_style(style:="#StyleMEFL#italique+gras")
    'Creation d'un style italique+gras+PM
    Call creer_style(style:="#StyleMEFL#italique+gras+PM")
    'Creation d'un style italique+gras+petites maj+exposant
    Call creer_style(style:="#StyleMEFL#italique+gras+petites maj+exposant")
    'Creation d'un style italique+gras+petites maj+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#italique+gras+petites maj+exposant+soulignement")
    'Creation d'un style italique+gras+petites maj+indice
    Call creer_style(style:="#StyleMEFL#italique+gras+petites maj+indice")
    'Creation d'un style italique+gras+petites maj+indice+soulignement
    Call creer_style(style:="#StyleMEFL#italique+gras+petites maj+indice+soulignement")
    'Creation d'un style italique+gras+TM
    Call creer_style(style:="#StyleMEFL#italique+gras+TM")
    'Creation d'un style italique+gras+tout en maj+exposant
    Call creer_style(style:="#StyleMEFL#italique+gras+tout en maj+exposant")
    'Creation d'un style italique+gras+tout en maj+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#italique+gras+tout en maj+exposant+soulignement")
    'Creation d'un style italique+gras+tout en maj+indice
    Call creer_style(style:="#StyleMEFL#italique+gras+tout en maj+indice")
    'Creation d'un style italique+gras+tout en maj+indice+soulignement
    Call creer_style(style:="#StyleMEFL#italique+gras+tout en maj+indice+soulignement")
    'Creation d'un style italique+gras+exposant
    Call creer_style(style:="#StyleMEFL#italique+gras+exposant")
    'Creation d'un style italique+gras+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#italique+gras+exposant+soulignement")
    'Creation d'un style italique+gras+indice
    Call creer_style(style:="#StyleMEFL#italique+gras+indice")
    'Creation d'un style italique+gras+indice+soulignement
    Call creer_style(style:="#StyleMEFL#italique+gras+indice+soulignement")
    'Creation d'un style italique+gras+soulignement
    Call creer_style(style:="#StyleMEFL#italique+gras+soulignement")
    'Creation d'un style italique+PM
    Call creer_style(style:="#StyleMEFL#italique+PM")
    'Creation d'un style italique+PM+exposant
    Call creer_style(style:="#StyleMEFL#italique+PM+exposant")
    'Creation d'un style italique+PM+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#italique+PM+exposant+soulignement")
    'Creation d'un style italique+PM+indice
    Call creer_style(style:="#StyleMEFL#italique+PM+indice")
    'Creation d'un style italique+PM+indice+soulignement
    Call creer_style(style:="#StyleMEFL#italique+PM+indice+soulignement")
    'Creation d'un style italique+PM+soulignement
    Call creer_style(style:="#StyleMEFL#italique+PM+soulignement")
    'Creation d'un style italique+TM
    Call creer_style(style:="#StyleMEFL#italique+TM")
    'Creation d'un style italique+TM+exposant
    Call creer_style(style:="#StyleMEFL#italique+TM+exposant")
    'Creation d'un style italique+TM+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#italique+TM+exposant+soulignement")
    'Creation d'un style italique+TM+indice
    Call creer_style(style:="#StyleMEFL#italique+TM+indice")
    'Creation d'un style italique+TM+indice+soulignement
    Call creer_style(style:="#StyleMEFL#italique+TM+indice+soulignement")
    'Creation d'un style italique+TM+soulignement
    Call creer_style(style:="#StyleMEFL#italique+TM+soulignement")
    'Creation d'un style italique+exposant
    Call creer_style(style:="#StyleMEFL#italique+exposant")
    'Creation d'un style italique+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#italique+exposant+soulignement")
    'Creation d'un style italique+indice
    Call creer_style(style:="#StyleMEFL#italique+indice")
    'Creation d'un style italique+indice+soulignement
    Call creer_style(style:="#StyleMEFL#italique+indice+soulignement")
    'Creation d'un style italique+soulignement
    Call creer_style(style:="#StyleMEFL#italique+soulignement")
    'Creation d'un style gras    '
    Call creer_style(style:="#StyleMEFL#gras")
    'Creation d'un style gras+PM
    Call creer_style(style:="#StyleMEFL#gras+PM")
    'Creation d'un style gras+PM+exposant
    Call creer_style(style:="#StyleMEFL#gras+PM+exposant")
    'Creation d'un style gras+PM+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#gras+PM+exposant+soulignement")
    'Creation d'un style gras+PM+Indice
    Call creer_style(style:="#StyleMEFL#gras+PM+Indice")
    'Creation d'un style gras+PM+Indice+soulignement
    Call creer_style(style:="#StyleMEFL#gras+PM+Indice+soulignement")
    'Creation d'un style gras+PM+soulignement
    Call creer_style(style:="#StyleMEFL#gras+PM+soulignement")
    'Creation d'un style gras+TM
    Call creer_style(style:="#StyleMEFL#gras+TM")
    'Creation d'un style gras+TM+exposant
    Call creer_style(style:="#StyleMEFL#gras+TM+exposant")
    'Creation d'un style gras+TM+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#gras+TM+exposant+soulignement")
    'Creation d'un style gras+TM+Indice
    Call creer_style(style:="#StyleMEFL#gras+TM+Indice")
    'Creation d'un style gras+TM+Indice+soulignement
    Call creer_style(style:="#StyleMEFL#gras+TM+Indice+soulignement")
    'Creation d'un style gras+TM+soulignement
    Call creer_style(style:="#StyleMEFL#gras+TM+soulignement")
    'Creation d'un style gras+exposant
    Call creer_style(style:="#StyleMEFL#gras+exposant")
    'Creation d'un style gras+exposant+soulignement
    Call creer_style(style:="#StyleMEFL#gras+exposant+soulignement")
    'Creation d'un style gras+indice
    Call creer_style(style:="#StyleMEFL#gras+indice")
    'Creation d'un style gras+indice+soulignement
    Call creer_style(style:="#StyleMEFL#gras+indice+soulignement")
    'Creation d'un style gras+soulignement
    Call creer_style(style:="#StyleMEFL#gras+soulignement")
    'Creation d'un style PM'
    Call creer_style(style:="#StyleMEFL#PM")
    'Creation d'un style PM+Exposant
    Call creer_style(style:="#StyleMEFL#PM+Exposant")
    'Creation d'un style PM+Exposant+soulignement
    Call creer_style(style:="#StyleMEFL#PM+Exposant+soulignement")
    'Creation d'un style PM+Indice
    Call creer_style(style:="#StyleMEFL#PM+Indice")
    'Creation d'un style PM+indice+soulignement
    Call creer_style(style:="#StyleMEFL#PM+indice+soulignement")
    'Creation d'un style PM+soulignement
    Call creer_style(style:="#StyleMEFL#PM+soulignement")
    'Creation d'un style TM'
    Call creer_style(style:="#StyleMEFL#TM")
    'Creation d'un style TM+Exposant
    Call creer_style(style:="#StyleMEFL#TM+Exposant")
    'Creation d'un style TM+Exposant+soulignement
    Call creer_style(style:="#StyleMEFL#TM+Exposant+soulignement")
    'Creation d'un style TM+Indice
    Call creer_style(style:="#StyleMEFL#TM+Indice")
    'Creation d'un style TM+Indice+soulignement
    Call creer_style(style:="#StyleMEFL#TM+Indice+soulignement")
    'Creation d'un style TM+soulignement
    Call creer_style(style:="#StyleMEFL#TM+soulignement")
    'Creation d'un style exposant'
    Call creer_style(style:="#StyleMEFL#exposant")
    'Creation d'un style Exposant+soulignement
    Call creer_style(style:="#StyleMEFL#Exposant+soulignement")
    'Creation d'un style indice'
    Call creer_style(style:="#StyleMEFL#indice")
    'Creation d'un style indice+soulignement
    Call creer_style(style:="#StyleMEFL#indice+soulignement")
    'Creation d'un style soulignement'
    Call creer_style(style:="#StyleMEFL#soulignement")
    'Fin du traitement
    Selection.HomeKey Unit:=wdStory 'Revenir au debut
    Selection.Delete                'Supprimer le paragraphe vide cree au debut
End Sub

Sub nettoyage_styles(notes As Boolean)
    nom_macro = "nettoyage_styles"
    Selection.HomeKey Unit:=wdStory
    'Protection des styles de caract�res locaux pertinents, pour ne pas qu'ils soient affect�s par la macro
'    TEI_bibl_reference-inline
'    TEI_code-inline
'    TEI_linguistic_number-inline
'    TEI_quote-inline
'    TEI_versenumber-inline
'    TEI_didascaly-inline
'    TEI_speaker-inline
'    TEI_formula-inline
'    TEI_reviewed_title-inline
'    TEI_reviewed_author-inline
'    TEI_reviewed_date-inline
    Options.DefaultHighlightColorIndex = wdYellow
    On Error Resume Next
    Call surligne_style(style:="TEI_bibl_reference-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_code-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_linguistic_number-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_quote-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_versenumber-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_didascaly-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_speaker-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_formula-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_reviewed_title-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_reviewed_author-inline")
    On Error Resume Next
    Call surligne_style(style:="TEI_reviewed_date-inline")
    Selection.HomeKey Unit:=wdStory

    'Penser a nettoyer ces styles a la fin
    'Protection des listes (surlignement)
    Call protection_MEFL_listes
    'Protection des liens hypertextes (surlignement)
    Call protection_MEFL_liens
    'Nettoyage du reste du document
    'Nettoyage normal
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#normal", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage des italiques
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italiques", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+PM
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+PM", italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+petites maj+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+petites maj+exposant", italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+petites maj+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+petites maj+exposant+soulignement", italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+gras+petites maj+indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+petites maj+indice", italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+gras+petites maj+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+petites maj+indice+soulignement", italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+gras+TM
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+TM", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+tout en maj+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+tout en maj+exposant", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+tout en maj+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+tout en maj+exposant+soulignement", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+gras+tout en maj+indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+tout en maj+indice", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+gras+tout en maj+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+tout en maj+indice+soulignement", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+gras+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+exposant", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+exposant+soulignement", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+gras+indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+indice", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+gras+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+indice+soulignement", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+gras+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+gras+soulignement", italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+PM
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+PM", italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+PM+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+PM+exposant", italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+PM+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+PM+exposant+soulignement", italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+PM+indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+PM+indice", italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+PM+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+PM+indice+soulignement", italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+PM+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+PM+soulignement", italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+TM
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+TM", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+TM+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+TM+exposant", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+TM+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+TM+exposant+soulignement", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+TM+indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+TM+indice", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+TM+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+TM+indice+soulignement", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+TM+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+TM+soulignement", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+exposant", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+exposant+soulignement", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+indice", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+indice+soulignement", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#italique+soulignement", italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras    '
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+PM
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+PM", italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+PM+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+PM+exposant", italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+PM+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+PM+exposant+soulignement", italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+PM+Indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+PM+Indice", italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage gras+PM+Indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+PM+Indice+soulignement", italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage gras+PM+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+PM+soulignement", italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+TM
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+TM", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+TM+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+TM+exposant", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+TM+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+TM+exposant+soulignement", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+TM+Indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+TM+Indice", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage gras+TM+Indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+TM+Indice+soulignement", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage gras+TM+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+TM+soulignement", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+exposant", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+exposant+soulignement", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+indice", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage gras+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+indice+soulignement", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage gras+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#gras+soulignement", italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage PM'
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#PM", italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage PM+Exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#PM+Exposant", italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage PM+Exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#PM+Exposant+soulignement", italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage PM+Indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#PM+Indice", italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage PM+indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#PM+indice+soulignement", italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage PM+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#PM+soulignement", italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage TM'
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#TM", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage TM+Exposant
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#TM+Exposant", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage TM+Exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#TM+Exposant+soulignement", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage TM+Indice
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#TM+Indice", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage TM+Indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#TM+Indice+soulignement", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage TM+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#TM+soulignement", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage exposant'
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#exposant", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage Exposant+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#Exposant+soulignement", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage indice'
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#indice", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage indice+soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#indice+soulignement", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage soulignement
    Call rechercher_style_remplacement_attributs(notes:=notes, style:="#StyleMEFL#soulignement", italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    
    'Nettoyage des styles surlignes au debut
    'Styles de caracteres surlignes
    'Les desurligner
    On Error Resume Next
    Call desurligne_style(style:="TEI_bibl_reference-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_code-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_linguistic_number-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_quote-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_versenumber-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_didascaly-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_speaker-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_formula-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_reviewed_title-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_reviewed_author-inline")
    On Error Resume Next
    Call desurligne_style(style:="TEI_reviewed_date-inline")
    
    'Nettoyage des styles de caractress
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_bibl_reference-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_code-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_linguistic_number-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_quote-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_versenumber-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_didascaly-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_speaker-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_formula-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_reviewed_title-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_reviewed_author-inline")
    On Error Resume Next
    Call nettoyage_style_unique(style:="TEI_reviewed_date-inline")
    
    
    'nettoyage des listes
    On Error Resume Next
    Call nettoyage_MEFL_listes
    
    'nettoyage des liens hypertextes
    On Error Resume Next
    Call nettoyage_MEFL_liens
    End Sub
    Sub suppression_styles()
    nom_macro = "suppression_styles"
    
    Word.Application.ScreenUpdating = False
    
    'Nettoyage normal'
    Call kill_style(style:="#StyleMEFL#normal")
    'Nettoyage des italiques
    Call kill_style(style:="#StyleMEFL#italiques")
    'Nettoyage italique+gras
    Call kill_style(style:="#StyleMEFL#italique+gras")
    'Nettoyage italique+gras+PM
    Call kill_style(style:="#StyleMEFL#italique+gras+PM")
    'Nettoyage italique+gras+petites maj+exposant
    Call kill_style(style:="#StyleMEFL#italique+gras+petites maj+exposant")
    'Nettoyage italique+gras+petites maj+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#italique+gras+petites maj+exposant+soulignement")
    'Nettoyage italique+gras+petites maj+indice
    Call kill_style(style:="#StyleMEFL#italique+gras+petites maj+indice")
    'Nettoyage italique+gras+petites maj+indice+soulignement
    Call kill_style(style:="#StyleMEFL#italique+gras+petites maj+indice+soulignement")
    'Nettoyage italique+gras+TM
    Call kill_style(style:="#StyleMEFL#italique+gras+TM")
    'Nettoyage italique+gras+tout en maj+exposant
    Call kill_style(style:="#StyleMEFL#italique+gras+tout en maj+exposant")
    'Nettoyage italique+gras+tout en maj+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#italique+gras+tout en maj+exposant+soulignement")
    'Nettoyage italique+gras+tout en maj+indice
    Call kill_style(style:="#StyleMEFL#italique+gras+tout en maj+indice")
    'Nettoyage italique+gras+tout en maj+indice+soulignement
    Call kill_style(style:="#StyleMEFL#italique+gras+tout en maj+indice+soulignement")
    'Nettoyage italique+gras+exposant
    Call kill_style(style:="#StyleMEFL#italique+gras+exposant")
    'Nettoyage italique+gras+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#italique+gras+exposant+soulignement")
    'Nettoyage italique+gras+indice
    Call kill_style(style:="#StyleMEFL#italique+gras+indice")
        'Nettoyage italique+gras+indice+soulignement
    Call kill_style(style:="#StyleMEFL#italique+gras+indice+soulignement")
    'Nettoyage italique+gras+soulignement
    Call kill_style(style:="#StyleMEFL#italique+gras+soulignement")
    'Nettoyage italique+PM
    Call kill_style(style:="#StyleMEFL#italique+PM")
    'Nettoyage italique+PM+exposant
    Call kill_style(style:="#StyleMEFL#italique+PM+exposant")
    'Nettoyage italique+PM+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#italique+PM+exposant+soulignement")
    'Nettoyage italique+PM+indice
    Call kill_style(style:="#StyleMEFL#italique+PM+indice")
    'Nettoyage italique+PM+indice+soulignement
    Call kill_style(style:="#StyleMEFL#italique+PM+indice+soulignement")
    'Nettoyage italique+PM+soulignement
    Call kill_style(style:="#StyleMEFL#italique+PM+soulignement")
    'Nettoyage italique+TM
    Call kill_style(style:="#StyleMEFL#italique+TM")
    'Nettoyage italique+TM+exposant
    Call kill_style(style:="#StyleMEFL#italique+TM+exposant")
    'Nettoyage italique+TM+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#italique+TM+exposant+soulignement")
    'Nettoyage italique+TM+indice
    Call kill_style(style:="#StyleMEFL#italique+TM+indice")
    'Nettoyage italique+TM+indice+soulignement
    Call kill_style(style:="#StyleMEFL#italique+TM+indice+soulignement")
    'Nettoyage italique+TM+soulignement
    Call kill_style(style:="#StyleMEFL#italique+TM+soulignement")
    'Nettoyage italique+exposant
    Call kill_style(style:="#StyleMEFL#italique+exposant")
    'Nettoyage italique+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#italique+exposant+soulignement")
    'Nettoyage italique+indice
    Call kill_style(style:="#StyleMEFL#italique+indice")
    'Nettoyage italique+indice+soulignement
    Call kill_style(style:="#StyleMEFL#italique+indice+soulignement")
    'Nettoyage italique+soulignement
    Call kill_style(style:="#StyleMEFL#italique+soulignement")
    'Nettoyage gras
    Call kill_style(style:="#StyleMEFL#gras")
    'Nettoyage gras+PM
    Call kill_style(style:="#StyleMEFL#gras+PM")
    'Nettoyage gras+PM+exposant
    Call kill_style(style:="#StyleMEFL#gras+PM+exposant")
    'Nettoyage gras+PM+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#gras+PM+exposant+soulignement")
    'Nettoyage gras+PM+Indice
    Call kill_style(style:="#StyleMEFL#gras+PM+Indice")
    'Nettoyage gras+PM+Indice+soulignement
    Call kill_style(style:="#StyleMEFL#gras+PM+Indice+soulignement")
    'Nettoyage gras+PM+soulignement
    Call kill_style(style:="#StyleMEFL#gras+PM+soulignement")
    'Nettoyage gras+TM
    Call kill_style(style:="#StyleMEFL#gras+TM")
    'Nettoyage gras+TM+exposant
    Call kill_style(style:="#StyleMEFL#gras+TM+exposant")
    'Nettoyage gras+TM+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#gras+TM+exposant+soulignement")
    'Nettoyage gras+TM+Indice
    Call kill_style(style:="#StyleMEFL#gras+TM+Indice")
    'Nettoyage gras+TM+Indice+soulignement
    Call kill_style(style:="#StyleMEFL#gras+TM+Indice+soulignement")
    'Nettoyage gras+TM+soulignement
    Call kill_style(style:="#StyleMEFL#gras+TM+soulignement")
    'Nettoyage gras+exposant
    Call kill_style(style:="#StyleMEFL#gras+exposant")
    'Nettoyage gras+exposant+soulignement
    Call kill_style(style:="#StyleMEFL#gras+exposant+soulignement")
    'Nettoyage gras+indice
    Call kill_style(style:="#StyleMEFL#gras+indice")
    'Nettoyage gras+indice+soulignement
    Call kill_style(style:="#StyleMEFL#gras+indice+soulignement")
    'Nettoyage gras+soulignement
    Call kill_style(style:="#StyleMEFL#gras+soulignement")
    'Nettoyage PM'
    Call kill_style(style:="#StyleMEFL#PM")
    'Nettoyage PM+Exposant
    Call kill_style(style:="#StyleMEFL#PM+Exposant")
    'Nettoyage PM+Exposant+soulignement
    Call kill_style(style:="#StyleMEFL#PM+Exposant+soulignement")
    'Nettoyage PM+Indice
    Call kill_style(style:="#StyleMEFL#PM+Indice")
    'Nettoyage PM+indice+soulignement
    Call kill_style(style:="#StyleMEFL#PM+indice+soulignement")
    'Nettoyage PM+soulignement
    Call kill_style(style:="#StyleMEFL#PM+soulignement")
    'Nettoyage TM'
    Call kill_style(style:="#StyleMEFL#TM")
    'Nettoyage TM+Exposant
    Call kill_style(style:="#StyleMEFL#TM+Exposant")
    'Nettoyage TM+Exposant+soulignement
    Call kill_style(style:="#StyleMEFL#TM+Exposant+soulignement")
    'Nettoyage TM+Indice
    Call kill_style(style:="#StyleMEFL#TM+Indice")
    'Nettoyage TM+Indice+soulignement
    Call kill_style(style:="#StyleMEFL#TM+Indice+soulignement")
    'Nettoyage TM+soulignement
    Call kill_style(style:="#StyleMEFL#TM+soulignement")
    'Nettoyage exposant'
    Call kill_style(style:="#StyleMEFL#exposant")
    'Nettoyage Exposant+soulignement
    Call kill_style(style:="#StyleMEFL#Exposant+soulignement")
    'Nettoyage indice'
    Call kill_style(style:="#StyleMEFL#indice")
    'Nettoyage indice+soulignement
    Call kill_style(style:="#StyleMEFL#indice+soulignement")
    'Nettoyage soulignement'
    Call kill_style(style:="#StyleMEFL#soulignement")
End Sub


Sub creer_style(style As String)
    nom_macro = "creer_style"
    'Creation d'un style, dont le nom est d_fini en variable
    On Error Resume Next
    ActiveDocument.Styles.Add Name:=style, Type:=wdStyleTypeCharacter
    ActiveDocument.Styles(style).BaseStyle = wdStyleDefaultParagraphFont
End Sub

'NETTOYAGE DES MISES EN FORME LOCALES SUR LES LISTES
Sub protection_MEFL_listes()
    Call nettoyage_recherche_remplace
    Selection.HomeKey Unit:=wdStory
    Dim type_liste As String
    Dim i As Integer
    Dim nbre_paragraphes_liste As Integer
    nbre_paragraphes_liste = ActiveDocument.ListParagraphs.Count
    'S'il n'y a pas de liste dans le document, ne pas ex_cuter la macro
    If nbre_paragraphes_liste = 0 Then Exit Sub
    For i = 1 To nbre_paragraphes_liste
        ActiveDocument.ListParagraphs(i).Range.Select
        Selection.Range.HighlightColorIndex = wdYellow
    Next i
    Selection.HomeKey Unit:=wdStory
End Sub

Sub protection_MEFL_liens()
    'protection liens dans le corps de texte
    Dim i As Integer
    For i = 1 To ActiveDocument.Hyperlinks.Count
    ActiveDocument.Hyperlinks(i).Range.HighlightColorIndex = wdYellow
    Next i
    
    'protection des liens dans les notes de bas de page
    Dim note As footnote
    Dim champ As Field
     For Each note In Application.ActiveDocument.Footnotes 'pour chaque note dans les notes :
        For Each champ In note.Range.Fields                     'et pour chaque champ dans la note active :
            If champ.Type = wdFieldHyperlink Then                   'voir si le champ est un lien
                champ.Select
                Selection.Range.HighlightColorIndex = wdYellow
                Selection.Collapse Direction:=wdCollapseEnd
            End If
                
        Next champ                                              'Traiter le champ suivant dans la note, en lui appliquant les actions pr_c_dentes
     Next note                                              'Traiter la note suivante; etc.
    Selection.HomeKey Unit:=wdStory                     'Placer le curseur au d_but des notes
End Sub

Sub rechercher_style_remplacement_attributs(notes As Boolean, style As String, Optional italique As Boolean, Optional gras As Boolean, Optional petitesmajuscules As Boolean, Optional toutmajuscules As Boolean, Optional exposant As Boolean, Optional indice As Boolean, Optional soulignement As Boolean, Optional surlignage As Boolean)
    nom_macro = "rechercher_style_remplacement_attributs"
    'Rechercher un style et le remplacer par ses attributs li_s
    Call nettoyage_recherche_remplace 'Remise � z_ro des paramtres de recherchee
    
    If notes = True Then                'Si on se trouve dans les notes :
    Call nettoyage_marque_paragraphes   'Nettoyer d'abord les marques de paragraphe
    End If
    'Placer le curseur au d_but du document
    Selection.HomeKey Unit:=wdStory
            With Selection.Find                 'Trouver les parties avec les attributs d_finis en variable, et qui ne sont pas surlign_es
            .Highlight = False                  'Recherche de l'absence de surlignement
            .Text = ""                          'Recherche de tout texte
            .Format = True
            .MatchCase = False
            .MatchWholeWord = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
            .Forward = True
            .Wrap = wdFindContinue
            .Replacement.style = ActiveDocument.Styles(style) 'Ajouter un style de caractre "#Style#MEFL" � ces parties trouv_es ; le nom du style d_pend de la combinaison de mises en forme recherch_es..
            'Hack Word 2003
            If Application.Version < "12" Then  'Si on est dans Word 2003 (Application.Version < "12") :
                If notes = True Then                            'Et si on est dans les notes...
                    .Replacement.Text = ""                          'Remplacer par rien
                Else                                            'Si on n'est pas dans les notes :
                    .Replacement.Text = "^&"                        'remplacer le texte trouv_ par lui-mme (^&))
                End If
    
            Else                            'Si on n'est pas dans Word 2003 (donc si on est dans 2007) :
                .Replacement.Text = ""                          'Remplacer le texte par rien pour _viter un blocage du rechercher/remplacer
            End If
            'Fin hack
            .Font.Italic = italique
            .Font.Bold = gras
            .Font.SmallCaps = petitesmajuscules
            .Font.AllCaps = toutmajuscules
            .Font.Superscript = exposant
            .Font.Subscript = indice
            .Font.Underline = soulignement
            End With
            Selection.Find.Execute Replace:=wdReplaceAll
    
    If Selection.Find.Found Then
        GoTo suite1:
    Else
        Call nettoyage_recherche_remplace
        Exit Sub
    End If
    
    
suite1:
    Call nettoyage_recherche_remplace
    Selection.HomeKey Unit:=wdStory
    'notes As Boolean,
          With Selection.Find                   'Trouver les parties avec les attributs d_finis en variable, et qui ne sont pas surlign_es
                                                'Recherche de l'absence de surlignement
            .Text = ""                          'Recherche de tout texte
            .Format = True
            .MatchCase = False
            .MatchWholeWord = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
            .Forward = True
            .Wrap = wdFindContinue
            .style = ActiveDocument.Styles(style) 'recherche le style de caractre "#Style#MEFL""
            'Hack Word 2003
            If Application.Version < "12" Then  'Si on est dans Word 2003 (Application.Version < "12") :
                If notes = True Then                            'Et si on est dans les notes...
                    .Replacement.Text = ""                          'Remplacer par rien
                Else                                            'Si on n'est pas dans les notes :
                    .Replacement.Text = "^&"                        'remplacer le texte trouv_ par lui-mme (^&))
                End If
    
            Else                                'Si on n'est pas dans Word 2003 (donc si on est dans 2007) :
                .Replacement.Text = ""                          'Remplacer le texte par rien pour _viter un blocage du rechercher/remplacer
            End If
            'Fin hack
            .Replacement.Font.Italic = italique
            .Replacement.Font.Bold = gras
            .Replacement.Font.SmallCaps = petitesmajuscules
            .Replacement.Font.AllCaps = toutmajuscules
            .Replacement.Font.Superscript = exposant
            .Replacement.Font.Subscript = indice
            .Replacement.Font.Underline = soulignement
            End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    Selection.HomeKey Unit:=wdStory
End Sub

Sub desurligne_style(style As String)
    nom_macro = "desurligne_style"
    Selection.Find.style = ActiveDocument.Styles(style)
    With Selection.Find
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Replacement.Highlight = False
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .Execute Replace:=wdReplaceAll
    End With
    Call nettoyage_recherche_remplace
End Sub

Sub nettoyage_style_unique(style As String)
    nom_macro = "nettoyage_style_unique"
    'Nettoyage normal
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage des italiques
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+PM
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+petites maj+exposant
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+petites maj+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+gras+petites maj+indice
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+gras+petites maj+indice+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+gras+TM
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+tout en maj+exposant
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+tout en maj+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+gras+tout en maj+indice
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+gras+tout en maj+indice+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+gras+exposant
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+gras+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+gras+indice
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+gras+indice+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+gras+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+PM
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+PM+exposant
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+PM+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+PM+indice
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+PM+indice+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+PM+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+TM
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+TM+exposant
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+TM+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+TM+indice
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+TM+indice+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+TM+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+exposant
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage italique+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage italique+indice
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage italique+indice+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage italique+soulignement
    Call loop_styles_car(style:=style, italique:=True, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras    '
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+PM
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+PM+exposant
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+PM+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+PM+Indice
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage gras+PM+Indice+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage gras+PM+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+TM
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+TM+exposant
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+TM+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+TM+Indice
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage gras+TM+Indice+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage gras+TM+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+exposant
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage gras+exposant+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage gras+indice
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage gras+indice+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage gras+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=True, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage PM'
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage PM+Exposant
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage PM+Exposant+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage PM+Indice
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage PM+indice+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage PM+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=True, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage TM'
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=False) 'ok
    'Nettoyage TM+Exposant
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage TM+Exposant+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage TM+Indice
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage TM+Indice+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage TM+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=True, exposant:=False, indice:=False, soulignement:=True) 'ok
    'Nettoyage exposant'
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=False) 'ok
    'Nettoyage Exposant+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=True, indice:=False, soulignement:=True) 'ok
    'Nettoyage indice'
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=False) 'ok
    'Nettoyage indice+soulignement
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=True, soulignement:=True) 'ok
    'Nettoyage soulignement'
    Call loop_styles_car(style:=style, italique:=False, gras:=False, petitesmajuscules:=False, toutmajuscules:=False, exposant:=False, indice:=False, soulignement:=True) 'ok
End Sub

Sub nettoyage_MEFL_listes()
Selection.HomeKey Unit:=wdStory
'Nettoyer les marques de paragraphe, pour contourner un bug de word
Selection.Find.Replacement.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont)
    With Selection.Find
        .Text = "^p"
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    'Debut macro
    Dim type_liste As String
    Dim i As Integer
    Dim nbre_paragraphes_liste As Integer
    nbre_paragraphes_liste = ActiveDocument.ListParagraphs.Count
    For i = 1 To nbre_paragraphes_liste
        'Nettoyer le paragraphe
            'Sauvegarde des infos de mise en forme
                'Sauvegarde italiques
                ActiveDocument.ListParagraphs(i).Range.Select
                With Selection.Find
                    .Font.Italic = True
                    .Replacement.Text = "<#ITALIQUES#>" & "^&" & "</#ITALIQUES#>"
                    .Wrap = wdFindStop
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Selection.HomeKey Unit:=wdStory
                Call nettoyage_recherche_remplace
                
                'Sauvegarde gras
                ActiveDocument.ListParagraphs(i).Range.Select
                With Selection.Find
                    .Font.Bold = True
                    .Replacement.Text = "<#GRAS#>" & "^&" & "</#GRAS#>"
                    .Wrap = wdFindStop
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Selection.HomeKey Unit:=wdStory
                Call nettoyage_recherche_remplace
                
                'Sauvegarde soulignement
                ActiveDocument.ListParagraphs(i).Range.Select
                With Selection.Find
                    .Font.Underline = True
                    .Replacement.Text = "<#SOULIGNEMENT#>" & "^&" & "</#SOULIGNEMENT#>"
                    .Wrap = wdFindStop
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Selection.HomeKey Unit:=wdStory
                Call nettoyage_recherche_remplace
                
                'Sauvegarde exposant
                ActiveDocument.ListParagraphs(i).Range.Select
                With Selection.Find
                    .Font.Superscript = True
                    .Replacement.Text = "<#EXPOSANT#>" & "^&" & "</#EXPOSANT#>"
                    .Wrap = wdFindStop
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Selection.HomeKey Unit:=wdStory
                Call nettoyage_recherche_remplace
                
                'Sauvegarde indice
                ActiveDocument.ListParagraphs(i).Range.Select
                With Selection.Find
                    .Font.Subscript = True
                    .Replacement.Text = "<#INDICE#>" & "^&" & "</#INDICE#>"
                    .Wrap = wdFindStop
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Selection.HomeKey Unit:=wdStory
                Call nettoyage_recherche_remplace
                
                'Sauvegarde petites majuscules
                ActiveDocument.ListParagraphs(i).Range.Select
                With Selection.Find
                    .Font.SmallCaps = True
                    .Replacement.Text = "<#PETITESMAJUSCULES#>" & "^&" & "</#PETITESMAJUSCULES#>"
                    .Wrap = wdFindStop
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Selection.HomeKey Unit:=wdStory
                Call nettoyage_recherche_remplace
                
                'Sauvegarde tout en majuscules
                ActiveDocument.ListParagraphs(i).Range.Select
                With Selection.Find
                    .Font.AllCaps = True
                    .Replacement.Text = "<#TOUTMAJUSCULES#>" & "^&" & "</#TOUTMAJUSCULES#>"
                    .Wrap = wdFindStop
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Selection.HomeKey Unit:=wdStory
                Call nettoyage_recherche_remplace
            
            'Nettoyage du paragraphe
            ActiveDocument.ListParagraphs(i).Range.style = wdStyleDefaultParagraphFont
            
    Next i
            'Retablissement des mises en forme
                ' italiques
                Selection.HomeKey Unit:=wdStory
                With Selection.Find
                    .MatchWildcards = True
                    .Text = "(\<\#ITALIQUES\#\>)(*)(\<\/\#ITALIQUES\#\>)"
                    .Replacement.Text = "\2"
                    .Replacement.Font.Italic = True
                    .Wrap = wdFindContinue
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Call nettoyage_recherche_remplace
                
                ' gras
                Selection.HomeKey Unit:=wdStory
                With Selection.Find
                    .MatchWildcards = True
                    .Text = "(\<\#GRAS\#\>)(*)(\<\/\#GRAS\#\>)"
                    .Replacement.Text = "\2"
                    .Replacement.Font.Bold = True
                    .Wrap = wdFindContinue
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Call nettoyage_recherche_remplace
                
                ' soulignement
                Selection.HomeKey Unit:=wdStory
                With Selection.Find
                    .MatchWildcards = True
                    .Text = "(\<\#SOULIGNEMENT\#\>)(*)(\<\/\#SOULIGNEMENT\#\>)"
                    .Replacement.Text = "\2"
                    .Replacement.Font.Underline = True
                    .Wrap = wdFindContinue
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Call nettoyage_recherche_remplace
                
                ' exposant
                Selection.HomeKey Unit:=wdStory
                With Selection.Find
                    .MatchWildcards = True
                    .Text = "(\<\#EXPOSANT\#\>)(*)(\<\/\#EXPOSANT\#\>)"
                    .Replacement.Text = "\2"
                    .Replacement.Font.Superscript = True
                    .Wrap = wdFindContinue
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Call nettoyage_recherche_remplace
                
                ' indice
                Selection.HomeKey Unit:=wdStory
                With Selection.Find
                    .MatchWildcards = True
                    .Text = "(\<\#INDICE\#\>)(*)(\<\/\#INDICE\#\>)"
                    .Replacement.Text = "\2"
                    .Replacement.Font.Subscript = True
                    .Wrap = wdFindContinue
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Call nettoyage_recherche_remplace
                
                ' petites majuscules
                Selection.HomeKey Unit:=wdStory
                With Selection.Find
                    .MatchWildcards = True
                    .Text = "(\<\#PETITESMAJUSCULES\#\>)(*)(\<\/\#PETITESMAJUSCULES\#\>)"
                    .Replacement.Text = "\2"
                    .Replacement.Font.Subscript = True
                    .Wrap = wdFindContinue
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Call nettoyage_recherche_remplace
                
                ' tout majuscules
                Selection.HomeKey Unit:=wdStory
                With Selection.Find
                    .MatchWildcards = True
                    .Text = "(\<\#TOUTMAJUSCULES\#\>)(*)(\<\/\#TOUTMAJUSCULES\#\>)"
                    .Replacement.Text = "\2"
                    .Replacement.Font.Subscript = True
                    .Wrap = wdFindContinue
                End With
                Selection.Find.Execute Replace:=wdReplaceAll
                Call nettoyage_recherche_remplace
                
    Selection.HomeKey Unit:=wdStory
End Sub

Sub nettoyage_MEFL_liens()
    'Nettoyage des liens dans le corps de texte
    Dim i As Integer
    For i = 1 To ActiveDocument.Hyperlinks.Count
        'Nettoyage des mises en forme sur les liens
        ActiveDocument.Hyperlinks(i).Range.style = wdStyleDefaultParagraphFont
    Next i
    'Nettoyage des liens dans les notes de bas de page
    Call activer_notes
    Dim note As footnote
    Dim champ As Field
    'Nettoyer les liens hypertextes
    For Each note In Application.ActiveDocument.Footnotes       'pour chaque note dans les notes :
       For Each champ In note.Range.Fields                      'et pour chaque champ dans la note active :
           If champ.Type = wdFieldHyperlink Then                'voir si le champ est un lien
               champ.Select
               Selection.Range.style = wdStyleDefaultParagraphFont
               Selection.Collapse Direction:=wdCollapseEnd
               
           End If
               
       Next champ                                               'Traiter le champ suivant dans la note, en lui appliquant les actions pr_c_dentes
    Next note                                                   'Traiter la note suivante; etc.
    Selection.HomeKey Unit:=wdStory                             'Placer le curseur au d_but des notes
    Call nettoyage_recherche_remplace
    Call activer_cdt
    Call nettoyage_surlignement
End Sub

Sub loop_styles_car(style As String, italique As Boolean, gras As Boolean, soulignement As Boolean, exposant As Boolean, indice As Boolean, petitesmajuscules As Boolean, toutmajuscules As Boolean)
    nom_macro = "loop_styles_car"
    Call nettoyage_recherche_remplace
    Selection.HomeKey Unit:=wdStory
    With Selection.Find
        .style = style
        .Font.Italic = italique
        .Font.Bold = gras
        .Font.Underline = soulignement
        .Font.Superscript = exposant
        .Font.Subscript = indice
        .Font.SmallCaps = petitesmajuscules
        .Font.AllCaps = toutmajuscules
        .Forward = True
        .Wrap = wdFindStop
        Do While Selection.Find.Execute
            Selection.style = style
            With Selection
            .Font.Italic = italique
            .Font.Bold = gras
            .Font.Underline = soulignement
            .Font.Superscript = exposant
            .Font.Subscript = indice
            .Font.SmallCaps = petitesmajuscules
            .Font.AllCaps = toutmajuscules
            End With
            Selection.Collapse Direction:=wdCollapseEnd
        Loop
    End With
End Sub

Sub activer_cdt()
    nom_macro = "activer_cdt"
    'Macro plaant le curseur dans le corps de textee
    Call activer_affichage_normal
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then    'v_rifier que les notes sont activ_es
        ActiveWindow.Panes(2).Close     'Si oui, les fermer
        Selection.HomeKey Unit:=wdStory
    Else
        On Error Resume Next
        ActiveWindow.Panes(2).Close
    End If
End Sub

Sub kill_style(style As String)
    nom_macro = "kill_style"
    'Suppression du style d_fini en variable
    On Error Resume Next
    ActiveDocument.Styles(style).Delete
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN MISES EN FORME LOCALES ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''





''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT OBSOLETE ''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'Sub activation_champs()
'    nom_macro = "activation_champs"
'    'Activation des champs (montre le fichier ou le texte ins_r_ dans un champ, au lieu de montrer le code du champ)
'    Dim champ As Field
'    For Each champ In ActiveDocument.Fields
'    With Selection
'        .GoTo What:=wdGoToField
'        .Expand unit:=wdWord
'        If .Fields.Count = 1 Then .Fields(1).ShowCodes = False
'    End With
'    Next champ
'    Selection.HomeKey unit:=wdStory
'End Sub
'
'
'Sub activation_champs_notes()
'    nom_macro = "activation_champs_notes"
'    'Activation des champs (montre le fichier ou le texte ins_r_ dans un champ, au lieu de montrer le code du champ) - dans les notes de bas de page
'    Dim note As Footnote
'    Dim champ As Field
'    For Each note In Application.ActiveDocument.Footnotes       'pour chaque note dans les notes :
'       For Each champ In note.Range.Fields                      'et pour chaque champ dans la note active :
'           With Selection
'               .GoTo What:=wdGoToField
'               .Expand unit:=wdWord                             'Selectionner le champ
'               If .Fields.Count = 1 Then
'               .Fields(1).ShowCodes = False                     'L'afficher sans code
'               End If
'           End With
'           Selection.MoveLeft unit:=wdCharacter, Count:=1       'Avancer le curseur d'un point vers la droite, pour d_selectionner le champ
'       Next champ                                               'Traiter le champ suivant dans la note, en lui appliquant les actions pr_c_dentes
'    Next note                                                   'Traiter la note suivante; etc.
'    Selection.HomeKey unit:=wdStory                             'Placer le curseur au d_but des notes
'End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN OBSOLETE ''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''




