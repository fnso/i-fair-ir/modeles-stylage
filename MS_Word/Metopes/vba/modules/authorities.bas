Attribute VB_Name = "authorities"
Sub author(control As IRibbonControl)
    Dim result1 As String
    Dim result As String
    Dim styleName As String
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload author_selector_fr
        Load author_selector_fr
        author_selector_fr.Show
        result1 = Right(author_selector_fr.dropdown_aut.Value, 4)
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload author_selector_es
        Load author_selector_es
        author_selector_es.Show
        result1 = Right(author_selector_es.dropdown_aut.Value, 4)
    Case Else 'anglais par d�faut
        Unload author_selector_en
        Load author_selector_en
        author_selector_en.Show
        result1 = Right(author_selector_en.dropdown_aut.Value, 4)
    End Select
    
    result = Left(result1, 3)
    
    If result = "rcp" Then
    result = "aut rcp"
    End If
    
    'MsgBox "Result =" + result
    
    If result = "" Then
    Else
        styleName = "TEI_author:" + result
        On Error Resume Next
            Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
            With the_style
                .BaseStyle = ActiveDocument.Styles("TEI_author:")
            End With
        paragraphs.paragraphs.style = ActiveDocument.Styles(styleName)
    End If
    
End Sub

Sub editor(control As IRibbonControl)
    Dim result1 As String
    Dim result As String
    Dim styleName As String
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload editor_selector_fr
        Load editor_selector_fr
        editor_selector_fr.Show
        result1 = Right(editor_selector_fr.dropdown_aut.Value, 4)
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload editor_selector_es
        Load editor_selector_es
        editor_selector_es.Show
        result1 = Right(editor_selector_es.dropdown_aut.Value, 4)
    Case Else 'anglais par d�faut
        Unload editor_selector_es
        Load editor_selector_es
        editor_selector_en.Show
        result1 = Right(editor_selector_en.dropdown_aut.Value, 4)
    End Select
    
    
    result = Left(result1, 3)
        
    If result = "" Then
    Else
    styleName = "TEI_editor:" + result
    On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
        With the_style
                .BaseStyle = ActiveDocument.Styles("TEI_editor:")
        End With
    paragraphs.paragraphs.style = ActiveDocument.Styles(styleName)
    End If
    
End Sub
Sub authority_biography(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authority_biography")
End Sub
Sub authority_affiliation(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authority_affiliation")
End Sub
Sub authority_mail(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authority_mail")
End Sub
Sub neutral(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_neutral-inline")
End Sub
Sub authorities(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authorities")
End Sub
Sub affiliation(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_affiliation-inline")
End Sub
Sub mail(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_mail-inline")
End Sub
Sub authorities_mail(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authorities_mails")
End Sub
'Sub paraCollaborateurs(control As IRibbonControl)
'    Selection.style = ActiveDocument.Styles("adcollaborateurs")
'End Sub
Sub author_inline(control As IRibbonControl)
    Dim result1 As String
    Dim result As String
    Dim styleName As String
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload author_selector_fr
        Load author_selector_fr
        author_selector_fr.Show
        result1 = Right(author_selector_fr.dropdown_aut.Value, 4)
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload author_selector_es
        Load author_selector_es
        author_selector_es.Show
        result1 = Right(author_selector_es.dropdown_aut.Value, 4)
    Case Else 'anglais par d�faut
        Unload author_selector_en
        Load author_selector_en
        author_selector_en.Show
        result1 = Right(author_selector_en.dropdown_aut.Value, 4)
    End Select
    
    result = Left(result1, 3)
    
    If result = "" Then
    Else
    styleName = "TEI_author-inline:" + result
    On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeCharacter)
            With the_style
                .BaseStyle = ActiveDocument.Styles("TEI_author-inline:")
            End With
    Selection.Range.style = ActiveDocument.Styles(styleName)
        With ActiveDocument.Styles(styleName).Font
         With .Shading
             .Texture = wdTexture20Percent
             .ForegroundPatternColor = wdColorAutomatic
             .BackgroundPatternColor = wdColorAutomatic
         End With
        End With
    End If
    
End Sub

Sub editor_inline(control As IRibbonControl)
    Dim result1 As String
    Dim result As String
    Dim styleName As String
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Unload editor_selector_fr
        Load editor_selector_fr
        editor_selector_fr.Show
        result1 = Right(editor_selector_fr.dropdown_aut.Value, 4)
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Unload editor_selector_es
        Load editor_selector_es
        editor_selector_es.Show
        result1 = Right(editor_selector_es.dropdown_aut.Value, 4)
    Case Else 'anglais par d�faut
        Unload editor_selector_en
        Load editor_selector_en
        editor_selector_en.Show
        result1 = Right(editor_selector_en.dropdown_aut.Value, 4)
    End Select
    
    result = Left(result1, 3)
    
    If result = "" Then
    Else
    styleName = "TEI_editor-inline:" + result
    On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeCharacter)
            With the_style
                .BaseStyle = ActiveDocument.Styles("TEI_editor-inline:")
            End With
    Selection.Range.style = ActiveDocument.Styles(styleName)
    With ActiveDocument.Styles(styleName).Font
         With .Shading
             .Texture = wdTexture10Percent
             .ForegroundPatternColor = wdColorAutomatic
             .BackgroundPatternColor = wdColorAutomatic
         End With
        End With
    End If
    
End Sub
