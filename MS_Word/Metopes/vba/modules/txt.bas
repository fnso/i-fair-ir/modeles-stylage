Attribute VB_Name = "txt"
Sub normal(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleNormal)
End Sub
Sub paragraph_consecutive(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_paragraph_consecutive")
End Sub
Sub epigraph(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_epigraph")
End Sub
Sub bibl_reference_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_bibl_reference-inline")
End Sub
Sub paragraph_break(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_paragraph_break")
End Sub
Sub footnote(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleFootnoteText)
End Sub
Sub endnote(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleEndnoteText)
End Sub
Sub nlocal(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_localnote")
End Sub
'Code
Sub code(control As IRibbonControl)
    Dim result As String
    Dim styleName As String
    Unload code_selector
    Load code_selector
    code_selector.Show
    result = code_selector.dropdown_code.Value

    If result <> "" And result <> "undefined" Then
        styleName = "TEI_code:" + result
        On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
        the_style.BaseStyle = ActiveDocument.Styles("TEI_code")
        Selection.paragraphs.style = ActiveDocument.Styles(styleName)
    Else
        Selection.paragraphs.style = ActiveDocument.Styles("TEI_code")
    End If
    
    Dim startPara As String
    Dim endPara As String
    Dim characterToRepeat As String
    Dim repeatEmdash As Integer
    characterToRepeat = ChrW(&H2014)
    repeatEmdash = 50
    startPara = ChrW(&H2014) & " <code> " & String(repeatEmdash, characterToRepeat)
    endPara = ChrW(&H2014) & " </code> " & String(repeatEmdash, characterToRepeat)
    
    Call section("TEI_code_start", startPara, "TEI_code_end", endPara)
End Sub
Sub code_inline(control As IRibbonControl)
    Dim result As String
    Dim styleName As String
    Unload code_selector
    Load code_selector
    code_selector.Show
    result = code_selector.dropdown_code.Value

    If result <> "" And result <> "undefined" Then
        styleName = "TEI_code-inline:" + result
        On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeCharacter)
        the_style.BaseStyle = ActiveDocument.Styles("TEI_code-inline")
        Selection.style = ActiveDocument.Styles(styleName)
    Else
        Selection.style = ActiveDocument.Styles("TEI_code-inline")
    End If
End Sub

