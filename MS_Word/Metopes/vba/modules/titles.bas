Attribute VB_Name = "titles"
Sub title_main(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleTitle)
End Sub
Sub T1(control As IRibbonControl)
Attribute T1.VB_Description = "Application du style Titre 1"
Attribute T1.VB_ProcData.VB_Invoke_Func = "TemplateProject.NewMacros.T1"
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading1)
End Sub
Sub T2(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading2)
End Sub
Sub T3(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading3)
End Sub
Sub T4(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading4)
End Sub
Sub T5(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading5)
End Sub
Sub T6(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading6)
End Sub
Sub T7(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading7)
End Sub
Sub T8(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading8)
End Sub
Sub T9(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading9)
End Sub
'Sub listnum(control As IRibbonControl)
'    ListGalleries(wdNumberGallery).ListTemplates(1).Name = ""
'    Selection.Range.ListFormat.ApplyListTemplate ListTemplate:=ListGalleries(wdNumberGallery).ListTemplates(1), ContinuePreviousList:= _
'        False, ApplyTo:=wdListApplyToWholeList, DefaultListBehavior:=wdWord10ListBehavior
'
'    Selection.style = ActiveDocument.Styles("adlistenum1")
'
'End Sub
Sub title_sup(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_title:sup")
End Sub
Sub title_trl(control As IRibbonControl)
    ApplyStyleLang "TEI_title:trl"
End Sub
Sub title_sub(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_title:sub")
End Sub
Sub setheading(control As IRibbonControl)
    Unload type_selector_fr
    Load type_selector_fr
    type_selector_fr.Show
    result = type_selector_fr.dropdown_type.Value
    current_style_name = ActiveDocument.ActiveWindow.Selection.style
    current_outlinelevel = Selection.paragraphs(1).OutlineLevel
    styleName = "TEI_" + current_style_name + "+" + result
    
    If result = "<alt�>" Then
        Call customheading
    Else
            'Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
            On Error Resume Next
            Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
            If Err.Number <> 0 Then
                Set the_style = ActiveDocument.Styles(styleName)
            End If
            On Error GoTo 0
            Selection.paragraphs.style = the_style
    'paragraphs.paragraphs.style = ActiveDocument.Styles(styleName)
    End If
    
End Sub

Sub customheading()
    Unload typetext_fr
    Load typetext_fr
    typetext_fr.Show
    resultCustom = typetext_fr.TextBox.Value

    current_style_name = ActiveDocument.ActiveWindow.Selection.style
    'current_outlinelevel = Selection.paragraphs(1).OutlineLevel
    styleName = "TEI_" + current_style_name + "+" + resultCustom
    
    On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
    paragraphs.paragraphs.style = ActiveDocument.Styles(styleName)
End Sub
