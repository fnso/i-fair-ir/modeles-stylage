Attribute VB_Name = "menus_conf"
Sub getSupertip(control As IRibbonControl, ByRef ReturnedVal)
  Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
       Select Case control.ID
        Case "archeoCB"
        ReturnedVal = "Menu regroupant les styles d�di�s au traitement des articles et notices arch�o."
        Case "recensionsCB"
        ReturnedVal = "Menu regroupant les styles d�di�s au traitement des recensions."
        Case "extendedCB"
        ReturnedVal = "Mod�le �tendu : surtitre, niveaux de titres compl�mentaires, financeur, chap�, donn�es et publications associ�es, code informatique, encadr�, sous-structure, adlocalnote, media, annexe, outils +."
        Case "citationsCB"
        ReturnedVal = "Modalit�s de citations : linguistique, po�sie, th��tre + entretien."
        Case "fullmodeleCB"
        ReturnedVal = "Active l'affichage de tous les styles (coche les 5 menus pr�c�dents)."
   End Select
  End Select
End Sub


    Sub logMeta(control As IRibbonControl)
    
    Dim style As style
    
'On recherche notre style:adCAauteur
    'On se met au debut du doc
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start
        With Selection.Find
            .ClearFormatting
                .style = "adCAauteur"
    'tant qu'on le trouve on stocke le texte trouv� avec ce style dans une variable
            While .Execute
                Valeur = Valeur & "  [" & Selection.Text & "]" & vbCr
    'On se met a la suite pour poursuivre la recherce
                Selection.Start = Selection.End + 1
                Selection.End = Selection.Start
            Wend
        End With
        
'On recherche notre style adauteur
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start
        With Selection.Find
            .ClearFormatting
             .style = "adauteur"
            While .Execute
                auteurname = Replace(Selection.Text, vbCr, "")
                AuteurNameClean = (Replace(auteurname, "*", ""))
                Valeur = Valeur & "  [" & AuteurNameClean & "]" & vbCr
                Selection.Start = Selection.End + 1
                Selection.End = Selection.Start
            Wend
        End With
        
  'On recherche notre style:adCAauteur
    'On se met au debut du doc
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start
        With Selection.Find
            .ClearFormatting
                .style = "adCAArcheoresponsable"
    'tant qu'on le trouve on stocke le texte trouv� avec ce style dans une variable
            While .Execute
                ValeurCollab = ValeurCollab & "  [" & Selection.Text & "]" & vbCr
    'On se met a la suite pour poursuivre la recherce
                Selection.Start = Selection.End + 1
                Selection.End = Selection.Start
            Wend
        End With
    
    'On recherche notre style adcollaborateur
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start
        With Selection.Find
            .ClearFormatting
             .style = "adcollaborateur"
            While .Execute
                CollaborateurName = Replace(Selection.Text, vbCr, "")
                CollaborateurNameClean = (Replace(CollaborateurName, "*", ""))
                ValeurCollab = ValeurCollab & "  [" & CollaborateurNameClean & "]" & vbCr
                Selection.Start = Selection.End + 1
                Selection.End = Selection.Start
            Wend
        End With
        
'On recherche notre style adCAaffiliation
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start '
        With Selection.Find
            .ClearFormatting
            .style = "adCAaffiliation"
            While .Execute
                ValeurAff = ValeurAff & "  [" & Selection.Text & "]" & vbCr
                Selection.Start = Selection.End + 1
                Selection.End = Selection.Start
            Wend
        End With
        
 'Si la variable est vide aprs la recherche du style de caractres adCAaffiliation alors on passe � la recherche du style de paragraphe adrattachementnt
  If IsEmpty(ValeurAff) Then
'On recherche le style adrattachement
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start
        With Selection.Find
            .ClearFormatting
            .style = "adrattachement"
            While .Execute
                AffString = Replace(Selection.Text, vbCr, "")
                ValeurAff = ValeurAff & "  [" & AffString & "]" & vbCr
                Selection.Start = Selection.End
                Selection.End = Selection.Start
            Wend
       End With
       
       'Si vide, on relance la recherche dans les notes
       If IsEmpty(ValeurAff) Then
        'MsgBox "On devrait chercher dans les notes"
        ActiveDocument.StoryRanges(wdFootnotesStory).Select
        With Selection.Find
            .style = "adCAaffiliation"
        While .Execute
                AffString = Replace(Selection.Text, vbCr, "")
                ValeurAff = ValeurAff & "  [" & AffString & "]" & vbCr
                Selection.Start = Selection.End + 1
                Selection.End = Selection.Start
            Wend
        End With
       End If
       
    End If
    
    Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        'MsgBox "Auteur(s) : " & vbCr + Valeur & vbCrLf & "Collaborateur(s) :" & vbCr + ValeurCollab & vbCrLf & "Affiliation(s) : " & vbCr + ValeurAff, vbOKOnly, "M�tadonn�es"
        MsgBox "Auteur(s) / Collaborateur(s) : " & vbCr + Valeur & ValeurCollab & vbCrLf & "Affiliation(s) : " & vbCr + ValeurAff, vbOKOnly, "M�tadonn�es"
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        MsgBox "Autores : " & vbCr + Valeur & vbCrLf & "Afiliaciones : " & vbCr + ValeurAff, vbOKOnly, "Metadatos"
    Case Else 'autre lang = en
        MsgBox "Authors : " & vbCr + Valeur & vbCrLf & "Affiliations : " & vbCr + ValeurAff, vbOKOnly, "Metadata"
    End Select
    
    End Sub
    
' apply_style_to_selection est utilis� dans init_and_commons -> Function paragraphs
Sub GetPressedStyleMode(control As IRibbonControl, ByRef ReturnedVal)
    Select Case control.ID
        Case "apply_style_to_selection"
            ReturnedVal = apply_style_to_selection
    End Select
End Sub

Sub ApplyStyleMode(control As IRibbonControl, pressed As Boolean)
    Select Case control.ID
         Case "apply_style_to_selection"
            If pressed = True Then
                apply_style_to_selection = True
            End If
            If pressed = False Then
                apply_style_to_selection = False
            End If
    End Select
End Sub
