Attribute VB_Name = "figures"
Sub figure_title(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_title")
End Sub
Sub figure_caption(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_caption")
End Sub
Sub figure_credits(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_credits")
End Sub
Sub figure_alternative(control As IRibbonControl)
    #If Mac Then
        Call insertFigMac
    #Else
        Call insertFigWin
    #End If
    paragraphs.style = ActiveDocument.Styles("TEI_figure_alternative")
End Sub
Sub formula(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_formula")
End Sub
Sub formula_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_formula-inline")
End Sub
Sub illnum(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_figure_num_inline")
End Sub
Sub illcredcar(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_figure_credits_inline")
End Sub
Sub illsourcecar(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_figure_source_inline")
End Sub


Sub mmlpara(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_formula-mml")
End Sub
Sub figuregrp_section(control As IRibbonControl)
    Dim startPara As String
    Dim endPara As String
    Dim characterToRepeat As String
    Dim repeatEmdash As Integer
    characterToRepeat = ChrW(&H2014)
    repeatEmdash = 50
    startPara = ChrW(&H2014) & " <figure-grp> " & String(repeatEmdash, characterToRepeat)
    endPara = ChrW(&H2014) & " </figure-grp> " & String(repeatEmdash, characterToRepeat)
    Call section("TEI_figure-grp_start", startPara, "TEI_figure-grp_end", endPara)
End Sub
Sub figure_section(control As IRibbonControl)
    Dim startPara As String
    Dim endPara As String
    Dim characterToRepeat As String
    Dim repeatEmdash As Integer
    characterToRepeat = ChrW(&H2014)
    repeatEmdash = 50
    startPara = ChrW(&H2014) & " <figure> " & String(repeatEmdash, characterToRepeat)
    endPara = ChrW(&H2014) & " </figure> " & String(repeatEmdash, characterToRepeat)
    Call section("TEI_figure_start", startPara, "TEI_figure_end", endPara)
End Sub

Sub insertFig(control As IRibbonControl)
    #If Mac Then
        Call insertFigMac
    #Else
        Call insertFigWin
    #End If
    paragraphs.style = ActiveDocument.Styles(wdStyleNormal)
End Sub
Sub insertFigMac()
    Dim FolderPath As String
    Dim RootFolder As String
    Dim Scriptstr As String
    Dim IconoFolder As String
    Dim FilePath

    On Error Resume Next
    
    FilePath = ActiveDocument.Path
    RootFolder = Left(ActiveDocument.Path, InStrRev(ActiveDocument.Path, "/"))
    IconoFolder = RootFolder & "icono/br"
    
    If Len(Dir(IconoFolder, vbDirectory)) = 0 Then
        RootFolder = MacScript("return POSIX file (""" & FilePath & """) as string")
            Scriptstr = "return POSIX path of (choose file with prompt ""File Selection""" & _
                " default location alias """ & RootFolder & """) as string"
   
        FolderPath = MacScript(Scriptstr)
        On Error GoTo 0
    Else
        RootFolder = MacScript("return POSIX file (""" & IconoFolder & """) as string")
            Scriptstr = "return POSIX path of (choose file with prompt ""File Selection""" & _
                " default location alias """ & RootFolder & """) as string"
   
        FolderPath = MacScript(Scriptstr)
        On Error GoTo 0
    End If
    
    If FolderPath <> "" Then
        Selection.InlineShapes.AddPicture fileName:= _
            FolderPath, LinkToFile:=True, SaveWithDocument:=False

    End If
End Sub

Sub insertFigWin()
Dim pickFile As FileDialog
Dim FolderPath As String
Dim IconoFolder As String

Set pickFile = Application.FileDialog(msoFileDialogFilePicker)

    With pickFile
        .Title = "Select Picture"
        .AllowMultiSelect = False
        If .Show <> -1 Then Exit Sub
        RootFolder = Left(ActiveDocument.Path, InStrRev(ActiveDocument.Path, "\"))
        IconoFolder = RootFolder & "\icono\br"
        
        If InStr(IconoFolder, "\\") > 0 Then
            FolderPath = ActiveDocument.Path
        Else
            FolderPath = IconoFolder
        End If
        If .SelectedItems.Count > 0 Then
            FolderPath = .SelectedItems(1)
        End If
    End With
 
    If FolderPath <> "" Then
        Selection.InlineShapes.AddPicture fileName:= _
            FolderPath, LinkToFile:=True, SaveWithDocument:=False
    End If

End Sub

Sub InsertAltTextCaptions()
Dim AltTextCaption As String
Dim ImageRange As Range
Dim AltTextParagraph As Paragraph
Dim i As Integer

Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst
With ActiveDocument
    For i = 1 To .InlineShapes.Count
      If Not .InlineShapes(i).AlternativeText = "" Then 'V�rifie si le text alternatif est vide
        AltTextCaption = .InlineShapes(i).AlternativeText
        Set ImageRange = .InlineShapes(i).Range
        ImageRange.Collapse Direction:=wdCollapseEnd
        ImageRange.InsertAfter vbCr
        ImageRange.InsertAfter AltTextCaption
        Set AltTextParagraph = ImageRange.paragraphs.Last
        AltTextParagraph.style = ActiveDocument.Styles("TEI_figure_alttext")
      End If
    Next
End With

End Sub
Sub DeleteAltTextCaptions()
    With ActiveDocument.Content.Find
    .ClearFormatting
    .style = ActiveDocument.Styles("TEI_figure_alttext")
    Do While .Execute(FindText:="", Forward:=True, Format:=True) = True
    .Parent.Delete
    Loop
    End With
End Sub

Sub DisplayAltTextCaptions(control As IRibbonControl)
    With ActiveDocument.Content.Find
    .style = "TEI_figure_alttext"
        If .Execute = True Then
        Call DeleteAltTextCaptions
    Else
        Call InsertAltTextCaptions
    End If
End With
End Sub
