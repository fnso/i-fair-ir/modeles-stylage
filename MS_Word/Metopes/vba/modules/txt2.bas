Attribute VB_Name = "txt2"
'Encadr�s
Sub floatingText(control As IRibbonControl)
    Dim startPara As String
    Dim endPara As String
    Dim characterToRepeat As String
    Dim repeatEmdash As Integer
    characterToRepeat = ChrW(&H2014)
    repeatEmdash = 50
    startPara = ChrW(&H2014) & " <floatingText> " & String(repeatEmdash, characterToRepeat)
    endPara = ChrW(&H2014) & " </floatingText> " & String(repeatEmdash, characterToRepeat)
    Call sectionFT("TEI_floatingText_start", startPara, "TEI_floatingText_end", endPara)
End Sub
Sub floatingTextTitle(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_floatingText_title")
End Sub
Sub inclusion(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_xi_include")
End Sub
Sub section_author(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_section_author")
End Sub
' Entretienn
Sub question(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_question")
End Sub
Sub answer(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_answer")
End Sub

Sub TEIlocal(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_localpara")
End Sub
Sub TEIlocalinline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_localcar")
End Sub

Sub signature(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_signature")
End Sub
