Attribute VB_Name = "review"
Sub reviewed_reference(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_reviewed_reference")
End Sub
Sub reviewed_title_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_reviewed_title-inline")
End Sub
Sub reviewed_author_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_reviewed_author-inline")
End Sub
Sub reviewed_date_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_reviewed_date-inline")
End Sub
Sub review_section(control As IRibbonControl)
    Dim startPara As String
    Dim endPara As String
    Dim characterToRepeat As String
    Dim repeatEmdash As Integer
    characterToRepeat = ChrW(&H2014)
    repeatEmdash = 50
    startPara = ChrW(&H2014) & " <bibl-review> " & String(repeatEmdash, characterToRepeat)
    endPara = ChrW(&H2014) & " </bibl-review> " & String(repeatEmdash, characterToRepeat)
    Call section("TEI_review_start", startPara, "TEI_review_end", endPara)
End Sub
   

