Attribute VB_Name = "back"
Sub bibl_start(control As IRibbonControl)
    'Dim SelParaFirst As Paragraph
    'Set SelParaFirst = Selection.paragraphs.First
    'SelParaFirst.Range.InsertParagraphBefore
    'SelParaFirst.Previous.style = ActiveDocument.Styles("TEI_bibl_start")
    'ActiveDocument.Range(SelParaFirst.Previous.Range.Start, SelParaFirst.Previous.Range.End - 1).Text = "##### BIBLIOGRAPHY START #####"
    paragraphs.style = ActiveDocument.Styles("TEI_bibl_start")
End Sub
Sub bibl_reference(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_bibl_reference")
End Sub
Sub appendix_start(control As IRibbonControl)
    'Dim SelParaFirst As Paragraph
    'Set SelParaFirst = Selection.paragraphs.First
    'SelParaFirst.Range.InsertParagraphBefore
    'SelParaFirst.Previous.style = ActiveDocument.Styles("TEI_appendix_start")
    'ActiveDocument.Range(SelParaFirst.Previous.Range.Start, SelParaFirst.Previous.Range.End - 1).Text = "##### APPENDIX START #####"
    paragraphs.style = ActiveDocument.Styles("TEI_appendix_start")
End Sub
