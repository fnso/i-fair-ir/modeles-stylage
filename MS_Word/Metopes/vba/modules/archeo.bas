Attribute VB_Name = "archeo"
Sub archeo_type(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_archeoCHR_type")
End Sub
Sub archeo_IDpatriarche(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_archeoCHR_IDpatriarche")
End Sub
Sub archeo_reportlink(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_reportlink")
End Sub
Sub archeo_fieldwork_nature(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_fieldwork_method")
End Sub
Sub archeo_keywords_subjects_excavationyear(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:excavationyear")
End Sub
Sub archeo_keywords(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_keywords_subjects")
End Sub
Sub archeo_keywords_chrono(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_keywords_subjects:chronology")
End Sub
Sub archeo_link(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeo_link")
End Sub
Sub archeo_fieldwork_team(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoART_team")
End Sub
Sub archeo_nat_authority(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoART_nat_authority")
End Sub
Sub archeo_fieldwork_holder(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoART_holder")
End Sub
Sub archeo_fieldwork_holder2(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_holder")
End Sub
Sub archeo_IDmission(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoART_IDmission:EFA")
End Sub
Sub archeo_keywords_pactols(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_keywords_subjects:pactols")
End Sub
Sub archeo_fieldwork_date(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoART_date")
End Sub
Sub archeo_fieldwork_years(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_fieldwork_year")
End Sub
Sub archeo_authority(control As IRibbonControl)
   Selection.style = ActiveDocument.Styles("TEI_archeoCHR_authority")
End Sub
Sub archeo_coord_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_name:edt")
End Sub
Sub archeo_resp_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_name:fld")
End Sub
Sub archeo_redac_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_name:aut")
End Sub
Sub archeo_authorresp_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_name:aut fld")
End Sub
Sub archeo_affiliation_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_archeoCHR_aff_inline")
End Sub
