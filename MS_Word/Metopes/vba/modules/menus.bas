Attribute VB_Name = "menus"
Public myRibbon As IRibbonUI
Public standard As String
Public archeoCB As String
Public recensionsCB As String
Public extendedCB As String
Public citationsCB As String
Public fullmodeleCB As String
Dim ReturnedVal As Boolean
Sub OnLoad(ribbon As IRibbonUI)
    Set myRibbon = ribbon
    standard = True
    apply_style_to_selection = False
End Sub
Sub RefreshRibbon()
Dim Msg As String

Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
        Msg = "Perte du lien avec le ruban, veuillez red�marrer l�application."
        ErrorMsg = "Erreur"
    Case 1034, 3082, 11274, 2058, 9226, 1050 'es
        Msg = "Message � traduire en espagnol."
        ErrorMsg = "Error"
    Case Else 'autre lang = en
        Msg = "Error on Ribbon loading. Please restart Word."
        ErrorMsg = "Error"
    End Select
    
    On Error GoTo ErrorMsg
    myRibbon.Invalidate
    DoEvents
    'On Error GoTo 0
    Exit Sub
    
ErrorMsg:

    MsgBox ErrorMsg & " " & Err.Number & " (" & Err.Description & ")", _
    vbCritical, Msg
    

End Sub
Sub GetVisibleArcheo(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = archeoCB
End Sub
Sub GetVisibleRecensions(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = recensionsCB
End Sub
Sub GetVisibleExtended(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = extendedCB
End Sub
Sub GetVisibleCitations(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = citationsCB
End Sub
Sub GetVisibleFullmodele(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = fullmodeleCB
End Sub
Sub GetPressed(control As IRibbonControl, ByRef ReturnedVal)
    Select Case control.ID
        Case "archeoCB"
            ReturnedVal = archeoCB
        Case "recensionsCB"
            ReturnedVal = recensionsCB
        Case "extendedCB"
            ReturnedVal = extendedCB
        Case "citationsCB"
            ReturnedVal = citationsCB
        Case "fullmodeleCB"
            ReturnedVal = fullmodeleCB
    End Select
End Sub
Sub ToggleVisibility(control As IRibbonControl, pressed As Boolean)
    Select Case control.ID
        Case "archeoCB"
            If pressed = True Then
                archeoCB = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                archeoCB = False
                Call RefreshRibbon
            End If
        Case "recensionsCB"
            If pressed = True Then
                recensionsCB = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                recensionsCB = False
                Call RefreshRibbon
            End If
        Case "extendedCB"
            If pressed = True Then
                extendedCB = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                extendedCB = False
                Call RefreshRibbon
            End If
        Case "citationsCB"
            If pressed = True Then
                citationsCB = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                citationsCB = False
                Call RefreshRibbon
            End If
        Case "fullmodeleCB"
            If pressed = True Then
                archeoCB = True
                recensionsCB = True
                extendedCB = True
                citationsCB = True
                fullmodeleCB = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                archeoCB = False
                recensionsCB = False
                extendedCB = False
                citationsCB = False
                fullmodeleCB = False
                Call RefreshRibbon
            End If
    End Select
End Sub
