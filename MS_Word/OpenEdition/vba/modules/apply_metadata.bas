Attribute VB_Name = "apply_metadata"
Sub title_sup(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_title:sup")
    Call SetOEDocumentProperty
End Sub

Sub title_main(control As IRibbonControl)
    Call title_main_cmd
End Sub
Sub title_main_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleTitle)
End Sub

Sub title_sub(control As IRibbonControl)
    Call title_sub_cmd
End Sub
Sub title_sub_cmd()
    paragraphs.style = ActiveDocument.Styles("TEI_title:sub")
End Sub

Sub author_aut(control As IRibbonControl)
    Call author_aut_cmd
End Sub
Sub author_aut_cmd()
    paragraphs.style = ActiveDocument.Styles("TEI_author:aut")
End Sub

Sub editor_trl(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_editor:trl")
End Sub
Sub editor_ctb(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_editor:ctb")
End Sub
Sub authority_biography(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authority_biography")
End Sub
Sub authority_mail(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authority_mail")
End Sub
Sub authority_affiliation(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_authority_affiliation")
End Sub
Sub keywords_subjects_chronology(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:chronology")
End Sub
Sub keywords_subjects_geography(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:geography")
End Sub
Sub keywords_subjects_subject(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:subject")
End Sub
Sub keywords_subjects_work(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:work")
End Sub
Sub keywords_subjects_propernoun(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:propernoun")
End Sub
Sub keywords_subjects_personcited(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:personcited")
End Sub
Sub date_reception(control As IRibbonControl)
    Call format_date(style_name:="TEI_date_reception")
End Sub
Sub date_acceptance(control As IRibbonControl)
    Call format_date(style_name:="TEI_date_acceptance")
End Sub
'Sub language(control As IRibbonControl)
'    paragraphs.style = ActiveDocument.Styles("TEI_language")
'End Sub
Sub document_number(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_document_number")
End Sub
Sub pagination(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_pagination")
End Sub
Sub editor_edt(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_editor:edt")
End Sub
Sub editor_fld(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_editor:fld")
End Sub
Sub keywords_subjects_excavationyear(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_keywords_subjects:excavationyear")
End Sub
Sub patriarcheid(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_patriarcheid")
End Sub
Sub abstract(control As IRibbonControl)
    ApplyStyleLang "TEI_abstract"
End Sub
Sub keywords(control As IRibbonControl)
    ApplyStyleLang "TEI_keywords"
End Sub
Sub title_trl(control As IRibbonControl)
    ApplyStyleLang "TEI_title:trl"
End Sub

Sub language(control As IRibbonControl)
    Dim SelParaFirst As Paragraph
    Set SelParaFirst = Selection.paragraphs.First
    Dim languageCode As String
    
    Unload language_selector
    Load language_selector
    language_selector.Show
    result = language_selector.dropdown_language.value
    
    If result <> "" And result <> "-------------" Then
        On Error Resume Next
        languageCode = Split(result, " | ")(0)
        On Error Resume Next
        SelParaFirst.Range.InsertParagraphBefore
        SelParaFirst.Previous.style = ActiveDocument.Styles("TEI_language")
        ActiveDocument.Range(SelParaFirst.Previous.Range.Start, SelParaFirst.Previous.Range.End - 1).Text = languageCode
    Else
        msgbox "A language is required"
    End If
    
End Sub

Sub format_date(style_name)
 ' If the string match a date, format and apply style_name
 
  Dim k As String
  Dim DateValue As Date
  k = paragraphs.Text
  If IsDate(k) Then
    DateValue = CDate(k)
    paragraphs.Text = Format(DateValue, "DD/MM/YYYY") & Chr(13)
    paragraphs.style = ActiveDocument.Styles(style_name)
    msgbox "The date will be formatted as: " & Format(DateValue, "DD/MM/YYYY") & " (dd/mm/yyyy)"
  Else
    msgbox "This is not a date. Please check."
  End If
End Sub

