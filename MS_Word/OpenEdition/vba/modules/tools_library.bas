Attribute VB_Name = "tools_library"
Sub messages_ok(texte_message As String)
    'If messages_actives = True Then
    Call msgbox(texte_message, vbInformation)
    'End If
End Sub

Sub messages_erreur(texte_message As String)
    Call msgbox(texte_message, vbExclamation, "Erreur")
End Sub

Sub refresh()
    nom_macro = "refresh"
    Application.ScreenRefresh
    Repeat 10
End Sub

Sub nettoyage_recherche_remplace()
    nom_macro = "nettoyage_recherche_remplace"
    'Remise a zero des paramtres de recherche et de remplacementt
    Selection.Find.ClearFormatting 'Nettoyage de la zone "rechercher"
    Selection.Find.Replacement.ClearFormatting 'Nettoyage de la zone "remplacer"
    With Selection.Find
        .Text = ""
        .Replacement.Text = "^&"
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
End Sub

Sub desactive_revision()
    nom_macro = "desactive_revision"
    'Macro de d�sactivation du mode r�vision
    If ActiveDocument.TrackRevisions = True Then
       ActiveDocument.TrackRevisions = False
    End If
End Sub

Sub rechercheremplace(textecherche As String, texteremplace As String)
    nom_macro = "rechercheremplace"
    'Macro rechercher/remplacer
    'Utilisation dans d'autres macros, pour r_duire la taille du code :
    With Selection.Find
        .Text = textecherche
        .Replacement.Text = texteremplace
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .Execute Replace:=wdReplaceAll
    End With
End Sub

Sub rechercheremplaceER(textecherche As String, texteremplace As String)
    nom_macro = "rechercheremplaceER"
    'Macro rechercher/remplacer utilisant des expressions r_guliress
    With Selection.Find
        .Text = textecherche
        .Replacement.Text = texteremplace
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .MatchSoundsLike = False
        .MatchWildcards = True
        .Execute Replace:=wdReplaceAll
    End With
End Sub

Sub rechercheremplaceERboucle(textecherche As String, texteremplace As String)
    nom_macro = "rechercheremplaceERboucle"
    'Macro rechercher/remplacer utilisant une boucle et des expressions r�guli�res
    With Selection.Find
        .Text = textecherche
        .Replacement.Text = texteremplace
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
        .MatchSoundsLike = False
        .MatchWildcards = True
        .Execute Replace:=wdReplaceAll
        Do While Selection.Find.Execute
            'recherche jusqu'� ce que plus rien ne soit trouv�
            .Execute Replace:=wdReplaceAll
        Loop
    End With
End Sub

Sub activer_affichage_normal()
    nom_macro = "activer_affichage_normal"
    'Sous-macro permettant d'afficher une page en mode normal
    If ActiveWindow.View.Type = wdNormalView Then           'Si le mode normal est active, alors aucune action n'est necessaire
    Else
        ActiveWindow.ActivePane.View.Type = wdNormalView    'S'il n'est pas activ_, activation.
    End If
End Sub




''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT NETTOYAGE SURLIGNEMENT ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub nettoyage_surlignement()
    nom_macro = "nettoyage_surlignement"
    'Macro supprimant tout surlignage dans un document
    On Error Resume Next
    Call desactive_revision
    Call nettoyage_recherche_remplace
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
        ActiveWindow.Panes(1).Activate
    End If
    Selection.HomeKey Unit:=wdStory
    Selection.Find.Highlight = True
    Selection.Find.Replacement.Highlight = False
    Call rechercheremplace("", "")
    Call nettoyage_recherche_remplace
    
    'nettoyage dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
            Selection.HomeKey Unit:=wdStory
            Selection.Find.Highlight = True
            Selection.Find.Replacement.Highlight = False
            Call rechercheremplace("", "")
            Call nettoyage_recherche_remplace
        End If
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN NETTOYAGE SURLIGNEMENT ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT ESPACES INSECABLES ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub Espace_insecables_simple()
    nom_macro = "Espace_insecables_simple"
    Call Espaces_insecables(var_batch:=False)
End Sub

Sub Espaces_insecables(var_batch As Boolean)
    nom_macro = "Espaces_insecables"
    ' R�tablissement des ins�cables devant les signes de ponctuation double
    Call desactive_revision
    Call nettoyage_recherche_remplace
    Call activer_affichage_normal
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
        ActiveWindow.Panes(1).Activate
    End If
    Selection.HomeKey Unit:=wdStory
    
    Call retablissement_esp_ins
    Call nettoyage_insec_appel

    'R�tablir les espaces ins�cables dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call retablissement_esp_ins
        End If
    End If
    If var_batch = False Then
        Call messages_ok(texte_message:="Les actions suivantes ont �t� effectu�es :" & vbCrLf & vbCrLf & "   * r�tablissement des espaces ins�cables devant les signes de ponctuation double ;" & vbCrLf & "   * suppression des espaces ins�cables dans les passages en anglais ;" & vbCrLf & "   * suppression des espaces doubles.")
    End If
End Sub

Sub retablissement_esp_ins()
    nom_macro = "retablissement_esp_ins"
    'Faire un nettoyage : supprimer tous les ins�cables devant les signes de ponctuation doubles
    Call nettoyage_recherche_remplace
    Call rechercheremplaceER("^s([\?\;\:\!\%\=])", "\1")
    'Macro de r�tablissement des espaces ins�cables
    Selection.HomeKey Unit:=wdStory
    Call desactive_revision
    'Ajout d'espaces ins�cables
    Call nettoyage_recherche_remplace
    Call rechercheremplaceER("([\?\;\:\!\�\%\=])", "^s\1") 'Ajoute une espace ins�cable devant ? ; : ! � % et =
    Call rechercheremplace("�", "�^s")
    Call rechercheremplace("�^s�", "��^s") 'R�pare l'inversion des guillemets qui suivent une apostrophe
    Call rechercheremplace("��", "��^s")   'R�pare l'inversion des guillemets qui suivent une apostrophe
    Call rechercheremplace("n�", "n�^s")
    Call rechercheremplace("op. cit.", "op.^scit.")
    Call rechercheremplace("art. cit.", "art.^scit.")
    Call rechercheremplace("Art. cit.", "Art.^scit.")
    Call rechercheremplace("pp.", "pp.^s")
    Call rechercheremplace("(p.", "(p.^s")
    Call rechercheremplace(" vol.", " vol.^s")
    Call rechercheremplace("vol. ", "vol.^s")
    Call rechercheremplace("fig. ", "fig.^s")
    Call rechercheremplace("ill. ", "ill.^s")
    'Remplacement des espaces normales par des espaces ins�cables dans les nombres � plus de 4 chiffres
    Call rechercheremplaceER("([0-9]) ([0-9])([0-9])([0-9])", "\1^s\2\3\4")
    'Remplacement des "p." et "t." (abbr�viations de "pages" et "tome" : une espace ins�cable doit s�parer le chiffre de l'abbr�viation)
    'lignes sp�cifiques de code en utilisant des expressions r�guli�res :
    '* application de la macro uniquement aux abbr�viations suivies ou pr�c�d�es d'un chiffre, sinon cela pose un probl�me dans le cas o? "p." ou "t." sont des abbr�viations de noms
    '* le mode "utiliser les caract�res g�n�riques" (expressions r�guli�res) permet de conserver la casse des abbr�viations (majuscules ou minuscules)
    Call rechercheremplaceER("([0-9]) p.", "\1^sp.")
    Call rechercheremplaceER("([0-9]) t.", "\1^st.")
    Call rechercheremplaceER(" p. ([0-9])", " p.^s\1")
    Call rechercheremplaceER(" t. ([0-9])", " t.^s\1")
    Call rechercheremplaceER(" t.([0-9])", " t.^s\1")
    Call rechercheremplaceER(" p.([0-9])", " p.^s\1")
    Call rechercheremplaceER(" t.([0-9])", " t.^s\1")
    Call rechercheremplaceER(" p.  ([0-9])", " p.^s\1")
    Call rechercheremplaceER(" t.  ([0-9])", " t.^s\1")
    'Espaces ins�cables pour les signes mon�taires $, �, �, francs (= ChrW(8355) ), pesetas (= ChrW(8359) ), shekel (= ChrW(8362) ), dong (= ChrW(8363) ), livres (= ChrW(8356) )
    'recherche symbole mon�taire apr�s une s�rie de chiffres
    Call rechercheremplaceER("([0-9]) $", "\1^s$")
    Call rechercheremplaceER("([0-9]) �", "\1^s�")
    Call rechercheremplaceER("([0-9]) �", "\1^s�")
    Call rechercheremplaceER("([0-9]) " & ChrW(8355), "\1^s" & ChrW(8355)) 'fr
    Call rechercheremplaceER("([0-9]) " & ChrW(8359), "\1^s" & ChrW(8359)) 'pts
    Call rechercheremplaceER("([0-9]) " & ChrW(8362), "\1^s" & ChrW(8362)) 'shekel
    Call rechercheremplaceER("([0-9]) " & ChrW(8363), "\1^s" & ChrW(8363)) 'dong
    Call rechercheremplaceER("([0-9]) " & ChrW(8356), "\1^s" & ChrW(8356)) 'livres
    Call rechercheremplaceER("([0-9])$", "\1^s$")
    Call rechercheremplaceER("([0-9])�", "\1^s�")
    Call rechercheremplaceER("([0-9])�", "\1^s�")
    Call rechercheremplaceER("([0-9])" & ChrW(8355), "\1^s" & ChrW(8355)) 'fr
    Call rechercheremplaceER("([0-9])" & ChrW(8359), "\1^s" & ChrW(8359)) 'pts
    Call rechercheremplaceER("([0-9])" & ChrW(8362), "\1^s" & ChrW(8362)) 'shekel
    Call rechercheremplaceER("([0-9])" & ChrW(8363), "\1^s" & ChrW(8363)) 'dong
    Call rechercheremplaceER("([0-9])" & ChrW(8356), "\1^s" & ChrW(8356)) 'livres
    'recherche symbole mon�taire avant une s�rie de chiffres
    Call rechercheremplaceER("$ ([0-9])", "$^s\1")
    Call rechercheremplaceER("� ([0-9])", "�^s\1")
    Call rechercheremplaceER("� ([0-9])", "�^s\1")
    Call rechercheremplaceER(ChrW(8355) & " ([0-9])", ChrW(8355) & "^s\1") 'fr
    Call rechercheremplaceER(ChrW(8359) & " ([0-9])", ChrW(8359) & "^s\1") 'pts
    Call rechercheremplaceER(ChrW(8362) & " ([0-9])", ChrW(8362) & "^s\1") 'shekel
    Call rechercheremplaceER(ChrW(8363) & " ([0-9])", ChrW(8363) & "^s\1") 'dong
    Call rechercheremplaceER(ChrW(8356) & " ([0-9])", ChrW(8356) & "^s\1") 'livres
    Call rechercheremplaceER("$([0-9])", "$^s\1")
    Call rechercheremplaceER("�([0-9])", "�^s\1")
    Call rechercheremplaceER("�([0-9])", "�^s\1")
    Call rechercheremplaceER(ChrW(8355) & "([0-9])", ChrW(8355) & "^s\1") 'fr
    Call rechercheremplaceER(ChrW(8359) & "([0-9])", ChrW(8359) & "^s\1") 'pts
    Call rechercheremplaceER(ChrW(8362) & "([0-9])", ChrW(8362) & "^s\1") 'shekel
    Call rechercheremplaceER(ChrW(8363) & "([0-9])", ChrW(8363) & "^s\1") 'dong
    Call rechercheremplaceER(ChrW(8356) & "([0-9])", ChrW(8356) & "^s\1") 'livres
    'Remplacement des espaces doubles
    Call rechercheremplaceER("  ([\?\;\:\!\�\%\=])", "^s\1")
    Call rechercheremplace("^s ", "^s")
    Call rechercheremplace(" ^s", "^s")
    Call rechercheremplace("  ", " ")
    Call rechercheremplace("n�  ", "n�^s")
    Call rechercheremplace("pp.  ", "pp.^s")
    Call rechercheremplace(" pp.  ", " pp.^s")
    Call rechercheremplace("^s^s", "^s")
    Call nettoyage_recherche_remplace
    
    Call rechercheremplace("^s^s", "^s")
    Call rechercheremplace("^s://", "://") 'http :// https ://, etc.
    Call espaces_insecables_english
     
End Sub


Sub espaces_insecables_english() '
    nom_macro = "espaces_insecables_english"
    'R�tablir les espaces ins�cables en anglais
    Call desactive_revision
    Call nettoyage_recherche_remplace
    Selection.Find.LanguageID = wdEnglishUK
    Call rechercheremplaceERboucle("^s([\?\;\:\!\%\=])", "\1")
    Call nettoyage_recherche_remplace
    Selection.Find.LanguageID = wdEnglishUS
    Call rechercheremplaceERboucle("^s([\?\;\:\!\%\=])", "\1")
    Call nettoyage_recherche_remplace
End Sub

Sub nettoyage_insec_appel()
    nom_macro = "nettoyage_insec_appel"
    Selection.HomeKey Unit:=wdStory 'Placement du curseur au d_but du texte
    Call rechercheremplaceER(textecherche:=" (^2)", texteremplace:="\1")
    Call nettoyage_recherche_remplace
    Call rechercheremplaceER(textecherche:="^s(^2)", texteremplace:="\1")
    Call nettoyage_recherche_remplace
    Selection.HomeKey Unit:=wdStory
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN ESPACES INSECABLES ''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT APOSTROPHES '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Sub apostrophe()
    nom_macro = "apostrophe"
    'Macro de remplacement des apostrophes verticales par des apostrophes typographiques
    'Utilis�e comme sous-macro des macros apostrophe_cdt (correction dans le corps de texte) et apostrophe_notes (dans les notes)
     Call rechercheremplaceER("'", "�")
     Call rechercheremplace("�^s�", "��^s") 'R�pare l'inversion des guillemets qui suivent une apostrophe
     Call rechercheremplace("��", "��^s")   'R�pare l'inversion des guillemets qui suivent une apostrophe
     Call nettoyage_recherche_remplace
End Sub

Sub apostrophe_cdt(var_batch As Boolean)
    nom_macro = "apostrophe_cdt"
    Call desactive_revision
    Call nettoyage_recherche_remplace
    Call activer_affichage_normal
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
        ActiveWindow.Panes(1).Activate
    End If
    Selection.HomeKey Unit:=wdStory
    Call apostrophe
    
    'Corriger les apostrophes dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call apostrophe
        End If
    End If
    
    If var_batch = False Then
        Call messages_ok("Les apostrophes verticales ont �t� remplac�es par des apostrophes typographiques dans le corps de texte et dans les notes.")
    End If
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN APOSTROPHES '''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT DOUBLES SAUTS PARAGRAPHES '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub recherche_remplace_double_paragraphe()
    nom_macro = "recherche_remplace_double_paragraphe"
    Call desactive_revision
    
    Selection.HomeKey Unit:=wdStory
    Call rechercheremplaceER("[\^013]{2;}", "^p")
    Selection.EndKey Unit:=wdStory
    Selection.Delete Unit:=wdCharacter, Count:=1
    Selection.HomeKey Unit:=wdStory
    Call rechercheremplaceER("[\^013]{2;}", "^p")
    Call nettoyage_recherche_remplace

End Sub


Sub double_saut_paragraphe()
    nom_macro = "double_saut_paragraphe"
    ' Supprimer les doubles sauts de paragraphe
    Call desactive_revision
    Call nettoyage_recherche_remplace
    If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then    'V�rifie si les notes de bas de page sont affich�es
        ActiveWindow.Panes(1).Activate                          'Si c'est le cas, placement du curseur dans le corps de texte, pour empcher l'application de la macro aux notes (cela provoque sinon un bug))
    End If
    Selection.HomeKey Unit:=wdStory                             'Placement du curseur au debut du texte
    Call recherche_remplace_double_paragraphe                   'Appel de la sous-macro de remplacement des doubles paragraphes
    Call nettoyage_recherche_remplace
    
     'R�tablir les espaces ins�cables dans les notes
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Selection.Find.ClearFormatting
            With Selection.Find
                .Text = "^p^p"
                .Replacement.Text = "^&"
                .Forward = True
                .Wrap = wdFindContinue
                .Format = False
                .MatchCase = False
                .MatchWholeWord = False
                .MatchWildcards = False
                .MatchSoundsLike = False
                .MatchAllWordForms = False
                'boucle de recherche et de suppression du saut de paragraphe en trop
                Do While Selection.Find.Execute
                    Selection.MoveLeft Unit:=wdCharacter, Count:=1
                    Selection.Delete Unit:=wdCharacter, Count:=1
                Loop
            End With
        End If
    End If
    
End Sub

Sub double_paragraphe_ndbp_simple()
    nom_macro = "double_paragraphe_ndbp_simple"
    Call double_paragraphe_ndbp(var_batch:=False)
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN DOUBLES SAUTS PARAGRAPHES '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''




''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT GUILLEMETS ''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub Guillemets_anglais_simple()
    nom_macro = "Guillemets_anglais_simple"
    Call Guillemets_anglais(var_batch:=False)
End Sub

Sub Guillemets_anglais(var_batch As Boolean)
    nom_macro = "Gu" 'llemets_anglais"
    'Remplacement des guillemets verticaux en guillemets anglais
    'If Application.Version > "12" Then
    With Options
        .AutoFormatAsYouTypeReplaceQuotes = False
    End With
    'End If
    Selection.HomeKey Unit:=wdStory 'Curseur en d�but de document
    Call nettoyage_recherche_remplace 'Remise � z�ro des param�tres de recherche
    Options.DefaultHighlightColorIndex = wdYellow 'Couleur de surlignage en jaune
    Selection.Find.Replacement.Highlight = True
    Call rechercheremplaceER("""(*)""", "�\1�")
    Selection.Range.HighlightColorIndex = wdYellow
    
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call nettoyage_recherche_remplace 'Remise � z�ro des param�tres de recherche
           Options.DefaultHighlightColorIndex = wdYellow 'Couleur de surlignage en jaune
           Selection.Find.Replacement.Highlight = True
           Call rechercheremplaceER("""(*)""", "�\1�")
           Selection.Range.HighlightColorIndex = wdYellow
        End If
    End If
    
    If var_batch = False Then
        Call messages_ok("Les guillemets verticaux ont �t� remplac�s par des guillemets anglais. Les passages modifi�s ont �t� surlign�s en jaune.")
    End If
    'Call nettoyage_recherche_remplace
End Sub

Sub Guillemets_francais_simple()
    nom_macro = "Guillemets_francais_simple"
    Call Guillemets_francais(var_batch:=False)
End Sub

Sub Guillemets_francais(var_batch As Boolean)
    nom_macro = "Guillemets_francais"
    Selection.HomeKey Unit:=wdStory
    'If Application.Version > "12" Then
    With Options
        .AutoFormatAsYouTypeReplaceQuotes = False
    End With
    'End If
    Call nettoyage_recherche_remplace
    Options.DefaultHighlightColorIndex = wdYellow
    Selection.Find.Replacement.Highlight = True
    Call rechercheremplaceER("""(*)""", "�^s\1^s�")
    Selection.Range.HighlightColorIndex = wdYellow
    
    If ActiveDocument.Footnotes.Count >= 1 Then
        If ActiveWindow.View.SplitSpecial <> wdPaneFootnotes Then
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes
        End If
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then
           ActiveWindow.Panes(2).Activate
           'Une fois dans les notes, mettre le curseur au d�but des notes
           Selection.HomeKey Unit:=wdStory
           Call nettoyage_recherche_remplace
           Options.DefaultHighlightColorIndex = wdYellow
           Selection.Find.Replacement.Highlight = True
           Call rechercheremplaceER("""(*)""", "�^s\1^s�")
           Selection.Range.HighlightColorIndex = wdYellow
        End If
    End If
    
    If var_batch = False Then
        Call messages_ok("Les guillemets verticaux ont �t� remplac�s par des guillemets francais. Les passages modifi�s ont �t� surlign�s en jaune.")
    End If
    Call nettoyage_recherche_remplace
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN GUILLEMETS ''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT VERIFICATION PARAGRAPHES ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub verification_paragraphe() '(var_batch As Boolean)'
    nom_macro = "verification_paragraphe"
    'Macro de verification des paragraphes
    'Surligne et met en rouge les paragraphes atypiques (ne commenant pas par une majuscule ou ne se terminant pas par un signe de ponctuation))
    'Reglage de la couleur de surlignage sur jaune
    Options.DefaultHighlightColorIndex = wdYellow
    'remise � z_ro des paramtres de recherche et de remplacementt
    Call nettoyage_recherche_remplace
    'Acc_l_ration de l'ex_cution de la macro, en n'affichant pas les modifications au fur et � mesure qu'elles sont effectu_es, mais seulement une fois la macro termin_e
    Word.Application.ScreenUpdating = False
    'Appel de la macro de d_sactivation du mode r_vision (s'il est activ_, la macro ne peut pas fonctionner correctement)
    Call desactive_revision
    'Mise en avant des passages ne se terminant pas par un point
        'surlignage
            Selection.Find.Replacement.Highlight = True
        'couleur de police rouge
            Selection.Find.Replacement.Font.Color = wdColorRed
        'recherche des passages ne se terminant pas par un point
            Call rechercheremplaceER("[ a-zA-Z0-9,;][\^013]", "^&")
    'Supprimer la mise en avant des passages pour lesquels l'absence de point final est normale
        Call verif_exclusion_styles_point
    'Mise en avant des passages commenant par une minusculee
    Call nettoyage_recherche_remplace
    Selection.Find.Replacement.Highlight = True
    Selection.Find.Replacement.Font.Color = wdColorRed
    Call rechercheremplaceER("[\^013][a-z]", "^&")
    Call nettoyage_recherche_remplace
    'Supprimer la mise en avant des passages pour lesquels le commencement par une minuscule est normal
    Call verif_exclusion_styles_minuscule
    'Supprimer le rouge des marques de paragraphe
    Selection.Find.Font.Color = wdColorRed
    Selection.Find.Replacement.style = ActiveDocument.Styles(wdStyleDefaultParagraphFont)
    With Selection.Find
        .Text = "^p"
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    'Boite de dialogue indiquant que la macro s'est appliqu_e correctement
    If var_batch = False Then
        Call messages_ok("Les d�buts de paragraphes commenant par des minuscules et les fins de paragraphes ne se terminant pas par de la ponctuation ont �t� surlign�s en jaune.")
    End If
End Sub

Sub verif_exclusion_styles_point()
    nom_macro = "verif_exclusion_styles_point"
    'Supprimer la mise en avant des passages pour lesquels l'absence de point final est normale
    'Intertitres
    Call verif_exclusion_style(style:="Titre 1", couleurpolice:=wdColorAutomatic) 'wdStyleHeading1
    Call verif_exclusion_style(style:="Titre 2", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 3", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 4", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 5", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 6", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 7", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 8", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Titre 9", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Separateur", couleurpolice:=wdColorAutomatic)
    'Certaines m_tadonn_es
    Call verif_exclusion_style(style:="Titre", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Auteur", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Mots Cles", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Keywords", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Palabrasclaves", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Schlagworter", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Themes", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Periode", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Geographie", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Notice Biblio", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Langue", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Droits Auteur", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Datepublipapier", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Datepubli", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Surtitre", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Sous-titre", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Title (en)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titulo (es)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titolo (it)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titel (de)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titre (fr)", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Traducteur", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Editeur scientifique", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Pagination", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Num_ro du document", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Titre oeuvre", couleurpolice:=wdColorViolet)
    Call verif_exclusion_style(style:="Auteur oeuvre", couleurpolice:=wdColorViolet)
    Call verif_exclusion_style(style:="Notice Biblio oeuvre", couleurpolice:=wdColorViolet)
    Call verif_exclusion_style(style:="Date publi oeuvre", couleurpolice:=wdColorViolet)
    'certains contenus dans le corps de texte
    Call verif_exclusion_style(style:="Titre Illustration", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Legende Illustration", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Cr_dits Illustration", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Bibliographie", couleurpolice:=wdColorAutomatic)
    Call verif_exclusion_style(style:="Code", couleurpolice:=wdColorAutomatic)
End Sub

Sub verif_exclusion_styles_minuscule()
    nom_macro = "verif_exclusion_styles_minuscule"
    'Supprimer la mise en avant des passages pour lesquels le commencement par une minuscule est normal
    Call verif_exclusion_style(style:="Langue", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Mots Cles", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Keywords", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Palabrasclaves", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Schlagworter", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Themes", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Periode", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Geographie", couleurpolice:=wdColorDarkBlue)
    Call verif_exclusion_style(style:="Code", couleurpolice:=wdColorAutomatic)
End Sub

Sub verif_exclusion_style(style As String, Optional couleurpolice As WdColor)
    nom_macro = "verif_exclusion_style"
    'Sous-macro permettant d'exclure le style d�fini en variable de l'application de la macro verification_paragraphe
    Call nettoyage_recherche_remplace
    On Error GoTo fin
    Selection.Find.style = ActiveDocument.Styles(style)
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Replacement.Font.Color = couleurpolice
    With Selection.Find
        .Highlight = True
        .Font.Color = wdColorRed
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Replacement.Highlight = True
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    Selection.Find.style = ActiveDocument.Styles(style)
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Replacement.Font.Color = couleurpolice
    With Selection.Find
        .Highlight = True
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
    Selection.Find.style = ActiveDocument.Styles(style)
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Replacement.Font.Color = couleurpolice
    With Selection.Find
        .Font.Color = wdColorRed
        .Text = ""
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
fin:
End Sub

Sub fin_verif_paragraphes()
    nom_macro = "fin_verif_paragraphes"
    'Macro permettant de desurligner et de supprimer le rouge ajoute lors de l'application de la macro de verification des paragraphes
    Call desactive_revision
    Call nettoyage_recherche_remplace
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="Description Auteur")
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="R_sum_")
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="Abstract")
    On Error Resume Next
    Call fin_verif_para_style_meta(style:="Resumen")
    'To do : autre type de m_ta
    Selection.HomeKey Unit:=wdStory
    Selection.Find.Highlight = True
    Selection.Find.Font.Color = wdColorRed
    Selection.Find.Replacement.Font.Color = wdColorAutomatic
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
End Sub

Sub fin_verif_para_style_meta(style As String)
    nom_macro = "fin_verif_para_style_meta"
    Selection.HomeKey Unit:=wdStory
    Selection.Find.Highlight = True
    Selection.Find.style = style
    Selection.Find.Font.Color = wdColorRed
    Selection.Find.Replacement.Font.Color = wdColorDarkBlue
    Selection.Find.Replacement.Highlight = False
    Selection.Find.Execute Replace:=wdReplaceAll
    Call nettoyage_recherche_remplace
End Sub

Sub activation_surlignement()
    nom_macro = "activation_surlignement"
    If Options.DefaultHighlightColorIndex = 0 Then
        Options.DefaultHighlightColorIndex = wdYellow
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN VERIFICATION PARAGRAPHES ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''



''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' DEBUT NOTES '''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub renumerotation_notes()
    nom_macro = "renumerotation_notes"
    'Macro destin�e a renum�roter correctement les appels de notes dans le cas ou ceux-ci sont tous a zero
    '(problme frequent lors du passage d'un document de Mac vers PC.))
    Call desactive_revision
    Call activer_cdt
    Call nettoyage_recherche_remplace
    
    Selection.HomeKey Unit:=wdStory
    With Selection.Find
        .Text = "^f" 'rechercher le premier appel de note dans le document
        .MatchWildcards = False
    End With
    Selection.Find.Execute
    
    'Si un appel de note est trouv� :
    If Selection.Find.Found Then
        With Selection.FootnoteOptions  'Puis r_gler les options de notes de bas de page :
            .Location = wdBottomOfPage
            .NumberingRule = wdRestartContinuous    '1) d_finir une num_rotation continue ;
            .StartingNumber = 1                     '2) commencant a 1 ;;
            .NumberStyle = wdNoteNumberStyleArabic  '3) en chiffres arabes.
        End With
    End If
End Sub


Sub nettoyage_notes_points()
    nom_macro = "nettoyage_notes_points"
    'Macro supprimant dans les notes les points aprs les num_ros de notee
    If ActiveDocument.Footnotes.Count > 0 Then
        Call activer_notes
        Selection.HomeKey Unit:=wdStory
        Call nettoyage_recherche_remplace
        Call rechercheremplaceER(textecherche:="(^2)\.", texteremplace:="\1")
        Call nettoyage_recherche_remplace
        Selection.HomeKey Unit:=wdStory
        Call messages_ok(texte_message:="Les points ont �t� supprim�s apres les num�ros de notes.")
        
    ElseIf ActiveDocument.Footnotes.Count = 0 Then
        Call messages_erreur("Erreur : aucune note de bas de page n'est pr_sente dans ce document.")
    End If
End Sub

Sub activer_notes()
    nom_macro = "activer_notes"
     'Macro plaant le curseur dans les notess
    If ActiveDocument.Footnotes.Count = 0 Then Exit Sub 'Si pas de notes dans le document, arrter cette macro....
        Call activer_affichage_normal
        If ActiveWindow.View.SplitSpecial = wdPaneFootnotes Then    'v_rifier que les notes sont activ_es
            ActiveWindow.Panes(2).Activate      'Placement du curseur dans les notes : utile si les notes sont affich_es mais que le curseur est plac_ dans le corps du document
            Selection.HomeKey Unit:=wdStory     'Placement du curseur au d_but des notes
        Else
            'On error resume next
            ActiveWindow.View.SplitSpecial = wdPaneFootnotes            'Si ce n'est pas le cas, les activer
        End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''' FIN NOTES '''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''


