Attribute VB_Name = "apply_figure"
Sub figure_title(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_title")
End Sub
Sub figure_caption(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_caption")
End Sub
Sub figure_alttext(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_alttext")
End Sub
Sub figure_credits(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_credits")
End Sub
Sub image_alternative(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_figure_alternative")
End Sub
Sub formula(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_formula")
End Sub
Sub formula_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_formula-inline")
End Sub
Sub figure_section(control As IRibbonControl)
    Call section("TEI_figure_start", "##### FIGURE START #####", "TEI_figure_end", "##### FIGURE END #####")
End Sub

Sub InsertAltTextCaptions(control As IRibbonControl)
    Dim AltTextCaption As String
    Dim ImageRange As Range
    Dim AltTextParagraph As Paragraph
    
    Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst
    With ActiveDocument
        For i = 1 To .InlineShapes.Count
            AltTextCaption = .InlineShapes(i).AlternativeText
            AltTextCaption = Replace(AltTextCaption, ChrW$(10), " ")
            If AltTextCaption <> "" Then
                Set ImageRange = .InlineShapes(i).Range
                ImageRange.Collapse Direction:=wdCollapseEnd
                ImageRange.InsertAfter vbCr
                ImageRange.InsertAfter AltTextCaption
                Set AltTextParagraph = ImageRange.paragraphs.Last
                AltTextParagraph.style = ActiveDocument.Styles("TEI_figure_alttext")
            End If
        Next
    End With
End Sub

