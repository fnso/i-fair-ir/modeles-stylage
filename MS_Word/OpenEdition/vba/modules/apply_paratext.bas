Attribute VB_Name = "apply_paratext"
Sub erratum(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_erratum")
End Sub
Sub acknowledgment(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_acknowledgment")
End Sub
Sub dedication(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_dedication")
End Sub
Sub epigraph(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_epigraph")
End Sub
Sub paragraph_lead(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_paragraph_lead")
End Sub
Sub note_aut(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_note:aut")
End Sub
Sub note_pbl(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_note:pbl")
End Sub
Sub note_trl(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_note:trl")
End Sub

Sub foot_note(control As IRibbonControl)
    Call foot_note_cmd
End Sub
Sub foot_note_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleFootnoteText)
End Sub
Sub end_note(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles(wdStyleEndnoteText)
End Sub

Sub bibl_start(control As IRibbonControl)
    Dim SelParaFirst As Paragraph
    Dim PrevRange As Range
    
    Dim oTable As table
    Dim oRng As Range
    ' La s�lection est dans un tableau
    If Selection.Information(wdWithInTable) = True Then
        Set oTable = Selection.Tables(1)
        
        Set oRng = oTable.Range
        oRng.Select
        Selection.SplitTable
        Set PrevRange = oRng.paragraphs.First.Range
        
    ' La s�lection n'est pas dans un tableau
    Else
        Selection.Expand wdParagraph
        Set SelParaFirst = Selection.paragraphs.First
        SelParaFirst.Range.InsertParagraphBefore
        Set PrevRange = SelParaFirst.Previous.Range
    End If

    PrevRange.style = ActiveDocument.Styles("TEI_bibl_start")
    ActiveDocument.Range(PrevRange.Start, PrevRange.End - 1).Text = "##### BIBLIOGRAPHY START #####"
    
    Set oTable = Nothing
    Set oRng = Nothing
    Set SelParaFirst = Nothing
    Set PrevRange = Nothing
End Sub

Sub appendix_start(control As IRibbonControl)
    Dim SelParaFirst As Paragraph
    Dim PrevRange As Range
    
    Dim oTable As table
    Dim oRng As Range
    ' La s�lection est dans un tableau
    If Selection.Information(wdWithInTable) = True Then
        Set oTable = Selection.Tables(1)
        
        Set oRng = oTable.Range
        oRng.Select
        Selection.SplitTable
        Set PrevRange = oRng.paragraphs.First.Range
        
    ' La s�lection n'est pas dans un tableau
    Else
        Selection.Expand wdParagraph
        Set SelParaFirst = Selection.paragraphs.First
        SelParaFirst.Range.InsertParagraphBefore
        Set PrevRange = SelParaFirst.Previous.Range
    End If

    PrevRange.style = ActiveDocument.Styles("TEI_appendix_start")
    ActiveDocument.Range(PrevRange.Start, PrevRange.End - 1).Text = "##### APPENDIX START #####"
    
    Set oTable = Nothing
    Set oRng = Nothing
    Set SelParaFirst = Nothing
    Set PrevRange = Nothing
End Sub
