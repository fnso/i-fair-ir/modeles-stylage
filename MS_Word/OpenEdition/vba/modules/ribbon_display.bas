Attribute VB_Name = "ribbon_display"
Public myRibbon As IRibbonUI
Public standard As String
Public display_archeology As String
Public display_poetry_theater As String
Public display_review As String
Public display_optional_keywords As String
Public display_citation As String
Public display_linguistic As String
Public display_all As String
Public apply_style_to_selection As String

Dim ReturnedVal As Boolean
Sub OnLoad(ribbon As IRibbonUI)
    Set myRibbon = ribbon
    standard = True
    apply_style_to_selection = False
End Sub
Sub RefreshRibbon()
    Dim Msg As String
    
    Select Case Application.International(wdProductLanguageID)
        Case 1036, 2060, 3084 'fr
            Msg = "Perte du lien avec le ruban, veuillez red�marrer l�application."
            ErrorMsg = "Erreur"
        Case Else 'autre lang = en
            Msg = "Error on Ribbon loading. Please restart Word"
            ErrorMsg = "Error"
        End Select
        
        On Error GoTo ErrorMsg
        myRibbon.Invalidate
        'On Error GoTo 0
        Exit Sub
        
ErrorMsg:
        msgbox ErrorMsg & " " & Err.Number & " (" & Err.Description & ")", _
        vbCritical, Msg
End Sub
Sub get_visible_archeology(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = display_archeology
End Sub
Sub get_visible_poetry_theater(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = display_poetry_theater
End Sub
Sub get_visible_review(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = display_review
End Sub
Sub get_visible_optional_keywords(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = display_optional_keywords
End Sub
Sub get_visible_citation(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = display_citation
End Sub
Sub get_visible_linguistic(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = display_linguistic
End Sub
Sub get_visible_all(control As IRibbonControl, ByRef ReturnedVal)
    ReturnedVal = display_all
End Sub
Sub GetPressed(control As IRibbonControl, ByRef ReturnedVal)
    Select Case control.ID
        Case "display_archeology"
            ReturnedVal = display_archeology
        Case "display_poetry_theater"
            ReturnedVal = display_poetry_theater
        Case "display_review"
            ReturnedVal = display_review
        Case "display_optional_keywords"
            ReturnedVal = display_optional_keywords
        Case "display_citation"
            ReturnedVal = display_citation
        Case "display_linguistic"
            ReturnedVal = display_linguistic
        Case "display_all"
            ReturnedVal = display_all
    End Select
End Sub

Sub ToggleVisibility(control As IRibbonControl, pressed As Boolean)
    Select Case control.ID
        Case "display_archeology"
            If pressed = True Then
                display_archeology = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                display_archeology = False
                Call RefreshRibbon
            End If
        Case "display_poetry_theater"
            If pressed = True Then
                display_poetry_theater = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                display_poetry_theater = False
                Call RefreshRibbon
            End If
        Case "display_review"
            If pressed = True Then
                display_review = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                display_review = False
                Call RefreshRibbon
            End If
         Case "display_optional_keywords"
            If pressed = True Then
                display_optional_keywords = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                display_optional_keywords = False
                Call RefreshRibbon
            End If
         Case "display_citation"
            If pressed = True Then
                display_citation = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                display_citation = False
                Call RefreshRibbon
            End If
        Case "display_linguistic"
            If pressed = True Then
                display_linguistic = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                display_linguistic = False
                Call RefreshRibbon
            End If
        Case "display_all"
            If pressed = True Then
                display_archeology = True
                display_poetry_theater = True
                display_review = True
                display_optional_keywords = True
                display_citation = True
                display_linguistic = True
                display_all = True
                Call RefreshRibbon
            End If
            If pressed = False Then
                display_archeology = False
                display_poetry_theater = False
                display_review = False
                display_optional_keywords = False
                display_citation = False
                display_linguistic = False
                display_all = False
                Call RefreshRibbon
            End If
    End Select
End Sub

' apply_style_to_selection est utilis� dans init_and_commons -> Function paragraphs
Sub GetPressedStyleMode(control As IRibbonControl, ByRef ReturnedVal)
    Select Case control.ID
        Case "apply_style_to_selection"
            ReturnedVal = apply_style_to_selection
    End Select
End Sub
Sub ApplyStyleMode(control As IRibbonControl, pressed As Boolean)
    Select Case control.ID
         Case "apply_style_to_selection"
            If pressed = True Then
                apply_style_to_selection = True
            End If
            If pressed = False Then
                apply_style_to_selection = False
            End If
    End Select
End Sub
