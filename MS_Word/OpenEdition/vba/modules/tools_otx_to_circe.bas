Attribute VB_Name = "tools_otx_to_circe"
Public Deleted As String
Public Todo As String
    
Sub otx2circe_styles(control As IRibbonControl)
    
    Deleted = ""
    Todo = ""
    Selection.GoTo What:=wdGoToLine, Which:=wdGoToAbsolute, Count:=1
    
    Call search_replace_style(search_style:="Sous-titre", replace_style:="TEI_title:sub")
    Call search_replace_style(search_style:="Auteur", replace_style:="TEI_author:aut")
    Call search_replace_style(search_style:="Traducteur", replace_style:="TEI_editor:trl")
    Call search_replace_style(search_style:="Editeur scientifique", replace_style:="TEI_editor:edt")
    Call search_replace_style(search_style:="EditeurScientifique", replace_style:="TEI_editor:edt")
    Call search_replace_style(search_style:="Description Auteur", replace_style:="TEI_authority_biography")
    Call search_replace_style(search_style:="DescriptionAuteur", replace_style:="TEI_authority_biography")
    Call search_replace_style(search_style:="Langue", replace_style:="TEI_language")
    Call search_replace_style(search_style:="Notice Biblio oeuvre", replace_style:="TEI_reviewed_reference")
    Call search_replace_style(search_style:="NoticeBiblioOeuvre", replace_style:="TEI_reviewed_reference")
    Call search_replace_style(search_style:="Themes", replace_style:="TEI_keywords_subjects:subject")
    Call search_replace_style(search_style:="Periode", replace_style:="TEI_keywords_subjects:chronology")
    Call search_replace_style(search_style:="Geographie", replace_style:="TEI_keywords_subjects:geography")
    Call search_replace_style(search_style:="Pagination", replace_style:="TEI_pagination")
    Call search_replace_style(search_style:="Separateur", replace_style:="TEI_paragraph_break")
    Call search_replace_style(search_style:="ParagrapheSansRetrait", replace_style:="TEI_paragraph_consecutive")
    Call search_replace_style(search_style:="Paragraphe sans retrait", replace_style:="TEI_paragraph_consecutive")
    Call search_replace_style(search_style:="Titreillustration", replace_style:="TEI_figure_title")
    Call search_replace_style(search_style:="Titre Illustration", replace_style:="TEI_figure_title")
    Call search_replace_style(search_style:="Legendeillustration", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="Legende Illustration", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="L�gende Illustration", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="Creditsillustration", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="Credits Illustration", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="Cr�dits Illustration", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="Citation", replace_style:="TEI_quote")
    Call search_replace_style(search_style:="Grille couleur - Accent 1", replace_style:="TEI_quote")  ' from MacOS
    Call search_replace_style(search_style:="CitationBis", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Citation bis", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="CitationTer", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Citation ter", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Question", replace_style:="TEI_question")
    Call search_replace_style(search_style:="Reponse", replace_style:="TEI_answer")
    Call search_replace_style(search_style:="Code", replace_style:="TEI_code")
    Call search_replace_style(search_style:="Remerciements", replace_style:="TEI_acknowledgment")
    Call search_replace_style(search_style:="Dedicace", replace_style:="TEI_dedication")
    Call search_replace_style(search_style:="Epigraphe", replace_style:="TEI_epigraph")
    Call search_replace_style(search_style:="Erratum", replace_style:="TEI_erratum")
    Call search_replace_style(search_style:="NDLA", replace_style:="TEI_note:aut")
    Call search_replace_style(search_style:="NDLR", replace_style:="TEI_note:pbl")
    Call search_replace_style(search_style:="Surtitre", replace_style:="TEI_title:sup")
    Call search_replace_style(search_style:="NumeroDuDocument", replace_style:="TEI_document_number")
    Call search_replace_style(search_style:="Num�ro Du Document", replace_style:="TEI_document_number")
    Call search_replace_style(search_style:="directeurfouilles", replace_style:="TEI_editor:fld")
    Call search_replace_style(search_style:="Collaborateur", replace_style:="TEI_editor:ctb")
    Call search_replace_style(search_style:="PersonnesCitees", replace_style:="TEI_keywords_subjects:personcited")
    Call search_replace_style(search_style:="personscited", replace_style:="TEI_keywords_subjects:personcited")
    Call search_replace_style(search_style:="personnescitees", replace_style:="TEI_keywords_subjects:personcited")
    Call search_replace_style(search_style:="Oeuvrecitee", replace_style:="TEI_keywords_subjects:work")
    Call search_replace_style(search_style:="oeuvretraite", replace_style:="TEI_keywords_subjects:work")
    Call search_replace_style(search_style:="oeuvretraitee", replace_style:="TEI_keywords_subjects:work")
    Call search_replace_style(search_style:="oeuvrecitee", replace_style:="TEI_keywords_subjects:work")
    Call search_replace_style(search_style:="nomsmotscles", replace_style:="TEI_keywords_subjects:propernoun")
    Call search_replace_style(search_style:="anneeoperation", replace_style:="TEI_keywords_subjects:excavationyear")
    
    Call search_replace_char_style(search_style:="mathLatex", replace_style:="TEI_formula-inline")
    Call search_replace_char_style(search_style:="mathlatex", replace_style:="TEI_formula-inline")
    Call search_replace_style(search_style:="mathlatex", replace_style:="TEI_formula")
    Call search_replace_style(search_style:="mathLatex", replace_style:="TEI_formula")

    Call search_replace_dyn_style(search_style:="Titre (fr)", replace_root_style:="TEI_title:trl", lang:="fr")
    Call search_replace_dyn_style(search_style:="Title (en)", replace_root_style:="TEI_title:trl", lang:="en")
    Call search_replace_dyn_style(search_style:="Titulo (es)", replace_root_style:="TEI_title:trl", lang:="es")
    Call search_replace_dyn_style(search_style:="Titolo (it)", replace_root_style:="TEI_title:trl", lang:="it")
    Call search_replace_dyn_style(search_style:="Titel (de)", replace_root_style:="TEI_title:trl", lang:="de")
    Call search_replace_dyn_style(search_style:="Titulopt", replace_root_style:="TEI_title:trl", lang:="pt")
    Call search_replace_dyn_style(search_style:="titrepl", replace_root_style:="TEI_title:trl", lang:="pl")
    Call search_replace_dyn_style(search_style:="Titrear", replace_root_style:="TEI_title:trl", lang:="ar")
    Call search_replace_dyn_style(search_style:="titreca", replace_root_style:="TEI_title:trl", lang:="ca")
    Call search_replace_dyn_style(search_style:="titrezh", replace_root_style:="TEI_title:trl", lang:="zh")
    Call search_replace_dyn_style(search_style:="Titulueu", replace_root_style:="TEI_title:trl", lang:="eu")
    Call search_replace_dyn_style(search_style:="Titrehe", replace_root_style:="TEI_title:trl", lang:="he")
    Call search_replace_dyn_style(search_style:="Cimhu", replace_root_style:="TEI_title:trl", lang:="hu")
    Call search_replace_dyn_style(search_style:="Titulusla", replace_root_style:="TEI_title:trl", lang:="la")
    Call search_replace_dyn_style(search_style:="titretraduitmg", replace_root_style:="TEI_title:trl", lang:="mg")
    Call search_replace_dyn_style(search_style:="Titelnl", replace_root_style:="TEI_title:trl", lang:="nl")
    Call search_replace_dyn_style(search_style:="titretraduitru", replace_root_style:="TEI_title:trl", lang:="ru")
    Call search_replace_dyn_style(search_style:="titreja", replace_root_style:="TEI_title:trl", lang:="ja")

    Call search_replace_dyn_style(search_style:="Resume", replace_root_style:="TEI_abstract", lang:="fr")
    Call search_replace_dyn_style(search_style:="Abstract", replace_root_style:="TEI_abstract", lang:="en")
    Call search_replace_dyn_style(search_style:="Resumen", replace_root_style:="TEI_abstract", lang:="es")
    Call search_replace_dyn_style(search_style:="Riassunto", replace_root_style:="TEI_abstract", lang:="fr")
    Call search_replace_dyn_style(search_style:="Zusammenfassung", replace_root_style:="TEI_abstract", lang:="de")
    Call search_replace_dyn_style(search_style:="Resumo", replace_root_style:="TEI_abstract", lang:="pt")
    Call search_replace_dyn_style(search_style:="resumepl", replace_root_style:="TEI_abstract", lang:="pl")
    Call search_replace_dyn_style(search_style:="Resumear", replace_root_style:="TEI_abstract", lang:="ar")
    Call search_replace_dyn_style(search_style:="resumeca", replace_root_style:="TEI_abstract", lang:="ca")
    Call search_replace_dyn_style(search_style:="Resumeel", replace_root_style:="TEI_abstract", lang:="el")
    Call search_replace_dyn_style(search_style:="resumezh", replace_root_style:="TEI_abstract", lang:="zh")
    Call search_replace_dyn_style(search_style:="Resumeeu", replace_root_style:="TEI_abstract", lang:="eu")
    Call search_replace_dyn_style(search_style:="Resumehe", replace_root_style:="TEI_abstract", lang:="he")
    Call search_replace_dyn_style(search_style:="Rezume", replace_root_style:="TEI_abstract", lang:="hu")
    Call search_replace_dyn_style(search_style:="Resumeja", replace_root_style:="TEI_abstract", lang:="ja")
    Call search_replace_dyn_style(search_style:="Resumela", replace_root_style:="TEI_abstract", lang:="la")
    Call search_replace_dyn_style(search_style:="resumemg", replace_root_style:="TEI_abstract", lang:="mg")
    Call search_replace_dyn_style(search_style:="Resumemk", replace_root_style:="TEI_abstract", lang:="mk")
    Call search_replace_dyn_style(search_style:="samenvattingnl", replace_root_style:="TEI_abstract", lang:="nl")
    Call search_replace_dyn_style(search_style:="Samenvatingnl", replace_root_style:="TEI_abstract", lang:="nl")
    Call search_replace_dyn_style(search_style:="resumeru", replace_root_style:="TEI_abstract", lang:="ru")
    Call search_replace_dyn_style(search_style:="Resumetr", replace_root_style:="TEI_abstract", lang:="tr")
    
    Call search_replace_dyn_style(search_style:="MotsCles", replace_root_style:="TEI_keywords", lang:="fr")
    Call search_replace_dyn_style(search_style:="Mots Cles", replace_root_style:="TEI_keywords", lang:="fr")
    Call search_replace_dyn_style(search_style:="Keywords", replace_root_style:="TEI_keywords", lang:="en")
    Call search_replace_dyn_style(search_style:="keywords", replace_root_style:="TEI_keywords", lang:="en")
    Call search_replace_dyn_style(search_style:="Palabrasclaves", replace_root_style:="TEI_keywords", lang:="es")
    Call search_replace_dyn_style(search_style:="Parolechiave", replace_root_style:="TEI_keywords", lang:="fr")
    Call search_replace_dyn_style(search_style:="Schlagworter", replace_root_style:="TEI_keywords", lang:="de")
    Call search_replace_dyn_style(search_style:="Palavraschaves", replace_root_style:="TEI_keywords", lang:="pt")
    Call search_replace_dyn_style(search_style:="motsclespl", replace_root_style:="TEI_keywords", lang:="pl")
    Call search_replace_dyn_style(search_style:="MotsClesar", replace_root_style:="TEI_keywords", lang:="ar")
    Call search_replace_dyn_style(search_style:="motsclesca", replace_root_style:="TEI_keywords", lang:="ca")
    Call search_replace_dyn_style(search_style:="MotsClesel", replace_root_style:="TEI_keywords", lang:="el")
    Call search_replace_dyn_style(search_style:="MotsCleseu", replace_root_style:="TEI_keywords", lang:="eu")
    Call search_replace_dyn_style(search_style:="MotsCleshe", replace_root_style:="TEI_keywords", lang:="he")
    Call search_replace_dyn_style(search_style:="MotsCleshu", replace_root_style:="TEI_keywords", lang:="hu")
    Call search_replace_dyn_style(search_style:="MotsClesja", replace_root_style:="TEI_keywords", lang:="ja")
    Call search_replace_dyn_style(search_style:="VerbaClaves", replace_root_style:="TEI_keywords", lang:="la")
    Call search_replace_dyn_style(search_style:="motsclesmg", replace_root_style:="TEI_keywords", lang:="mg")
    Call search_replace_dyn_style(search_style:="MotsClesmk", replace_root_style:="TEI_keywords", lang:="mk")
    Call search_replace_dyn_style(search_style:="Trefwoordennl", replace_root_style:="TEI_keywords", lang:="nl")
    Call search_replace_dyn_style(search_style:="motsclesru", replace_root_style:="TEI_keywords", lang:="ru")
    Call search_replace_dyn_style(search_style:="MotsClestr", replace_root_style:="TEI_keywords", lang:="tr")
    
    Call search_style(search_style:="encadre")
    'Call search_style(search_style:="Annexe")
    'Call search_style(search_style:="Bibliographie")
    'Call search_style(search_style:="Tableau Grille�2", display_str:="Bibliographie")
    Call insert_appendix_start(search_style:="Annexe")
    Call search_replace_style(search_style:="Annexe", replace_style:=wdStyleNormal)
    Call search_replace_style(search_style:="Bibliographie", replace_style:="TEI_bibl_reference")
    Call search_replace_style(search_style:="Tableau Grille�2", replace_style:="TEI_bibl_reference") ' from MacOS
    
    ' Authors description
    Call search_replace_char_style_next_para(search_style:="Affiliation", replace_style:="TEI_authority_affiliation")
    Call search_replace_char_style_next_para(search_style:="Courriel", replace_style:="TEI_authority_mail")
    
    Call search_style_delete(search_style:="Datepubli")
    Call search_style_delete(search_style:="Datepublipapier")
    Call search_style_delete(search_style:="Titre oeuvre")
    Call search_style_delete(search_style:="Auteur oeuvre")
    Call search_style_delete(search_style:="Date publi oeuvre")
    Call search_style_delete(search_style:="TitreOeuvre")
    Call search_style_delete(search_style:="AuteurOeuvre")
    Call search_style_delete(search_style:="DatePubliOeuvre")
    
    ' from oe_template_en
    Call search_replace_style(search_style:="Subhead", replace_style:="TEI_title:sup")
    Call search_replace_style(search_style:="Author", replace_style:="TEI_author:aut")
    Call search_replace_style(search_style:="Translator", replace_style:="TEI_editor:trl")
    Call search_replace_style(search_style:="AcademicEditor", replace_style:="TEI_editor:edt")
    Call search_replace_style(search_style:="AuthorDescription", replace_style:="TEI_authority_biography")
    Call search_replace_style(search_style:="Language", replace_style:="TEI_language")
    Call search_replace_style(search_style:="Subject", replace_style:="TEI_keywords_subjects:subject")
    Call search_replace_style(search_style:="Chronology", replace_style:="TEI_keywords_subjects:chronology")
    Call search_replace_style(search_style:="Geography", replace_style:="TEI_keywords_subjects:geography")
    Call search_replace_style(search_style:="ReviewedReference", replace_style:="TEI_reviewed_reference")
    Call search_replace_style(search_style:="PageNumbering", replace_style:="TEI_pagination")
    Call search_replace_style(search_style:="excavationsdirector", replace_style:="TEI_editor:fld")
    Call search_replace_style(search_style:="Collaborator", replace_style:="TEI_editor:ctb")
    Call search_replace_style(search_style:="QuotedPersons", replace_style:="TEI_keywords_subjects:personcited")
    Call search_replace_style(search_style:="Works", replace_style:="TEI_keywords_subjects:work")
    Call search_replace_style(search_style:="documentnumber", replace_style:="TEI_document_number")
    Call search_replace_style(search_style:="AuthorsNote", replace_style:="TEI_note:aut")
    Call search_replace_style(search_style:="EditorsNote", replace_style:="TEI_note:pbl")
    Call search_replace_style(search_style:="Acknowledgements", replace_style:="TEI_acknowledgment")
    Call search_replace_style(search_style:="Dedication", replace_style:="TEI_dedication")
    Call search_replace_style(search_style:="NotIndentedParagraph", replace_style:="TEI_paragraph_consecutive")
    Call search_replace_style(search_style:="IllustrationTitle", replace_style:="TEI_figure_title")
    Call search_replace_style(search_style:="IllustrationCaption", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="IllustrationCredits", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="Quotation", replace_style:="TEI_quote")
    Call search_replace_style(search_style:="QuotationBis", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="QuotationTer", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Answer", replace_style:="TEI_answer")
    Call search_replace_style(search_style:="Separator", replace_style:="TEI_paragraph_break")
    
    Call search_style_delete(search_style:="ReviewedTitle")
    Call search_style_delete(search_style:="ReviewedAuthor")
    Call search_style_delete(search_style:="ReviewedDate")
    Call search_style_delete(search_style:="PubliDate")
    Call search_style_delete(search_style:="ePubliDate")
   
    Call search_style(search_style:="box")
    'Call search_style(search_style:="Appendix")
    Call insert_appendix_start(search_style:="Appendix")
    Call search_replace_style(search_style:="Appendix", replace_style:=wdStyleNormal)
    Call search_replace_style(search_style:="Bibliography", replace_style:="TEI_bibl_reference")
 
    ' from revuesorg_complet_es
    Call search_replace_style(search_style:="Antetitulo", replace_style:="TEI_title:sup")
    Call search_replace_style(search_style:="Autor", replace_style:="TEI_author:aut")
    Call search_replace_style(search_style:="Traductor", replace_style:="TEI_editor:trl")
    Call search_replace_style(search_style:="EditorCientifico", replace_style:="TEI_editor:edt")
    Call search_replace_style(search_style:="DescripcionAutor", replace_style:="TEI_authority_biography")
    Call search_replace_style(search_style:="Idioma", replace_style:="TEI_language")
    Call search_replace_style(search_style:="Temas", replace_style:="TEI_keywords_subjects:subject")
    Call search_replace_style(search_style:="Periodo", replace_style:="TEI_keywords_subjects:chronology")
    Call search_replace_style(search_style:="Geografia", replace_style:="TEI_keywords_subjects:geography")
    Call search_replace_style(search_style:="ReferenciaBibliograficaObra", replace_style:="TEI_reviewed_reference")
    Call search_replace_style(search_style:="Paginacion", replace_style:="TEI_pagination")
    Call search_replace_style(search_style:="NumeroDocumento", replace_style:="TEI_document_number")
    Call search_replace_style(search_style:="Agradecimientos", replace_style:="TEI_acknowledgment")
    Call search_replace_style(search_style:="Dedicatoria", replace_style:="TEI_dedication")
    Call search_replace_style(search_style:="Epigrafe", replace_style:="TEI_epigraph")
    Call search_replace_style(search_style:="ParrafoSinSangria", replace_style:="TEI_paragraph_consecutive")
    Call search_replace_style(search_style:="Titulofigura", replace_style:="TEI_figure_title")
    Call search_replace_style(search_style:="LeyendaFigura", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="CreditosFigura", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="CitaTextualBis", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="CitaTextualTer", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Pregunta", replace_style:="TEI_question")
    Call search_replace_style(search_style:="Respuesta", replace_style:="TEI_answer")
    Call search_replace_style(search_style:="Separador", replace_style:="TEI_paragraph_break")
    
    Call search_style_delete(search_style:="TituloObra")
    Call search_style_delete(search_style:="AutorObra")
    Call search_style_delete(search_style:="FechaPubliObra")
    Call search_style_delete(search_style:="Fechapublipapel")
    Call search_style_delete(search_style:="Fechapubli")
   
    Call search_style(search_style:="Cuadro")
    'Call search_style(search_style:="Anexo")
    'Call search_style(search_style:="Bibliograf�a")
    Call insert_appendix_start(search_style:="Anexo")
    Call search_replace_style(search_style:="Anexo", replace_style:=wdStyleNormal)
    Call search_replace_style(search_style:="Bibliograf�a", replace_style:="TEI_bibl_reference")
    
    ' from revuesorg_complet_it
    Call search_replace_style(search_style:="Autore", replace_style:="TEI_author:aut")
    Call search_replace_style(search_style:="Traduttore", replace_style:="TEI_editor:trl")
    Call search_replace_style(search_style:="Curatore", replace_style:="TEI_editor:edt")
    Call search_replace_style(search_style:="Descrizione autore", replace_style:="TEI_authority_biography")
    Call search_replace_style(search_style:="Lingua", replace_style:="TEI_language")
    Call search_replace_style(search_style:="Info bibliografiche opera commentata", replace_style:="TEI_reviewed_reference")
    Call search_replace_style(search_style:="Paginazione", replace_style:="TEI_pagination")
    Call search_replace_style(search_style:="Nota autore", replace_style:="TEI_note:aut")
    Call search_replace_style(search_style:="Nota redazione", replace_style:="TEI_note:pbl")
    Call search_replace_style(search_style:="Ringraziamenti", replace_style:="TEI_acknowledgment")
    Call search_replace_style(search_style:="Dedica", replace_style:="TEI_dedication")
    Call search_replace_style(search_style:="Paragrafo senza rientro", replace_style:="TEI_paragraph_consecutive")
    Call search_replace_style(search_style:="Titolo illustrazione", replace_style:="TEI_figure_title")
    Call search_replace_style(search_style:="Legenda illustrazione", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="Fonte illustrazione", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="Citazione bis", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Citazione ter", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Domanda", replace_style:="TEI_question")
    Call search_replace_style(search_style:="Risposta", replace_style:="TEI_answer")
    Call search_replace_style(search_style:="Separatore", replace_style:="TEI_paragraph_break")
    Call search_replace_style(search_style:="Codice", replace_style:="TEI_code")
    
    Call search_style_delete(search_style:="Titolo opera commentata")
    Call search_style_delete(search_style:="Autore opera commentata")
    Call search_style_delete(search_style:="Data pubblicazione opera commentata")
    Call search_style_delete(search_style:="Data pubblicazione cartacea")
    Call search_style_delete(search_style:="Data pubblicazione elettronica")
   
    Call search_style(search_style:="Riquadro")
    'Call search_style(search_style:="Appendice")
    'Call search_style(search_style:="Bibliografia")
    Call insert_appendix_start(search_style:="Appendice")
    Call search_replace_style(search_style:="Appendice", replace_style:=wdStyleNormal)
    Call search_replace_style(search_style:="Bibliografia", replace_style:="TEI_bibl_reference")
    
    ' from revuesorg_complet_pt
    Call search_replace_style(search_style:="Tradutor", replace_style:="TEI_editor:trl")
    Call search_replace_style(search_style:="CoordenadorCientifico", replace_style:="TEI_editor:edt")
    Call search_replace_style(search_style:="DescricaoAutor", replace_style:="TEI_authority_biography")
    Call search_replace_style(search_style:="Tematico", replace_style:="TEI_keywords_subjects:subject")
    Call search_replace_style(search_style:="Cronologia", replace_style:="TEI_keywords_subjects:chronology")
    Call search_replace_style(search_style:="Paginacao", replace_style:="TEI_pagination")
    Call search_replace_style(search_style:="Epigrafo", replace_style:="TEI_epigraph")
    Call search_replace_style(search_style:="Errata", replace_style:="TEI_erratum")
    Call search_replace_style(search_style:="NotaAutor", replace_style:="TEI_note:aut")
    Call search_replace_style(search_style:="Nota do editor", replace_style:="TEI_note:pbl")
    Call search_replace_style(search_style:="Agradecimentos", replace_style:="TEI_acknowledgment")
    Call search_replace_style(search_style:="ParagrafoNaoRecolhido", replace_style:="TEI_paragraph_consecutive")
    Call search_replace_style(search_style:="TituloImagem", replace_style:="TEI_figure_title")
    Call search_replace_style(search_style:="LegendaImagem", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="CreditosImagem", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="Pergunta", replace_style:="TEI_question")
    Call search_replace_style(search_style:="Resposta", replace_style:="TEI_answer")
    Call search_replace_style(search_style:="Codigo", replace_style:="TEI_code")
    
    Call search_style_delete(search_style:="DataPublicacaoObra")
    Call search_style_delete(search_style:="DataPubliPapel")
    Call search_style_delete(search_style:="DataPubli")
   
    Call search_style(search_style:="Caixa")
    'Call search_style(search_style:="Anexos")
    Call insert_appendix_start(search_style:="Anexos")
    Call search_replace_style(search_style:="Anexos", replace_style:=wdStyleNormal)
    
    ' from revuesorg_complet_pl
    'Call search_replace_style(search_style:="Nadtytul", replace_style:="TEI_title:sup")
    Call search_replace_style(search_style:="Autore", replace_style:="TEI_author:aut")
    'Call search_replace_style(search_style:="T?umacz", replace_style:="TEI_editor:trl")
    Call search_replace_style(search_style:="Wydawca akademicki", replace_style:="TEI_editor:edt")
    Call search_replace_style(search_style:="O autorze", replace_style:="TEI_authority_biography")
    'Call search_replace_style(search_style:="J?zykowo", replace_style:="TEI_language")
    Call search_replace_style(search_style:="Tematycznie", replace_style:="TEI_keywords_subjects:subject")
    Call search_replace_style(search_style:="Czasowo", replace_style:="TEI_keywords_subjects:chronology")
    Call search_replace_style(search_style:="Geograficznie", replace_style:="TEI_keywords_subjects:geography")
    ' Call search_replace_style(search_style:="Odwo?anie bibliograficzne recenzji", replace_style:="TEI_reviewed_reference")
    Call search_replace_style(search_style:="Paginacja", replace_style:="TEI_pagination")
    Call search_replace_style(search_style:="Numer dokumentu", replace_style:="TEI_document_number")
    Call search_replace_style(search_style:="Nota autorska", replace_style:="TEI_note:aut")
    Call search_replace_style(search_style:="Nota redakcyjna", replace_style:="TEI_note:edt")
    'Call search_replace_style(search_style:="Podzi?kowania", replace_style:="TEI_acknowledgment")
    Call search_replace_style(search_style:="Dedykacja", replace_style:="TEI_dedication")
    Call search_replace_style(search_style:="Motto", replace_style:="TEI_epigraph")
    'Call search_replace_style(search_style:="Paragraf bez wci?cia", replace_style:="TEI_paragraph_consecutive")
    'Call search_replace_style(search_style:="Tytu? ilustracji", replace_style:="TEI_figure_title")
    Call search_replace_style(search_style:="Legenda ilustracji", replace_style:="TEI_figure_caption")
    Call search_replace_style(search_style:="Prawa do ilustracji", replace_style:="TEI_figure_credits")
    Call search_replace_style(search_style:="Cytat bis", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Citat ter", replace_style:="TEI_quote2")
    Call search_replace_style(search_style:="Pytanie", replace_style:="TEI_question")
    'Call search_replace_style(search_style:="Odpowied?", replace_style:="TEI_answer")
    Call search_replace_style(search_style:="Kod", replace_style:="TEI_code")
    Call search_replace_style(search_style:="Separatore", replace_style:="TEI_paragraph_break")
    
    'Call search_style_delete(search_style:="Tytu? recenzji")
    Call search_style_delete(search_style:="Autor recenzji")
    Call search_style_delete(search_style:="Data publikacji recenzji")
    Call search_style_delete(search_style:="Data publikacji drukowanej")
    Call search_style_delete(search_style:="Data publikacji elektronicznej")
   
    Call search_style(search_style:="Ramka")
    'Call search_style(search_style:="Za??cznik")
    'Call search_replace_style(search_style:="Za??cznik", replace_style:="TEI_bibl_reference")
        
    ' Insert bibl_start (all languagse - after biblio style replace)
    Call insert_bibl_start(search_style:="TEI_bibl_reference")
    
    ' DEPRECATED
    ' Call convert_language
   
    If Deleted <> "" Then
        Msg = "Les paragraphes suivants ne sont plus utilis�s avec Circ� et ont �t� supprim�s : " + vbNewLine + vbNewLine + Deleted + vbNewLine + vbNewLine
    End If
    If Todo <> "" Then
        Msg = Msg + "Styles � traiter manuellement : " + vbNewLine + vbNewLine + Todo
    End If
    If Msg <> "" Then
        msgbox Msg
    Else
        msgbox "Termin�"
    End If
    
End Sub


' Search style and Replace with a dynamic style with lang (root_style|lang)
Sub search_replace_dyn_style(Optional search_style As String, Optional replace_root_style As String, Optional lang As String)
    Dim replace_style As String
    Dim s_style As style
    If styleExists(search_style, ActiveDocument) = True Then
        Set s_style = ActiveDocument.Styles(search_style)
        If s_style.InUse = True Then
            replace_style = replace_root_style + "|" + lang
            If styleExists(replace_style, ActiveDocument) = False Then
                Call create_dyn_style(root_style:=replace_root_style, lang:=lang)
            End If
            Call search_replace_style(search_style:=search_style, replace_style:=replace_style)
        End If
    End If
End Sub

' Create a new style : root_style|lang
Sub create_dyn_style(Optional root_style As String, Optional lang As String)
    Dim style_name As String
    style_name = root_style + "|" + lang
    On Error Resume Next
    Set child_style = ActiveDocument.Styles.Add(style_name, wdStyleTypeParagraph)
    child_style.BaseStyle = ActiveDocument.Styles(root_style)
    Selection.paragraphs.style = ActiveDocument.Styles(styleName)
End Sub

' Search style and delete TEXT styled with this style
Sub search_style_delete(Optional search_style As String, Optional msg_box As Boolean = True)
    If styleExists(search_style, ActiveDocument) = True Then
        ' top of the doc
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start
        ' search the style
        Selection.Find.style = ActiveDocument.Styles(search_style)
        With Selection.Find
        ' get text from selection
            If msg_box Then
                While .Execute
                    DeletedStr = DeletedStr & Selection.Text
                    Selection.Start = Selection.End + 1
                    Selection.End = Selection.Start
                Wend
                If DeletedStr <> "" Then
                    Deleted = Deleted + search_style + " : " + DeletedStr
                End If
            End If
        ' delete selection
            .Text = ""
            .Replacement.Text = ""
            .Replacement.ClearFormatting
            .Execute Replace:=wdReplaceAll, Forward:=True, Wrap:=wdFindContinue
        End With
    End If
End Sub

' Identifies existing styles 'search_style' and adds them (or 'display_str' if provided) to the Todo list
Sub search_style(Optional search_style As String, Optional display_str As String)
    If styleExists(search_style, ActiveDocument) = True Then
        ' top of the doc
        Selection.Start = ActiveDocument.Content.Start
        Selection.End = ActiveDocument.Content.Start
        ' search the style
        Selection.Find.style = ActiveDocument.Styles(search_style)
        With Selection.Find
        ' get text from selection
            While .Execute And Found = False
                If display_str = "" Then
                   display_str = search_style
                End If
                Todo = Todo + display_str + vbNewLine
                Found = True
            Wend
        End With
    End If
End Sub

' Search and Replace style
Sub search_replace_style(Optional search_style As String, Optional replace_style As String)
   If styleExists(search_style, ActiveDocument) = True Then
        Selection.Find.style = ActiveDocument.Styles(search_style)
        With Selection.Find
            .Text = ""
            .Replacement.Text = "^&"
            .Replacement.style = ActiveDocument.Styles(replace_style)
            .Execute Replace:=wdReplaceAll, Forward:=True, Wrap:=wdFindContinue
        End With
    End If
End Sub

' Search if a style exists in the document. Used in all other functions
Function styleExists(ByVal styleToTest As String, ByVal docToTest As Word.Document) As Boolean
    Dim testStyle As Word.style
    On Error Resume Next
    Set testStyle = docToTest.Styles(styleToTest)
    styleExists = Not testStyle Is Nothing
End Function


' Search if a CHARACTER style exists in the document. Used in all other functions
Function charStyleExists(ByVal styleToTest As String, ByVal docToTest As Word.Document) As Boolean
    Dim testStyle As Word.style
    On Error Resume Next
    Set testStyle = docToTest.Styles(styleToTest)
    If Not testStyle Is Nothing Then
      If testStyle.Type = wdStyleTypeCharacter Then
        charStyleExists = True
        Exit Function
      Else
        charStyleExists = False
        Exit Function
      End If
    Else
      charStyleExists = False
      Exit Function
    End If
End Function

' Search and Replace CHARACTER style
Sub search_replace_char_style(Optional search_style As String, Optional replace_style As String)
   If charStyleExists(search_style, ActiveDocument) = True Then
        Selection.Find.style = ActiveDocument.Styles(search_style)
          With Selection.Find
            If True Then
            .Text = ""
            .Replacement.Text = "^&"
            .Replacement.style = ActiveDocument.Styles(replace_style)
            .Execute Replace:=wdReplaceAll, Forward:=True, Wrap:=wdFindContinue
            End If
          End With
        
    End If
End Sub


' Search a CHARACTER style, insert TEXT in a new paragraph and style this paragraph with replace _style
Sub search_replace_char_style_next_para(Optional search_style As String, Optional replace_style As String)
    If styleExists(search_style, ActiveDocument) = True Then
    
        Dim rngSearch As Range
        Set rngSearch = ActiveDocument.Range
        
        Dim SelParaLast As Paragraph
        Dim NewPara As Paragraph
   
        With rngSearch.Find
            .style = ActiveDocument.Styles(search_style)
            Do While .Execute(FindText:="", Forward:=True) = True
                With rngSearch 'we will work with what is found as it will be the selection
                    searched_str = .Text
                                        
                    ' Add new parahraph with "replace_style" and insert the found string
                    Set SelParaLast = .paragraphs.Last
                    SelParaLast.Range.InsertParagraphAfter
                    'Le comportement Word diff�re si la s�lection va jusqu'au dernier paragraphe ou non
                    If Not (.paragraphs.Last.Range.Characters.Count = 1) Then
                        'Selection dans le texte
                        Set NewPara = SelParaLast.Next
                    Else
                        'Selection avec dernier paragraphe du texte
                        Set NewPara = .paragraphs.Last
                    End If
        
                    NewPara.style = ActiveDocument.Styles(replace_style)
                    ActiveDocument.Range(NewPara.Range.Start, NewPara.Range.End - 1).Text = searched_str
                    
                End With
                rngSearch.Collapse Direction:=wdCollapseEnd
                'keep it moving
            Loop
        End With
        ' Remove search_style
        ActiveDocument.Styles(search_style).Delete
    End If
End Sub

Sub insert_section_start(Optional search_style As String, Optional section_header_style As String, Optional section_header_text As String)
    If styleExists(search_style, ActiveDocument) = True Then
        Dim rngSearch As Range
        Set rngSearch = ActiveDocument.Range
        Dim SelParaFirst As Paragraph
        Dim PrevRange As Range
        Dim oTable As table
        Dim oRng As Range
        
        With rngSearch.Find
            .style = ActiveDocument.Styles(search_style)
            If .Execute(FindText:="", Forward:=True) = True Then
                ' If TEI_appendix_start aleardy exists, remove it
                Call search_style_delete(search_style:=section_header_style, msg_box:=False)
                With rngSearch
                    Set SelParaFirst = .paragraphs.First
                    If SelParaFirst.Range.Information(wdWithInTable) = True Then
                        SelParaFirst.Range.Select
                        
                        Set oTable = Selection.Tables(1)
                        
                        Set oRng = oTable.Range
                        Set SelParaPrevious = oRng.paragraphs.First.Previous
                        
                         ' Search if Previous paragraphs are Headings and set the correct SelParaFirst
                        Set SelParaPrevious = SelParaFirst.Previous
                        While (SelParaPrevious.OutlineLevel <= 6 And SelParaPrevious.OutlineLevel >= 1 _
                            Or SelParaPrevious.style = "Titreillustration" _
                            Or SelParaPrevious.style = "Titre Illustration" _
                            Or SelParaPrevious.style = "IllustrationTitle" _
                            Or SelParaPrevious.style = "TEI_figure_title")
                            
                            Set SelParaFirst = SelParaPrevious
                            Set SelParaPrevious = SelParaPrevious.Previous
                        Wend
                        SelParaFirst.Range.InsertParagraphBefore
                        Set PrevRange = SelParaFirst.Previous.Range
                    ' La s�lection n'est pas dans un tableau
                    Else
                        ' Search if Previous paragraphs are Headings and set the correct SelParaFirst
                        Set SelParaPrevious = SelParaFirst.Previous
                        While (SelParaPrevious.OutlineLevel <= 6 And SelParaPrevious.OutlineLevel >= 1 _
                            Or SelParaPrevious.style = "Titreillustration" _
                            Or SelParaPrevious.style = "Titre Illustration" _
                            Or SelParaPrevious.style = "IllustrationTitle" _
                            Or SelParaPrevious.style = "TEI_figure_title")
                            
                            Set SelParaFirst = SelParaPrevious
                            Set SelParaPrevious = SelParaPrevious.Previous
                        Wend
                        SelParaFirst.Range.InsertParagraphBefore
                        Set PrevRange = SelParaFirst.Previous.Range
                    End If
                    
                    PrevRange.style = ActiveDocument.Styles(section_header_style)
                    ActiveDocument.Range(PrevRange.Start, PrevRange.End - 1).Text = section_header_text
                End With
            End If
        End With
        Set rngSearch = Nothing
        Set SelParaFirst = Nothing
        Set PrevRange = Nothing
        Set oTable = Nothing
        Set oRng = Nothing
    End If
End Sub

Sub insert_appendix_start(Optional search_style As String)
    Call insert_section_start(search_style, "TEI_appendix_start", "##### APPENDIX START #####")
End Sub

Sub insert_bibl_start(Optional search_style As String)
    Call insert_section_start(search_style, "TEI_bibl_start", "##### BIBLIOGRAPHY START #####")
End Sub


' "Dictionnary" old_language -> new_language
' DEPRECATED
Function new_language(ByVal old_language As String) As String
    Select Case old_language
        Case "de", "De"
            new_language = "de-DE"
        Case "en", "En"
            new_language = "en-GB"
        Case "fr", "Fr"
            new_language = "fr-FR"
        Case "es", "Es"
            new_language = "es-ES"
        Case "pt", "Pt"
            new_language = "pt-PT"
        Case "it", "It"
            new_language = "it-IT"
        Case "pl", "Pl"
            new_language = "pl-PL"
        Case "ru", "Ru"
            new_language = "ru-RU"
        Case "nl", "Nl"
            new_language = "nl-NL"
    End Select
End Function

' Search in paragraph styled as "TEI_language" old langage str (xFind) and replace with new string (Function "new_language")
' DEPRECATED
Sub convert_language()
    Dim lang_style As String
    lang_style = "TEI_language"
    If styleExists(lang_style, ActiveDocument) = True Then
        Dim xFind As String
        xFind = "de,De,en,En,fr,Fr,es,Es,pt,Pt,it,It,pl,Pl,ru,Ru,nl,Nl"
        Dim xFindArr
        Dim i As Long
       ' Make Array with String
        xFindArr = Split(xFind, ",")
        For i = 0 To UBound(xFindArr)
            Selection.Find.style = ActiveDocument.Styles(lang_style)
            With Selection.Find
                .Text = xFindArr(i) + "^p"
                .Replacement.Text = new_language(xFindArr(i)) + "^p"
                .MatchWholeWord = True
                .MatchCase = True
                .Execute Replace:=wdReplaceOne, MatchCase:=True, MatchWholeWord:=True
            End With
            ' Exit for after first Match
            If Selection.Find.Found Then
              Exit For
            End If
       Next
    End If
End Sub
