Attribute VB_Name = "tools_ribbon_menu"
Sub menu_retablissement_insec(ByVal control As IRibbonControl)
    nom_macro_globale = "R�tablir les espaces ins�cables dans le corps de texte"
    On Error GoTo menu_retablissement_insec_Erreur
    Call tools_library.Espace_insecables_simple
    Exit Sub
    On Error GoTo 0
    Exit Sub
    
menu_retablissement_insec_Erreur:
    Call tools_errors.info_syst
End Sub


Sub menu_suppression_double_para(ByVal control As IRibbonControl)
    nom_macro_globale = "Supprimer les doubles sauts de paragraphe"
    On Error GoTo menu_suppression_double_para_Erreur
    Call tools_library.double_saut_paragraphe
    Exit Sub
    On Error GoTo 0
    Exit Sub
menu_suppression_double_para_Erreur:
    Call tools_errors.info_syst
End Sub



Sub menu_remplacement_apostrophes(ByVal control As IRibbonControl)
    nom_macro_globale = "Remplacer les apostrophes verticales par des apostrophes typographiques"
    On Error GoTo menu_remplacement_apostrophes_Erreur
    Call tools_library.apostrophe_cdt(var_batch:=False)
    Exit Sub
    On Error GoTo 0
    Exit Sub
    
menu_remplacement_apostrophes_Erreur:
    Call tools_errors.info_syst
End Sub


Sub menu_remplacement_guillemets_en(ByVal control As IRibbonControl)
    nom_macro_globale = "Remplacer les guillemets verticaux par des guillemets doubles anglais"
    On Error GoTo menu_remplacement_guillemets_en_Erreur
    Call tools_library.Guillemets_anglais_simple
    Exit Sub
    On Error GoTo 0
    Exit Sub
    
menu_remplacement_guillemets_en_Erreur:
    Call tools_errors.info_syst
End Sub

Sub menu_remplacement_guillemets_fr(ByVal control As IRibbonControl)
    nom_macro_globale = "Remplacer les guillemets verticaux par des guillemets fran�ais"
    On Error GoTo menu_remplacement_guillemets_fr_Erreur
    Call tools_library.Guillemets_francais_simple
    Exit Sub
    On Error GoTo 0
    Exit Sub
menu_remplacement_guillemets_fr_Erreur:
    Call tools_errors.info_syst
End Sub

Sub menu_effacement_surlignement(ByVal control As IRibbonControl)
    nom_macro_globale = "Effacer le surlignement"
    On Error GoTo menu_effacement_surlignement_Erreur
    Call tools_library.nettoyage_surlignement
    Exit Sub
    On Error GoTo 0
    Exit Sub
    
menu_effacement_surlignement_Erreur:
    Call tools_errors.info_syst
End Sub


Sub menu_verif_para(ByVal control As IRibbonControl)
    nom_macro_globale = "V�rifier les paragraphes"
    On Error GoTo menu_verif_para_Erreur
    Call tools_library.verification_paragraphe
    Exit Sub
    On Error GoTo 0
    Exit Sub
menu_verif_para_Erreur:
    Call tools_errors.info_syst
End Sub

Sub menu_fin_verif_para(ByVal control As IRibbonControl)
    nom_macro_globale = "Fin de la v�rification des paragraphes"
    On Error GoTo menu_fin_verif_para_Erreur
    Call tools_library.fin_verif_paragraphes
    Exit Sub
    On Error GoTo 0
    Exit Sub
menu_fin_verif_para_Erreur:
    Call tools_errors.info_syst
End Sub


Sub menu_renumerotation_notes(ByVal control As IRibbonControl)
    nom_macro_globale = "Renum�roter les notes"
    If ActiveDocument.Footnotes.Count < 1 Then
        Call msgbox("Erreur : ce document ne semble pas contenir de notes de bas de page.", vbExclamation, "Absence de notes")
        Exit Sub
    End If
    
    On Error GoTo menu_renumerotation_notes_Erreur
    Call tools_library.renumerotation_notes
    Exit Sub
    On Error GoTo 0
    Exit Sub
    
menu_renumerotation_notes_Erreur:
    Call tools_errors.info_syst
End Sub


Sub menu_points_notes(ByVal control As IRibbonControl)
    nom_macro_globale = "Supprimer les points aprs les num�ros de notes"""

    If ActiveDocument.Footnotes.Count < 1 Then
        Call msgbox("Erreur : ce document ne semble pas contenir de notes de bas de page.", vbExclamation, "Absence de notes")
        Exit Sub
    End If
    
    On Error GoTo menu_points_notes_Erreur
    Call tools_library.nettoyage_notes_points
    Exit Sub
    On Error GoTo 0
    Exit Sub

menu_points_notes_Erreur:
    Call tools_errors.info_syst
End Sub


