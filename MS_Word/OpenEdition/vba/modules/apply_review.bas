Attribute VB_Name = "apply_review"
Sub reviewed_reference(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_reviewed_reference")
End Sub
Sub reviewed_title_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_reviewed_title-inline")
End Sub
Sub reviewed_author_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_reviewed_author-inline")
End Sub
Sub reviewed_date_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_reviewed_date-inline")
End Sub
