Attribute VB_Name = "tools_errors"
'2020 OpenEdition Center USR 2004
'Les macros de ce document sont optimis�es pour Microsoft Word (2016 et +) sur Windows

Option Explicit
Public nom_appli As String, version_word As String, systeme_exploitation As String, version_systeme As String
Public langue_interface As String
Public code_erreur As String, source_erreur As String, description_erreur As String
Public addins_installes As String

Sub info_syst()
    'RAPPORT D'ERREURS
    
    'Remise � z�ro des informations �ventuellement enregistr�es pr�c�demment :
    
    'Syst�me
    systeme_exploitation = ""
    version_systeme = ""
    'Application
    nom_appli = ""
    version_word = ""
    langue_interface = ""
    'Erreur
    code_erreur = ""
    source_erreur = ""
    description_erreur = ""
    'Addins
    addins_installes = ""
    'Numero de ligne d'erreur
    
    'Fin de la remise � z�ro
    
    'Enregistrement des infos :
    
    'Syst�me
    systeme_exploitation = System.OperatingSystem
    'version_systeme = System.Version
    
    'Application
    nom_appli = Application.Name
    version_word = Application.Version & " / " & Application.Build
    langue_interface = Application.language
    
    'Erreur
    code_erreur = Err.Number
    source_erreur = Err.Source
    description_erreur = Err.Description
    
    'Addins
    Dim i As Integer
    Dim nbre_addins As Integer
    nbre_addins = Application.AddIns.Count
    
    If nbre_addins > 0 Then
        For i = 1 To nbre_addins
            If Application.AddIns(i).Installed = True Then
                addins_installes = addins_installes & " - " & Application.AddIns(i).Name
            End If
        Next i
    End If
    
    
    On Error GoTo 0
    'ActiveDocument.Show 'comment� suite � bug
    'Msg_err.Show
    
End Sub



