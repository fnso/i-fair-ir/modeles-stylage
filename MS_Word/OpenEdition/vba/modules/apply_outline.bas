Attribute VB_Name = "apply_outline"
Sub h1(control As IRibbonControl)
    Call h1_cmd
End Sub
Sub h2(control As IRibbonControl)
    Call h2_cmd
End Sub
Sub h3(control As IRibbonControl)
    Call h3_cmd
End Sub
Sub h4(control As IRibbonControl)
    Call h4_cmd
End Sub
Sub h5(control As IRibbonControl)
    Call h5_cmd
End Sub
Sub h6(control As IRibbonControl)
    Call h6_cmd
End Sub
Sub h1_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading1)
End Sub
Sub h2_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading2)
End Sub
Sub h3_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading3)
End Sub
Sub h4_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading4)
End Sub
Sub h5_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading5)
End Sub
Sub h6_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleHeading6)
End Sub
Sub paragraph_break(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_paragraph_break")
End Sub
