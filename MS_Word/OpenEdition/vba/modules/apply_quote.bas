Attribute VB_Name = "apply_quote"
Sub quote_section(control As IRibbonControl)
    Call section("TEI_quote_start", "##### QUOTE START #####", "TEI_quote_end", "##### QUOTE END #####")
End Sub

Sub quote(control As IRibbonControl)
    Call quote_cmd
End Sub
Sub quote_cmd()
    paragraphs.style = ActiveDocument.Styles("TEI_quote")
End Sub

Sub quote2(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_quote2")
End Sub
Sub quote_continuation(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_quote_continuation")
End Sub
Sub quote_nested(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_quote_nested")
End Sub
Sub quote_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_quote-inline")
End Sub
Sub bibl_citation(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_bibl_citation")
End Sub
Sub replica(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_replica")
End Sub
Sub speaker(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_speaker")
End Sub
Sub speaker_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_speaker-inline")
End Sub
Sub didascaly(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_didascaly")
End Sub
Sub didascaly_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_didascaly-inline")
End Sub
Sub verse(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_verse")
End Sub
Sub versifiedreplica(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_versifiedreplica")
End Sub
Sub versenumber_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_versenumber-inline")
End Sub

Sub linguistic_section(control As IRibbonControl)
    Call section("TEI_linguistic_start", "##### LINGUISTIC EXAMPLE START #####", "TEI_linguistic_end", "##### LINGUISTIC EXAMPLE END #####")
End Sub
Sub linguistic_label(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_linguistic_label")
End Sub
Sub linguistic_example(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_linguistic_example")
End Sub
Sub linguistic_gloss(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_linguistic_gloss")
End Sub
Sub linguistic_translation(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_linguistic_translation")
End Sub
Sub linguistic_num(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_linguistic_num")
End Sub
Sub linguistic_lang_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_linguistic_lang-inline")
End Sub
Sub quote_trl(control As IRibbonControl)
    ApplyStyleLang "TEI_quote:trl"
End Sub

