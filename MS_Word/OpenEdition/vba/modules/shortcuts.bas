Attribute VB_Name = "shortcuts"
Sub AddKeyBinding()
    With Application
         ' \\ Do customization in THIS document
        .CustomizationContext = ActiveDocument

        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyShift, wdKeyT), _
        Command:="title_main_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKeyT), _
        Command:="title_sub_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKeyA), _
        Command:="author_aut_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKey1), _
        Command:="h1_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKey2), _
        Command:="h2_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKey3), _
        Command:="h3_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKey4), _
        Command:="h4_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKey5), _
        Command:="h5_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKey6), _
        Command:="h6_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyShift, wdKeyN), _
        Command:="normal_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKeyC), _
        Command:="quote_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKeyB), _
        Command:="bibl_reference_cmd"
        
        .KeyBindings.Add KeyCategory:=wdKeyCategoryCommand, _
        KeyCode:=BuildKeyCode(wdKeyControl, wdKeyAlt, wdKeyP), _
        Command:="foot_note_cmd"
        
     End With
End Sub

