Attribute VB_Name = "apply_text"
Sub floating_text(control As IRibbonControl)
    Call section("TEI_floatingText_start", "##### FLOATING TEXT START #####", "TEI_floatingText_end", "##### FLOATING TEXT END #####")
End Sub
Sub floating_text_title(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_floatingText_title")
End Sub
Sub normal(control As IRibbonControl)
    Call normal_cmd
End Sub
Sub normal_cmd()
    paragraphs.style = ActiveDocument.Styles(wdStyleNormal)
End Sub
Sub paragraph_consecutive(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_paragraph_consecutive")
End Sub
Sub bibl_reference(control As IRibbonControl)
    bibl_reference_cmd
End Sub
Sub bibl_reference_cmd()
    paragraphs.style = ActiveDocument.Styles("TEI_bibl_reference")
End Sub
Sub bibl_reference_inline(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles("TEI_bibl_reference-inline")
End Sub
Sub question(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_question")
End Sub
Sub answer(control As IRibbonControl)
    paragraphs.style = ActiveDocument.Styles("TEI_answer")
End Sub

Sub code(control As IRibbonControl)
    Dim result As String
    Dim styleName As String
    
    Unload code_selector
    Load code_selector
    code_selector.Show
    result = code_selector.dropdown_code.value

    If result <> "" And result <> "undefined" Then
        styleName = "TEI_code:" + result
        On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
        the_style.BaseStyle = ActiveDocument.Styles("TEI_code")
        Selection.paragraphs.style = ActiveDocument.Styles(styleName)
    Else
        Selection.paragraphs.style = ActiveDocument.Styles("TEI_code")
    End If
   
    Call section("TEI_code_start", "##### CODE START #####", "TEI_code_end", "##### CODE END #####")
    
End Sub

Sub code_inline(control As IRibbonControl)
    Dim result As String
    Dim styleName As String
    
    Unload code_selector
    Load code_selector
    code_selector.Show
    result = code_selector.dropdown_code.value

    If result <> "" And result <> "undefined" Then
        styleName = "TEI_code-inline:" + result
        On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeCharacter)
        the_style.BaseStyle = ActiveDocument.Styles("TEI_code-inline")
        Selection.style = ActiveDocument.Styles(styleName)
    Else
        Selection.style = ActiveDocument.Styles("TEI_code-inline")
    End If
End Sub
