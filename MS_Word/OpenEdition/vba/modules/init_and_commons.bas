Attribute VB_Name = "init_and_commons"
Sub initialize()
    With ActiveDocument.ActiveWindow
        .View.Type = wdNormalView
        .View.ShowAll = True
        .StyleAreaWidth = CentimetersToPoints(5)
    End With
    
    Call SetOEDocumentProperty

    #If Mac Then
        ' doesn't work on macOS
    #Else
        With Application
            .TaskPanes(wdTaskPaneFormatting).Visible = True
            .CommandBars("Styles").Position = msoBarRight
        End With
        With ActiveDocument
            'Styles pane options
            .FormattingShowClear = False
            .FormattingShowFilter = wdShowFilterStylesInUse
            .FormattingShowFont = True
            .FormattingShowNumbering = True
            .FormattingShowParagraph = True
        End With
    #End If
    
    If ActiveDocument.Footnotes.Count >= 1 Then
        With ActiveDocument.ActiveWindow.View
            .SplitSpecial = wdPaneFootnotes
        End With
    End If
    
    Call AddKeyBinding 'load shortcuts
   ActiveDocument.UpdateStyles
End Sub

Sub SetOEDocumentProperty()
    Call SetCustomDocumentProperty(Name_:="tplVersion", value:="1.0")
    Call SetCustomDocumentProperty(Name_:="source", value:="OpenEdition")
End Sub

Sub SetCustomDocumentProperty(Name_ As String, value)
    For Each Prop In ActiveDocument.CustomDocumentProperties
        If LCase(Prop.Name) = LCase(Name_) Then
            'suppression de la propri�t� ind�pendamment de la casse
            ActiveDocument.CustomDocumentProperties(Prop.Name).Delete
        End If
    Next
    ' D�finition de la propri�t� (nouvelle ou qui vient d'�tre supprim�e
    ActiveDocument.CustomDocumentProperties.Add _
        Name:=Name_, LinkToContent:=False, Type:=msoPropertyTypeString, value:=value
End Sub

Sub init(control As IRibbonControl)
   initialize
End Sub

Sub AutoNew()
    'Applies to new documents you create
    initialize
End Sub

Sub AutoOpen()
    'Applies to existing documents you open
    initialize
End Sub

Function paragraphs()
    Dim apply_style_to_selection As Boolean
    If apply_style_to_selection = False Then
        Selection.Expand wdParagraph
    End If
    
    Set paragraphs = Selection
End Function

Sub ApplyStyleLang(styleRootName As String, Optional delimiter As String = "|")
    Dim result As String
    Dim styleName As String
    Dim langiso6391 As String
    
    Unload language_selector
    Load language_selector
    language_selector.Show
    result = language_selector.dropdown_language.value

    If result <> "" And result <> "-------------" Then
        langiso6391 = Split(result, " | ")(0)
        ' If styleRootName ends with '|xx', remove suffix
        If Left(Right(styleRootName, 3), 1) = delimiter Then
          styleRootName = Left(styleRootName, Len(styleRootName) - 3)
        End If
        styleName = styleRootName + delimiter + langiso6391
        On Error Resume Next
        Set the_style = ActiveDocument.Styles.Add(styleName, wdStyleTypeParagraph)
        the_style.BaseStyle = ActiveDocument.Styles(styleRootName)
        Selection.paragraphs.style = ActiveDocument.Styles(styleName)
    Else
        msgbox "A language is required"
    End If
End Sub

' Apply language suffix, except for Headings
Sub style_language(control As IRibbonControl)
    Dim SelParaFirst As Paragraph
    Set SelParaFirst = Selection.paragraphs.First
    Dim current_style_name As String
    If SelParaFirst.OutlineLevel > 9 Or SelParaFirst.OutlineLevel < 1 Then
        current_style_name = ActiveDocument.ActiveWindow.Selection.style
        Call ApplyStyleLang(current_style_name, "|")
    Else
        msgbox "Language suffix is not available for Headings"
    End If
End Sub


Sub section(start_style As String, start_text As String, end_style As String, end_text As String)
    
    Dim SelRangeFirst As Range
    Dim SelRangeLast As Range
    Dim PrevRange As Range
    Dim NextRange As Range
    Dim oTable As table
    Dim oRng As Range
    
    Selection.Expand wdParagraph
    Set SelRangeFirst = Selection.paragraphs.First.Range
    Set SelRangeLast = Selection.paragraphs.Last.Range

    ' Le 1er paragraphe de la s�lection est dans un tableau
    If SelRangeFirst.Information(wdWithInTable) = True Then
        Set oTable = SelRangeFirst.Tables(1)
        Set oRng = oTable.Range
        oRng.Select
        Selection.SplitTable
        Set PrevRange = oRng.paragraphs.First.Range
    ' La 1er paragraphe de la s�lection n'est pas dans un tableau
    Else
        SelRangeFirst.InsertParagraphBefore
        Set PrevRange = SelRangeFirst.paragraphs.First.Range
    End If
     ' Le dernier paragraphe de la s�lection est dans un tableau
    If SelRangeLast.Information(wdWithInTable) = True Then
        Set oTable = SelRangeLast.Tables(1)
        Set oRng = oTable.Range
        oRng.Collapse wdCollapseEnd
        oRng.InsertParagraphAfter
        Set NextRange = oRng.paragraphs.Last.Range
    ' Le dernier paragraphe de la s�lection n'est pas dans un tableau
    Else
        If Len(SelRangeLast) > 2 Then
            SelRangeLast.End = SelRangeLast.End - 1
            SelRangeLast.InsertParagraphAfter
            Set NextRange = SelRangeLast.paragraphs.Last.Next.Range
        Else ' le dernier paragraphe contient uniquement une image : Len(SelRangeLast)==2
            SelRangeLast.InsertParagraphAfter
            Set NextRange = SelRangeLast.paragraphs.Last.Range
        End If
    End If

    PrevRange.style = ActiveDocument.Styles(start_style)
    ActiveDocument.Range(PrevRange.Start, PrevRange.End - 1).Text = start_text
    
    NextRange.style = ActiveDocument.Styles(end_style)
    ActiveDocument.Range(NextRange.Start, NextRange.End - 1).Text = end_text
    
    Set oTable = Nothing
    Set oRng = Nothing
    Set SelRangeFirst = Nothing
    Set SelRangeLast = Nothing
    Set PrevRange = Nothing
    Set NextRange = Nothing
End Sub

