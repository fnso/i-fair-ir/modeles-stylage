VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} code_selector 
   Caption         =   "Choose code language"
   ClientHeight    =   2040
   ClientLeft      =   108
   ClientTop       =   456
   ClientWidth     =   5148
   OleObjectBlob   =   "code_selector.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "code_selector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub dropdown_code_Change()

End Sub

Private Sub UserForm_Initialize()
    With code_selector.dropdown_code
        .AddItem "undefined"
        'source: https://github.com/syntaxhighlighter/syntaxhighlighter/wiki/Package-Status
        .AddItem "bash"
        .AddItem "cpp"
        .AddItem "csharp"
        .AddItem "css"
        .AddItem "diff"
        .AddItem "java"
        .AddItem "javascript"
        .AddItem "perl"
        .AddItem "php"
        .AddItem "plain"
        .AddItem "python"
        .AddItem "ruby"
        .AddItem "sass"
        .AddItem "scala"
        .AddItem "sql"
        .AddItem "vb"
        .AddItem "xml"
        
        'Use drop-down list
        .style = fmStyleDropDownList
        'Combo box values are ListIndex values
        .BoundColumn = 1
        'Set combo box to first entry
        .ListIndex = 0
    End With
End Sub

Private Sub validate_Click()
    code_selector.Hide
End Sub

