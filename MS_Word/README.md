# Exemple de code VBA



## Ruban UI

Technologie : `XML` : `custom UI`

```xml
<button id="titremain" getLabel="GetLabel" imageMso="ShapeStar" onAction="titremain"/>
```



## Application des styles

Technologie : `VBA`

Objectif : application des styles 

- styles génériques *Word*

  (*built-in styles*) : *Titre principal*, *Titre 1* à 6, *Normal*, *Note de bas de page*, *Citation*, *Bibliographie* 

- styles personnalisés du modèle



````vb
Sub T1(control As IRibbonControl)
    Selection.style = ActiveDocument.Styles(wdStyleHeading1)
End Sub
````





### Affectation d'un raccourci

Spec OpenEdition

````vb
CustomizationContext = ActiveDocument.AttachedTemplate
KeyBindings.Add KeyCategory:=wdKeyCategoryStyle, _
    Command:=wdStyleTitle, _
    KeyCode:=BuildKeyCode(wdKeyControl, wdKeyT)
````



## Traduction des labels

Technologie : `VBA`

Objectif : détection de la langue de l’utilisateur et affectation du label correspondant.

Comportement pas défaut : langue anglais

```vb
Select Case Application.International(wdProductLanguageID)
    Case 1036, 2060, 3084 'fr
			Msg = "Message en français"
    Case 1034, 3082, 11274, 2058, 9226 'es
 			Msg = "Message en espagnol"
    Case Else 'autre lang = en
      Msg = "Message en anglais"
End Select
```



## Ajout de métadonnées personnalisées

Technologie : `VBA`

```vb
Private Sub cmdOK_Click()
    ActiveDocument.CustomDocumentProperties.Add _
                Name:="pagination", _
                LinkToContent:=False, _
                Type:=msoPropertyTypeString, _
                Value:=TextBox1
    Me.Repaint
    Me.Hide
End Sub
```



Encodage `flatXML`

```xml
<meta:user-defined meta:name="pagination" meta:value-type="string">
  12
</meta:user-defined>
```


